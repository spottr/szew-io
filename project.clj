(defproject szew/io "0.5.9-SNAPSHOT"

  :description "File to data and back."
  :url "https://bitbucket.org/spottr/szew-io"

  :license {:name "MIT Public License"
            :distribution :repo
            :comments "LICENSE file in project root directory."}

  :dependencies [[org.clojure/clojure "1.12.0"]
                 ;; Common denominator across the land:
                 [clojure-csv/clojure-csv "2.0.2"]
                 ;; The king, who is not dead:
                 [org.clojure/data.xml "0.0.8"]
                 ;; Data traversal, helps with the king:
                 [org.clojure/data.zip "1.1.0"]
                 ;; Literally headers:
                 [camel-snake-kebab "0.4.3"]]

  :profiles {:dev {:dependencies [[criterium "0.4.6"]
                                  [org.clojure/test.check "1.1.1"]
                                  [eftest "0.6.0"]
                                  [orchestra "2021.01.01-1"]
                                  [hashp "0.2.2"]]
                   :plugins [[lein-codox "0.10.8"]]
                   :source-paths ["dev"]
                   ;; opts for Java 9+ and data.xml
                   #_#_jvm-opts ["--illegal-access=debug"]
                   #_#_jvm-opts ["--illegal-access=deny"]}
             :codox {}
             ;:test {}
             :1.10 {:dependencies [[org.clojure/clojure "1.10.3"]]}
             :1.11 {:dependencies [[org.clojure/clojure "1.11.3"]]}
             :uberjar {:aot :all}}

  :global-vars {*warn-on-reflection* true
                *assert* true}

  :repl-options {:init-ns user
                 :timeout 380000}
  :aot :all

  :codox {:project {:name "szew/io"}
          :namespaces [#"^szew\.io.*$"]
          :doc-files ["CHANGELOG.md"]
          ;; source links here
          :source-uri ~(str "https://bitbucket.org/spottr/szew-io/src/"
                            "300076741bd9931d7d8da7343784f19c80903a8a" ;; 0.5.8
                            "/{filepath}#{basename}-{line}")})
