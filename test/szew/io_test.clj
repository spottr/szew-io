; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns szew.io-test
  "Input and Output processors test suite."
  (:require
   [szew.io
    :refer [in! sink lines csv tsv fixed-width xml edn
            pruning-file-seq files paths hasher fast-hasher
            gzip-input-stream gzip-output-stream
            gzip-reader gzip-writer
            string-reader pass-through-writer]]
   [szew.io.fu
    :refer [to-string directory? file? exists?  file-name absolute-path
            pruning-path-seq]]
   [clojure.string :as string]
   [clojure.java.io :as clj.io]
   [clojure.zip :as clj.zip]
   [clojure.data.zip.xml :refer [xml-> attr attr=]]
   [clojure.test :refer [deftest testing is]]
   [orchestra.spec.test :as st])
  (:import [clojure.lang ExceptionInfo]))

(st/instrument)

;; ## State provider

(defn file-please!
  "Gimme a temporary File.
  "
  []
  (io! "Mr Java.Io.File, bring me a File!"
       (doto (java.io.File/createTempFile "szew-io-test-" ".bin")
         (.deleteOnExit))))

(defn thanks-for-the-file!
  "Delete a File.
  "
  [^java.io.File f]
  (when f
    (.delete f)))

;; ## Inputs

(deftest Lines-processing-tests

  (testing "Lines constructor contracts"
    (is (any? (lines)))
    (is (= (lines) (lines nil) (lines {}) (lines (lines))))
    (is (= 9001 (:extra (lines {:extra 9001}))))
    (is (thrown? AssertionError (lines :keyword)))
    (is (thrown? AssertionError (lines {:encoding :keyword})))
    (is (thrown? AssertionError (lines {:eol :keyword})))
    (is (thrown? AssertionError (lines {:append :maybe?})))
    (is (thrown? AssertionError (lines {:final-eol 111})))
    (is (thrown? AssertionError (lines {:part -1})))
    (is (= (System/getProperty "line.separator") (:eol (lines)))))

  (testing "Lines Input and Output checks"
    (let [tmp (file-please!)]
      (try
        (let [spec   (lines)
              borked (assoc spec :eol :not-a-valid-eol)
              source "datasets/sample_basic_level0_columns.txt"
              target (.getCanonicalPath ^java.io.File tmp)]
          (is (thrown? Exception (in! borked source)))
          (try
            (in! borked source)
            (catch Exception ex
              (is (= #{:explanation :source} (set (keys (ex-data ex)))))
              (is (= source (-> ex ex-data :source)))))
          (is (thrown? Exception (sink borked target)))
          (try
            (sink borked target)
            (catch Exception ex
              (is (= #{:explanation :target} (set (keys (ex-data ex)))))
              (is (= target (-> ex ex-data :target))))))
        (finally (thanks-for-the-file! tmp)))))

  (testing "Lines processing, super simple, head free."
    (is (= 20 (in! (lines {:processor count})
                   "datasets/sample_basic_level0_columns.txt")))
    (is (= 19 (in! (lines {:processor count})
                   "datasets/sample_basic_level0_relational.tsv")))
    (is (= 20 ((lines {:processor count})
               "datasets/sample_basic_level0_columns.txt")))
    (is (= 19 ((lines {:processor count})
               "datasets/sample_basic_level0_relational.tsv")))
    (is (= 20 (lines {:processor count}
                     "datasets/sample_basic_level0_columns.txt")))
    (is (= 19 (lines {:processor count}
                     "datasets/sample_basic_level0_relational.tsv"))))

  (testing "Line processing, custom partitioning on writer"
    (let [tmp (file-please!)]
      (try
        (let [reader (lines {:processor vec})
              writer (lines {:processor vec :part 3 :final-eol false})]
          ((sink writer tmp)
           (in! reader "datasets/sample_basic_level0_columns.txt"))
          (is (= (in! reader "datasets/sample_basic_level0_columns.txt")
                 (in! reader tmp))))
        (finally (thanks-for-the-file! tmp))))))

(deftest Delimited-processing-tests

  (testing "CSV/TSV constructor contracts"
    (is (any? (csv)))
    (is (= (csv) (csv nil) (csv {}) (csv (csv))))
    (is (= 9001 (:extra (csv {:extra 9001}))))
    (is (= (tsv) (csv {:delimiter \tab})))
    (is (thrown? ExceptionInfo (csv :keyword)))
    (is (thrown? ExceptionInfo (csv {:encoding :keyword})))
    (is (thrown? ExceptionInfo (csv {:append :maybe?})))
    (is (thrown? ExceptionInfo (csv {:delimiter 808})))
    (is (thrown? ExceptionInfo (csv {:strict :how-do-I-know?})))
    (is (thrown? ExceptionInfo (csv {:eol :keyword})))
    (is (thrown? ExceptionInfo (csv {:quote-char "not a char"})))
    (is (thrown? ExceptionInfo (csv {:force-quote "you tell me"})))
    (is (thrown? ExceptionInfo (csv {:part -1})))
    (is (= (System/getProperty "line.separator") (:eol (csv)))))

  (testing "Empty CSV file maybe."
    (let [spec (csv {:processor vec})
          source "datasets/empty.csv"]
      (try
        (is (= [] (in! spec source)))
        (catch Exception ex
          (is (true? ex))))))

  (testing "CSV Input and Output checks"
    (let [tmp (file-please!)]
      (try
        (let [spec   (csv)
              borked (assoc spec :eol :not-a-valid-eol :delimiter \space)
              source "datasets/sample_basic_level0_columns.txt"
              target (.getCanonicalPath ^java.io.File tmp)]
          (is (thrown? Exception (in! borked source)))
          (try
            (in! borked source)
            (catch Exception ex
              (is (= #{:explanation :source} (set (keys (ex-data ex)))))
              (is (= source (-> ex ex-data :source)))))
          (is (thrown? Exception (sink borked target)))
          (try
            (sink borked target)
            (catch Exception ex
              (is (= #{:explanation :target} (set (keys (ex-data ex)))))
              (is (= target (-> ex ex-data :target))))))
        (finally (thanks-for-the-file! tmp)))))

  (testing "CSV processing, almost as simple, head free."
    (is (= 20 (in! (csv {:processor count :delimiter \space})
                   "datasets/sample_basic_level0_columns.txt")))
    (is (= #{7 8 9 10 11}
           (in! (csv {:processor  (comp (partial into (hash-set))
                                        (partial map count))
                      :delimiter  \space
                      :quote-char \"})
                "datasets/sample_basic_level0_columns.txt")))
    (is (= 19 (in! (csv {:processor count :delimiter \tab})
                   "datasets/sample_basic_level0_relational.tsv")))
    (is (= #{13} (in! (csv {:processor (comp (partial into (hash-set))
                                             (partial map count))
                            :delimiter \tab})
                      "datasets/sample_basic_level0_relational.tsv")))
    (is (= 20 ((csv {:processor count :delimiter \space})
               "datasets/sample_basic_level0_columns.txt")))
    (is (= #{7 8 9 10 11}
           ((csv {:processor  (comp (partial into (hash-set))
                                    (partial map count))
                  :delimiter  \space
                  :quote-char \"})
            "datasets/sample_basic_level0_columns.txt")))
    (is (= 19 ((csv {:processor count :delimiter \tab})
               "datasets/sample_basic_level0_relational.tsv")))
    (is (= #{13} ((csv {:processor (comp (partial into (hash-set))
                                         (partial map count))
                        :delimiter \tab})
                  "datasets/sample_basic_level0_relational.tsv")))
    (is (= 20 (csv {:processor count :delimiter \space}
                   "datasets/sample_basic_level0_columns.txt")))
    (is (= #{7 8 9 10 11}
           (csv {:processor  (comp (partial into (hash-set))
                                   (partial map count))
                 :delimiter  \space
                 :quote-char \"}
                "datasets/sample_basic_level0_columns.txt")))
    (is (= 19 (csv {:processor count :delimiter \tab}
                   "datasets/sample_basic_level0_relational.tsv")))
    (is (= #{13} (csv {:processor (comp (partial into (hash-set))
                                        (partial map count))
                       :delimiter \tab}
                      "datasets/sample_basic_level0_relational.tsv"))))

  (testing "Meta of result"
    (let [source "datasets/sample_basic_level0_columns.txt"
          spec   (csv {:processor first :delimiter \space})
          row1   (in! spec source)]
      (is (= 1 (-> row1 meta :line)))
      (is (= spec (-> row1 meta :spec)))
      (is (= source (-> row1 meta :source)))))

  (testing "TSV processing, non-strict"
    (let [input  "datasets/sample_basic_level0_relational_loose.tsv"
          strict (tsv {:strict true})
          loose  (tsv {:strict false})]
      (is (thrown? Exception (in! strict input)))
      (try
        (in! strict input)
        (catch Exception ex
          (is (= 11 (:line (ex-data ex))))
          (is (= input (:source (ex-data ex))))
          (is (= strict (:spec (ex-data ex))))))
      (is (vector? (in! loose input)))
      (is (= 19 (count (in! loose input)))))))

(deftest FixedWidth-processing-tests

  (testing "FixedWidth constructor contracts"
    (is (any? (fixed-width)))
    (is (= (fixed-width)
           (fixed-width nil)
           (fixed-width {})
           (fixed-width (fixed-width))))
    (is (= 9001 (:extra (fixed-width {:extra 9001}))))
    (is (thrown? ExceptionInfo (fixed-width :keyword)))
    (is (thrown? ExceptionInfo (fixed-width {:encoding :keyword})))
    (is (thrown? ExceptionInfo (fixed-width {:append :maybe?})))
    (is (thrown? ExceptionInfo (fixed-width {:eol 9001})))
    (is (thrown? ExceptionInfo (fixed-width {:strict :how-do-I-know?})))
    (is (thrown? ExceptionInfo (fixed-width {:widths []})))
    (is (thrown? ExceptionInfo (fixed-width {:final-eol :keyword})))
    (is (thrown? ExceptionInfo (fixed-width {:fill-char :keyword})))
    (is (thrown? ExceptionInfo (fixed-width {:part -1})))
    (is (= (System/getProperty "line.separator") (:eol (fixed-width)))))

  (testing "FixedWidth processing, simple, head free."
    (is (= 5 (in! (fixed-width {:widths [5 5 5 5] :processor count})
                  "datasets/fixed_width.txt")))
    (is (= (in! (fixed-width {:widths [5 5] :processor vec})
                "datasets/fixed_width.txt")
           [["aaaaa" "bbbbb"]
            ["12345" "67890"]
            ["12345" "67890"]
            ["12345" "67890"]
            ["12345" "67890"]]))
    (is (empty? (in! (fixed-width {:widths [100]})
                     "datasets/fixed_width.txt")))
    (is (thrown? Exception
                 (in! (fixed-width {:widths [100] :strict true})
                      "datasets/fixed_width.txt")))
    (is (= 5 ((fixed-width {:widths [5 5 5 5] :processor count})
              "datasets/fixed_width.txt")))
    (is (= (fixed-width {:widths [5 5] :processor vec}
                        "datasets/fixed_width.txt")
           [["aaaaa" "bbbbb"]
            ["12345" "67890"]
            ["12345" "67890"]
            ["12345" "67890"]
            ["12345" "67890"]]))
    (is (empty? ((fixed-width {:widths [100]}) "datasets/fixed_width.txt")))
    (is (= 5 ((fixed-width {:widths [5 5 5 5] :processor count})
              "datasets/fixed_width.txt")))
    (is (= (fixed-width {:widths [5 5] :processor vec}
                        "datasets/fixed_width.txt")
           [["aaaaa" "bbbbb"]
            ["12345" "67890"]
            ["12345" "67890"]
            ["12345" "67890"]
            ["12345" "67890"]]))
    (is (empty? (fixed-width {:widths [100]} "datasets/fixed_width.txt")))
    (is (= 1 (in! (fixed-width {:processor (comp :line-no meta first)
                                :widths [5 5]})
                  "datasets/fixed_width.txt")))
    (is (= "datasets/fixed_width.txt"
           (in! (fixed-width {:processor (comp :source meta first)
                              :widths [5 5]})
                "datasets/fixed_width.txt"))))

  (testing "FixedWidth Input and Output checks"
    (let [tmp (file-please!)]
      (try
        (let [spec   (fixed-width {:widths [5 5 5 5]})
              borked (assoc spec :eol :not-a-valid-eol)
              source "datasets/fixed_width.txt"
              target (.getCanonicalPath ^java.io.File tmp)]
          (is (thrown? Exception (in! borked source)))
          (try
            (in! borked source)
            (catch Exception ex
              (is (= #{:explanation :source} (set (keys (ex-data ex)))))
              (is (= source (-> ex ex-data :source)))))
          (is (thrown? Exception (sink borked target)))
          (try
            (sink borked target)
            (catch Exception ex
              (is (= #{:explanation :target} (set (keys (ex-data ex)))))
              (is (= target (-> ex ex-data :target))))))
        (finally (thanks-for-the-file! tmp)))))

  (testing "FixedWidth round-tripping rows"
    (let [tmp (file-please!)]
      (try
        (let [data  [["A" "B"] ["xxx" "yyy"] ["aaa" "bbb"]]
              spec  (fixed-width {:widths [4 4]})
              path  (.getCanonicalPath ^java.io.File tmp)
              dump  (sink spec path)
              trim  (partial mapv (partial mapv (memfn ^String trim)))
              data2 [["A" "B"] ["xx" "yy"] ["aa" "bb"]]
              spec2 (fixed-width {:widths [2 2]})
              dump2 (sink spec2 path)
              spec3 (fixed-width {:widths [4 4] :fill-char \#})
              trim3 (partial mapv (partial mapv #(string/replace % #"#+$" "")))
              dump3 (sink spec3 path)]
          (dump data)
          (is (= 3 (in! (assoc spec :processor count) path)))
          (is (= data (in! (assoc spec :processor trim) path)))
          (dump2 data)
          (is (= 3 (in! (assoc spec2 :processor count) path)))
          (is (= data2 (in! (assoc spec2 :processor trim) path)))
          (dump3 data)
          (is (= [["A###" "B###"] ["xxx#" "yyy#"] ["aaa#" "bbb#"]]
                 (in! spec path)))
          (is (= data
                 (in! (assoc spec :processor trim3) path)
                 (in! (assoc spec3 :processor trim3) path))))
        (finally (thanks-for-the-file! tmp)))))

  (testing "FixedWidth processing, custom partitioning on writer"
    (let [tmp (file-please!)]
      (try
        (let [reader (fixed-width {:processor vec :widths [5 5 5 5]})
              writer (fixed-width {:processor vec :widths [5 5 5 5]
                                   :part 2 :final-eol false})]
          ((sink writer tmp)
           (in! reader "datasets/fixed_width.txt"))
          (is (= (in! reader "datasets/fixed_width.txt")
                 (in! reader tmp))))
        (finally (thanks-for-the-file! tmp))))))

(deftest XML-processing-tests

  (testing "XML constructor contracts"
    (is (any? (xml)))
    (is (= (xml) (xml nil) (xml {}) (xml (xml))))
    (is (= 9001 (:extra (xml {:extra 9001}))))
    (is (thrown? ExceptionInfo (xml :keyword)))
    (is (thrown? ExceptionInfo (xml {:encoding :keyword})))
    (is (thrown? ExceptionInfo (xml {:append :maybe?}))))

  (testing "XML processing, don't know what to say here."
    (is (= "Red Leader"
           (in! (xml {:processor (comp :name :attrs first :content first
                                       :content)})
                "datasets/sample.xml")))
    (is (= "Red Shirt"
           (in! (xml {:processor (comp second
                                       #(xml-> % :root :group :child
                                               (attr :name))
                                       clj.zip/xml-zip)})
                "datasets/sample.xml")))
    (is (= "Blue Pants"
           (in! (xml {:processor (comp first
                                       #(xml-> % :root :group :child
                                               (attr= :name "Blue Pants")
                                               (attr :name))
                                       clj.zip/xml-zip)})
                "datasets/sample.xml")))
    (is (map? (in! (xml) "datasets/sample.xml")))
    (is (= 2 (-> (in! (xml) "datasets/sample.xml") :content count)))
    (is (= "Dom" (-> (in! (xml) "datasets/sample.xml")
                     :content first :content first :content first)))
    (is (= "Red Leader"
           ((xml {:processor (comp :name :attrs first :content first
                                   :content)})
            "datasets/sample.xml")))
    (is (= "Red Shirt"
           ((xml {:processor (comp second
                                   #(xml-> % :root :group :child
                                           (attr :name))
                                   clj.zip/xml-zip)})
            "datasets/sample.xml")))
    (is (= "Blue Pants"
           ((xml {:processor (comp first
                                   #(xml-> % :root :group :child
                                           (attr= :name "Blue Pants")
                                           (attr :name))
                                   clj.zip/xml-zip)})
            "datasets/sample.xml")))
    (is (map? ((xml) "datasets/sample.xml")))
    (is (= 2 (-> ((xml) "datasets/sample.xml") :content count)))
    (is (= "Dom" (-> ((xml) "datasets/sample.xml")
                     :content first :content first :content first)))
    (is (= "Red Leader"
           (xml {:processor (comp :name :attrs first :content first
                                  :content)}
                "datasets/sample.xml")))
    (is (= "Red Shirt"
           (xml {:processor (comp second
                                  #(xml-> % :root :group :child
                                          (attr :name))
                                  clj.zip/xml-zip)}
                "datasets/sample.xml")))
    (is (= "Blue Pants"
           (xml {:processor (comp first
                                  #(xml-> % :root :group :child
                                          (attr= :name "Blue Pants")
                                          (attr :name))
                                  clj.zip/xml-zip)}
                "datasets/sample.xml")))
    (is (map? (xml {} "datasets/sample.xml")))
    (is (= 2 (-> (xml {} "datasets/sample.xml") :content count)))
    (is (= "Dom" (-> (xml {} "datasets/sample.xml")
                     :content first :content first :content first)))
    (let [tmp (file-please!)]
      (try
        (let [initial (in! (xml) "datasets/sample.xml")
              xsink (sink (xml) tmp)
              _ (xsink initial)
              reread (in! (xml) tmp)]
          (is (= initial reread)))
        (finally (thanks-for-the-file! tmp)))))

  (testing "XML Input and Output checks"
    (let [tmp (file-please!)]
      (try
        (let [spec   (xml)
              borked (assoc spec :encoding :not-a-valid-encoding-lol)
              source "datasets/sample.xml"
              target (.getCanonicalPath ^java.io.File tmp)]
          (is (thrown? Exception (in! borked source)))
          (try
            (in! borked source)
            (catch Exception ex
              (is (= #{:explanation :source} (set (keys (ex-data ex)))))
              (is (= source (-> ex ex-data :source)))))
          (is (thrown? Exception (sink borked target)))
          (try
            (sink borked target)
            (catch Exception ex
              (is (= #{:explanation :target} (set (keys (ex-data ex)))))
              (is (= target (-> ex ex-data :target))))))
        (finally (thanks-for-the-file! tmp))))))

(deftest EDN-processing-tests

  (testing "EDN constructor contracts"
    (is (any? (edn)))
    (is (= (edn) (edn nil) (edn {}) (edn (edn))))
    (is (= 9001 (:extra (edn {:extra 9001}))))
    (is (thrown? ExceptionInfo (edn :keyword)))
    (is (thrown? ExceptionInfo (edn {:encoding :keyword})))
    (is (thrown? ExceptionInfo (edn {:append :maybe?})))
    (is (thrown? ExceptionInfo (edn {:readers :keyword})))
    (is (thrown? ExceptionInfo (edn {:default "not-a-fn"}))))

  (testing "END processing, basically retesting standard library."
    (is (= {:a "a", :b "b"}
           (in! (edn {:processor first}) "datasets/sample.edn")))
    (is (= #inst "2019-03-11T20:37:00.666-00:00"
           (in! (edn {:processor last}) "datasets/sample.edn")))
    (is (= 11
           (in! (edn {:processor (comp count vec)}) "datasets/sample.edn")))
    (is (= {:a "a", :b "b"}
           ((edn {:processor first}) "datasets/sample.edn")))
    (is (= #inst "2019-03-11T20:37:00.666-00:00"
           ((edn {:processor last}) "datasets/sample.edn")))
    (is (= 11
           ((edn {:processor (comp count vec)}) "datasets/sample.edn")))
    (is (= {:a "a", :b "b"}
           (edn {:processor first} "datasets/sample.edn")))
    (is (= #inst "2019-03-11T20:37:00.666-00:00"
           (edn {:processor last} "datasets/sample.edn")))
    (is (= 11
           (edn {:processor (comp count vec)} "datasets/sample.edn")))
    (let [tmp (file-please!)]
      (try
        (let [initial (in! (edn) "datasets/sample.edn")
              _       ((sink (edn) tmp) initial)
              reread  (in! (edn) tmp)]
          (is (= initial reread))
          (let [_  ((sink (edn {:append true}) tmp) initial)
                x2 (in! (edn) tmp)]
            (is (= (vec (concat initial initial)) x2))))
        (finally (thanks-for-the-file! tmp)))))

  (testing "EDN Input and Output checks"
    (let [tmp (file-please!)]
      (try
        (let [spec   (edn)
              borked (assoc spec :encoding :not-a-valid-encoding-lol)
              source "datasets/sample.edn"
              target (.getCanonicalPath ^java.io.File tmp)]
          (is (thrown? Exception (in! borked source)))
          (try
            (in! borked source)
            (catch Exception ex
              (is (= #{:explanation :source} (set (keys (ex-data ex)))))
              (is (= source (-> ex ex-data :source)))))
          (is (thrown? Exception (sink borked target)))
          (try
            (sink borked target)
            (catch Exception ex
              (is (= #{:explanation :target} (set (keys (ex-data ex)))))
              (is (= target (-> ex ex-data :target))))))
        (finally (thanks-for-the-file! tmp))))))

(deftest Files-processors-testing

  (letfn [(file? [^java.io.File f] (.isFile f))
          (dir? [^java.io.File f] (.isDirectory f))
          (basename [^java.io.File f] (.getName f))
          (file-names [fs] (mapv basename (filterv file? fs)))
          (dir-names [fs] (mapv basename (filterv dir? fs)))
          (files? [fs] (->> fs file-names count pos?))
          (in-tree? [^java.io.File f]
            (re-find #"(?xi).*tree_root.*" (.getCanonicalPath f)))
          (in-empty-set? [^java.io.File f]
            (re-find #"(?xi).*xxxyyyzzz.*"
                     (.getCanonicalPath f)))]

    (testing "Testing basics around empty_dir"
      (is (= ["empty_dir"] (in! (files {:processor dir-names})
                                "datasets/empty_dir")))
      (is (= ["empty_dir"]
             (in! (files {:processor dir-names}) "datasets/empty_dir")))
      (is (= ["empty_dir"]
             (in! (files {:processor dir-names}) "datasets/empty_dir")))
      (is (empty?  (in! (files {:processor dir-names}) "xdatasets")))
      (is (true? (in! (files {:processor files?}) "datasets/")))
      (is (true? (in! (files {:processor files?
                              :follow? (constantly true)}) "datasets/"))))

    (testing "Kinda file-seq but with pruning"
      (is (files? (pruning-file-seq (clj.io/as-file "datasets/")
                                    (constantly true))))
      (is (= 0 (count (pruning-file-seq (clj.io/as-file "datasets/")
                                        (constantly false)))))
      (is (= 1 (count (pruning-file-seq (clj.io/as-file "xdatasets/")
                                        (constantly true)))))
      (is (= 0 (count (pruning-file-seq (clj.io/as-file "xdatasets/")
                                        (constantly false)))))
      (is (= 0 (count (pruning-file-seq (clj.io/as-file "xdatasets/")
                                        (memfn ^java.io.File exists)))))
      (is (false? (in! (files {:processor files?
                               :follow? in-empty-set?}) "datasets/")))
      (is (false? (in! (files {:processor files?
                               :follow? (constantly false)}) "datasets/")))
      (is (true? (in! (files {:processor files?}) "datasets/")))
      (is (true? (in! (files {:processor files?}) "datasets/")))
      (is (false? ((files {:processor files?
                           :follow? in-empty-set?}) "datasets/")))
      (is (false? ((files {:processor files?
                           :follow? (constantly false)}) "datasets/")))
      (is (true? ((files {:processor files?}) "datasets/")))
      (is (true? ((files {:processor files?}) "datasets/")))
      (is (false? (files {:processor files?
                          :follow? in-empty-set?} "datasets/")))
      (is (false? (files {:processor files?
                          :follow? (constantly false)} "datasets/")))
      (is (true? (files {:processor files?} "datasets/")))
      (is (true? (files {:processor files?} "datasets/"))))))

(deftest Paths-processors-testing

  (letfn [(file-names [ps] (mapv (comp to-string file-name)
                                 (filter file? ps)))
          (dir-names [ps] (mapv (comp to-string file-name)
                                (filter directory? ps)))
          (paths? [ps] (->> ps file-names count pos?))
          (in-tree? [^java.nio.file.Path p]
            (re-find #"(?xi).*tree_root.*"
                     (to-string (absolute-path p))))
          (in-empty-set? [^java.nio.file.Path p]
            (re-find #"(?xi).*xxxyyyzzz.*"
                     (to-string (absolute-path p))))]

    (testing "Testing basics around empty_dir"
      (is (= ["empty_dir"] (in! (paths {:processor dir-names})
                                "datasets/empty_dir")))
      (is (= ["empty_dir"]
             (in! (paths {:processor dir-names}) "datasets/empty_dir")))
      (is (= ["empty_dir"]
             (in! (paths {:processor dir-names}) "datasets/empty_dir")))
      (is (empty? (in! (paths {:processor dir-names}) "xdatasets")))
      (is (true? (in! (paths {:processor paths?}) "datasets/")))
      (is (true? (in! (paths {:processor paths?
                              :follow? (constantly true)}) "datasets/"))))

    (testing "Kinda path-seq but with pruning"
      (is (paths? (pruning-path-seq (clj.io/as-file "datasets/")
                                    (constantly true))))
      (is (= 0 (count (pruning-path-seq (clj.io/as-file "datasets/")
                                        (constantly false)))))
      (is (= 1 (count (pruning-path-seq (clj.io/as-file "xdatasets/")
                                        (constantly true)))))
      (is (= 0 (count (pruning-path-seq (clj.io/as-file "xdatasets/")
                                        (constantly false)))))
      (is (= 0 (count (pruning-path-seq (clj.io/as-file "xdatasets/")
                                        exists?))))
      (is (false? (in! (paths {:processor paths?
                               :follow? in-empty-set?}) "datasets/")))
      (is (false? (in! (paths {:processor paths?
                               :follow? (constantly false)}) "datasets/")))
      (is (true? (in! (paths {:processor paths?}) "datasets/")))
      (is (true? (in! (paths {:processor paths?}) "datasets/")))
      (is (false? ((paths {:processor paths?
                           :follow? in-empty-set?}) "datasets/")))
      (is (false? ((paths {:processor paths?
                           :follow? (constantly false)}) "datasets/")))
      (is (true? ((paths {:processor paths?}) "datasets/")))
      (is (true? ((paths {:processor paths?}) "datasets/")))
      (is (false? (paths {:processor paths?
                          :follow? in-empty-set?} "datasets/")))
      (is (false? (paths {:processor paths?
                          :follow? (constantly false)} "datasets/")))
      (is (true? (paths {:processor paths?} "datasets/")))
      (is (true? (paths {:processor paths?} "datasets/"))))))

(deftest Hashing-tests

  (testing "Hasher constructor contracts"
    (is (any? (hasher)))
    (is (= (hasher) (hasher nil) (hasher {}) (hasher (hasher))))
    (is (= 9001 (:extra (hasher {:extra 9001}))))
    (is (thrown? ExceptionInfo (hasher :keyword)))
    (is (thrown? ExceptionInfo (hasher {:hash-name :keyword})))
    (is (thrown? ExceptionInfo (hasher {:skip-bytes :so-many})))
    (is (thrown? ExceptionInfo (hasher {:sample-size -1}))))

  (let [tmp (file-please!)]
    (try
      (testing "Some pretty obvious hashes here!"
        (let [md5     "d41d8cd98f00b204e9800998ecf8427e"
              sha1    "da39a3ee5e6b4b0d3255bfef95601890afd80709"
              sha-256 (str "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca4"
                           "95991b7852b855")
              sha-512 (str "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83"
                           "f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b9"
                           "31bd47417a81a538327af927da3e")]
          (is (= md5 (in! (hasher {:hash-name "MD5"}) tmp)))
          (is (= sha1 (in! (hasher {:hash-name "SHA1"}) tmp)))
          (is (= sha-256 (in! (hasher {:hash-name "SHA-256"}) tmp)))
          (is (= sha-512 (in! (hasher {:hash-name "SHA-512"}) tmp)))
          ;; skipping 2 bytes... changes nothing
          (is (= md5 (in! (hasher {:hash-name "MD5" :skip-bytes 2}) tmp)))
          (is (= sha1 (in! (hasher {:hash-name "SHA1" :skip-bytes 2}) tmp)))
          (is (= sha-256 (in! (hasher {:hash-name "SHA-256" :skip-bytes 2})
                              tmp)))
          (is (= sha-512 (in! (hasher {:hash-name "SHA-512" :skip-bytes 2})
                              tmp)))
          ;; Shortcut test
          (is (= md5 ((hasher {:hash-name "MD5"}) tmp)))
          (is (= sha1 ((hasher {:hash-name "SHA1"}) tmp)))
          (is (= sha-256 ((hasher {:hash-name "SHA-256"}) tmp)))
          (is (= sha-512 ((hasher {:hash-name "SHA-512"}) tmp)))
          ;; Shorter shortcut test
          (is (= md5 (hasher {:hash-name "MD5"} tmp)))
          (is (= sha1 (hasher {:hash-name "SHA1"} tmp)))
          (is (= sha-256 (hasher {:hash-name "SHA-256"} tmp)))
          (is (= sha-512 (hasher {:hash-name "SHA-512"} tmp)))))

      (testing "Hashing tests - non-empty file!"
        (let [md5     "91162629d258a876ee994e9233b2ad87"
              sha1    "43fd70009a97a7d311c5644047ccc700f8d08a9d"
              sha-256 (str "124bfb6284d82f3b1105f88e3e7a0ee02d0e525193413c05b7"
                           "5041917022cd6e")
              sha-512 (str "ea6177922cf84bd32af98e6b497face3f76024bc827404676c"
                           "8f37db547a74910308858248dd7bb4a0900d3a11d0b98f0f84"
                           "c5c03dff7290c1da448c4d8a5d62")]
          (spit tmp "Ala ma kota")
          (is (= md5 (in! (hasher {:hash-name "MD5"}) tmp)))
          (is (= sha1 (in! (hasher {:hash-name "SHA1"}) tmp)))
          (is (= sha-256 (in! (hasher {:hash-name "SHA-256"}) tmp)))
          (is (= sha-512 (in! (hasher {:hash-name "SHA-512"}) tmp)))))

      (testing "Hashing tests - non-empty file - with sample-size!"
        (let [md5     "197cdcc53f062530d6256eddc6fc18e6"
              sha1    "f56f714299498c5d76bc034f964d6c9cc5a3d393"
              sha-256 (str "1af8ffa2785e9493acb0c9157f3f8b9fc194f7c5a756621882"
                           "c1f04e11fb6eb1")
              sha-512 (str "60134240b2f5a88635bb680c7ff0e02cc3671b3fcbe043502c"
                           "0225ff09c661d4c3dfcc42d1309eb7c16c8d2de860deae6056"
                           "fa7a9d4e1665efb5aee7f49948b8")]
          (spit tmp "Ala ma kota")
          (is (= md5 (in! (hasher {:hash-name "MD5" :sample-size 2}) tmp)))
          (is (= sha1 (in! (hasher {:hash-name "SHA1" :sample-size 2}) tmp)))
          (is (= sha-256 (in! (hasher {:hash-name "SHA-256" :sample-size 2})
                              tmp)))
          (is (= sha-512 (in! (hasher {:hash-name "SHA-512" :sample-size 2})
                              tmp)))))

      (testing "Hashing tests - non-empty - w/ sample-size and skip-bytes!"
        (let [md5     "99020cb24bd13238d907c65cc2b57c03"
              sha1    "9384a39d69d104db5d1db7ccceae2ce0cb6c01c2"
              sha-256 (str "6583dcd6056fd32aadd0d2d0be920de99f6ff08b80065e4b91"
                           "42aaa4169391cb")
              sha-512 (str "48542b8d3b9dddf9b1b46ff41654d5278f14091e325c34cfcc"
                           "26467db60972a9b9cc62a63ed5199f47f97a5ccc461c575640"
                           "395fddeef291e0a5b60599d8fec")]
          (spit tmp "Ala ma kota")
          (is (= md5 (in! (hasher {:hash-name  "MD5"
                                   :sample-size 2
                                   :skip-bytes  2})
                          tmp)))
          (is (= sha1 (in! (hasher {:hash-name   "SHA1"
                                    :sample-size 2
                                    :skip-bytes  2})
                           tmp)))
          (is (= sha-256 (in! (hasher {:hash-name   "SHA-256"
                                       :sample-size 2
                                       :skip-bytes  2})
                              tmp)))
          (is (= sha-512 (in! (hasher {:hash-name   "SHA-512"
                                       :sample-size 2
                                       :skip-bytes  2})
                              tmp)))))
      (finally (thanks-for-the-file! tmp)))))

(deftest FastHashing-tests

  (testing "FastHasher constructor contracts"
    (is (any? (fast-hasher)))
    (is (= (fast-hasher) (fast-hasher nil) (fast-hasher {})
           (fast-hasher (fast-hasher))))
    (is (= 9001 (:extra (fast-hasher {:extra 9001}))))
    (is (thrown? ExceptionInfo (fast-hasher :keyword)))
    (is (thrown? ExceptionInfo (fast-hasher {:skip-bytes :so-many})))
    (is (thrown? ExceptionInfo (fast-hasher {:sample-size -1}))))

  (let [tmp (file-please!)]
    (try
      (testing "Some pretty obvious hashes here!"
        (let [adler32 1]
          (is (= adler32 (in! (fast-hasher {}) tmp)))
          (is (= adler32 (fast-hasher {} tmp)))))

      (testing "Hashing tests - non-empty file!"
        (let [adler32 362415052]
          (spit tmp "Ala ma kota")
          (is (= adler32 (in! (fast-hasher {}) tmp)))
          (is (= adler32 (fast-hasher {} tmp)))))

      (testing "Hashing tests - non-empty file - w/ sample size!"
        (let [adler32 15728814]
          (spit tmp "Ala ma kota")
          (is (= adler32 (in! (fast-hasher {:sample-size 2}) tmp)))
          (is (= adler32 (fast-hasher {:sample-size 2} tmp)))))

      (testing "Hashing tests - non-empty file - w/ skip bytes!!"
        (let [adler32 244646687]
          (spit tmp "Ala ma kota")
          (is (= adler32 (in! (fast-hasher {:skip-bytes 2}) tmp)))
          (is (= adler32 (fast-hasher {:skip-bytes 2} tmp)))))

      (testing "Hashing tests - non-empty file - w/ sample and skip bytes!!"
        (let [spec    {:sample-size 2 :skip-bytes 2}
              adler32 14942338]
          (spit tmp "Ala ma kota")
          (is (= adler32 (in! (fast-hasher spec) tmp)))
          (is (= adler32 (fast-hasher spec tmp)))))
      (finally (thanks-for-the-file! tmp)))))

;; ## Outputs

(deftest Output-processors-testing

  (let [tmp (file-please!)]
    (try
      (testing "Lines processing, easier than CSV, but not really."
        (let [rows  (in! (lines {:processor vec})
                         "datasets/sample_basic_level0_relational.tsv")
              _     ((sink (lines) tmp) rows)
              rows2 (in! (lines {:processor vec}) tmp)
              _     ((sink (lines {:final-eol true}) tmp) rows)
              rows3 (in! (lines {:processor vec}) tmp)]

          (is (= rows rows2))
          (is (= (count rows) (count rows3)))
          (is (seq rows))))

      (testing "CSV processing, almost fun and games."
        (let [rows  (in! (csv {:processor vec :delimiter \tab})
                         "datasets/sample_basic_level0_relational.tsv")
              _     ((sink (csv {:delimiter \|}) tmp) rows)
              rows2 (in! (csv {:processor vec :delimiter \|}) tmp)
              _     ((sink (csv {:delimiter \~ :force-quote true}) tmp)
                     rows)
              rows3 (in! (csv {:processor vec :delimiter \~}) tmp)]
          (is (true? (= rows rows2 rows3)))
          (is (seq rows))))

      (testing "FixedWidth procesing."
        (let [fw    (fixed-width {:widths [5 10 5] :processor vec :eol "\n"})
              rows  (in! fw "datasets/fixed_width.txt")
              _     ((sink fw tmp) rows)
              rows2 (in! fw tmp)
              _     ((sink (assoc fw :final-eol true) tmp) rows)
              rows3  (in! fw tmp)]
          (is (= (count rows) (in! (lines {:processor count}) tmp)))
          (is (= rows rows2))
          (is (= rows rows3))))

      (testing "How about XML processing?"
        (in! (xml {:processor (sink (xml) tmp)}) "datasets/sample.xml")
        (let [p (comp :name :attrs first :content first :content)]
          (is (= "Red Leader"
                 (in! (xml {:processor p})
                      tmp)))))

      (testing "And now some EDN."
        (in! (edn {:processor (sink (edn) tmp)}) "datasets/sample.edn")
        (let [data-orig (in! (edn {:processor vec}) "datasets/sample.edn")
              data-test (in! (edn {:processor vec}) tmp)]
          (is (= [{:a "a", :b "b"}
                  :ala-ma-kota
                  #{:c :b :d :a}
                  '(:a :b (quote c) "d")
                  "beer"
                  3.14
                  2.7M
                  2/3
                  [[:vector :of-vectors [:of :vectors]] :and-a-keyword]
                  #uuid "00000000-0000-0000-0000-000000000000"
                  #inst "2019-03-11T20:37:00.666-00:00"]
                 data-orig
                 data-test))))

      (finally (thanks-for-the-file! tmp)))))

;; ## Helpers

(deftest Source-and-Target-helper-functions-testing

  (let [tmp (file-please!)]
    (try
      (testing "GZIP source and target"
        (let [rows  (in! (lines {:processor vec})
                         "datasets/sample_basic_level0_relational.tsv")
              _     ((sink (lines {:writer gzip-writer}) tmp)
                     rows)
              rows2 (in! (lines {:processor vec :reader gzip-reader}) tmp)]
          (is (= rows rows2))))

      (testing "GZIP stream in and out"
        (let [source "datasets/sample_basic_level0_relational.tsv"]
          (with-open [r ^java.io.BufferedInputStream
                      (clj.io/input-stream source)
                      w ^java.io.BufferedOutputStream
                      (gzip-output-stream tmp)]
            (clj.io/copy r w))
          (is (= (in! (hasher) source)
                 (in! (hasher {:input-stream gzip-input-stream}) tmp)))))
      (finally (thanks-for-the-file! tmp))))

  (testing "StringReader and pass-through (StringWriter!) helpers."
    (let [strings  "line1\nline2\nline3"
          target   (java.io.StringWriter.)
          s-sink   (sink (lines {:writer pass-through-writer
                                 :eol "\n"
                                 :final-eol false})
                         target)]
      (is (= 3 (in! (lines {:processor count :reader string-reader})
                    strings)))
      (in! (lines {:processor s-sink :reader string-reader}) strings)
      (is (= strings (.toString target))))))
