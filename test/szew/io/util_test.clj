; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns szew.io.util-test
  "Useful data mangling functions test suite."
  (:require
    [szew.io.util
     :refer [row-adjuster fixed-width-split field-unpacker row-field-unpacker
             vec->map vecs->maps map->vec maps->vecs vecs-maker maps-maker
             bespoke-header
             friendlify getter juxt-map deep-sort roll-in roll-out
             pp-map pp-filter pp-mapcat pp-remove pp-keep
             pp-map-indexed pp-keep-indexed all any]]
    [clojure.test :refer [deftest testing is]]
    [orchestra.spec.test :as st])
  (:import [clojure.lang ExceptionInfo]))

(st/instrument)

;; ## Tests

(deftest Simple-row-length-adjuster-completely-reliable-test

  (testing "row-adjuster"
    (is (fn? (row-adjuster [:x])))
    (is (= [:x] ((row-adjuster []) [:x])))
    (is (= [1 2 3] ((row-adjuster [1 2 3]) [])))
    (is (= [:1 2 3] ((row-adjuster [1 2 3]) [:1])))
    (is (= [:1 :2 3] ((row-adjuster [1 2 3]) [:1 :2])))
    (is (= [:1 :2 :3] ((row-adjuster [1 2 3]) [:1 :2 :3])))
    (is (= [:1 :2 :3] ((row-adjuster [1 2 3]) [:1 :2 :3 :4])))))

(deftest Simple-fixed-length-column-splitter-test

  (testing "fixed-width"
    (is (fn? (fixed-width-split [20 20 20])))
    (is (= [20 20 20] (:fields (meta (fixed-width-split [20 20 20])))))
    (is (= [20 20 20] (:slices (meta (fixed-width-split [20 20 20])))))
    (is (= [[0 20] [20 40] [40 60]]
           (:steps (meta (fixed-width-split [20 20 20])))))
    (is (any? (:doc (meta (fixed-width-split [20 20 20])))))
    (is (thrown? ExceptionInfo (fixed-width-split [])))
    (is (thrown? ExceptionInfo (fixed-width-split [20 20 :a])))
    (is (thrown? ExceptionInfo (fixed-width-split [0])))
    (is (thrown? ExceptionInfo (fixed-width-split [20 20 0])))
    (is (= ["Ala " "ma " "Kota."]
           ((fixed-width-split [4 3 5]) "Ala ma Kota.")))
    (is (= ["Ala " "ma " "Kota"]
           ((fixed-width-split [4 3 4]) "Ala ma Kota.")))
    (is (thrown? Exception ((fixed-width-split [20 20 20]) "Nope.")))))

(deftest Curating-records-test

  (testing "Unpacking fields, chewing bubble gum."
    (is (= ["a" "b" "c"] (field-unpacker \, "a,b,c")))
    (is (= "a,b,c" (field-unpacker \| "a,b,c"))))

  (testing "Unpacking fields from rows, out of bubble gum."
    (is (= ["0" ["a" "b" "c"] "1"] (row-field-unpacker \, ["0" "a,b,c" "1"])))
    (is (= ["0" "a,b,c" "1"] (row-field-unpacker \| ["0" "a,b,c" "1"])))))

(deftest Helpers-vecs->maps-and-maps->vecs-test

  (let [head      [:a :b :c]
        tails     [[1 1 1] [2 2 2] [3 4 5]]
        maps      [{:a 1 :b 1 :c 1} {:a 2 :b 2 :c 2} {:a 3 :b 4 :c 5}]
        maps-x    (mapv #(assoc % :x 0) maps)
        oops      [[1 1 1 2] [2 2 2 3] [3 4 5 6]]
        sparse    [[1 nil 1] [2 2 nil] [nil 4 5]]
        defaults  {:b 1, :c 2, :a 3}
        head2     [:a :b :x]
        defaults2 {:x 0}
        tails2    [[1 1 0] [2 2 0] [3 4 0]]]

    (testing "vec->map, vectors into maps"
      (let [f-map (vec->map head (first tails))
            d-map (vec->map {:b 101} head (first sparse))]
        (is (= (first maps) f-map))
        (is (= {:a 1 :b 101 :c 1} d-map))
        (is (= {:defaults nil, :header [:a :b :c], :values [1 1 1]}
               (meta f-map)))
        (is (= {:defaults {:b 101}, :header [:a :b :c], :values [1 nil 1]}
               (meta d-map)))))

    (testing "vecs->maps, vectors into maps."
      (is (= maps (vecs->maps (cons head tails))))
      (is (= maps (vecs->maps head tails)))
      (is (= maps (vecs->maps head oops)))
      (is (= maps (vecs->maps defaults head sparse)))
      (is (= maps-x (vecs->maps (assoc defaults :x 0) head sparse))))

    (testing "map->vec, map into vector"
      (let [f-vec (map->vec head (first maps))
            d-vec (map->vec defaults2 head2 (first maps))]
        (is (= (first tails) f-vec))
        (is (= (first tails2) d-vec))))

    (testing "maps->vecs, maps into vectors"
      (is (= tails (maps->vecs (cons head maps))))
      (is (= tails (maps->vecs head maps)))
      (is (= tails2 (maps->vecs defaults2 head2 maps))))

    (testing "maps-maker strategies."
      (is (= maps ((maps-maker) (cons head tails))))
      (is (= maps ((maps-maker) (cons ['A :B "C"] tails))))
      (is (= [:a :b :c]
             (-> ((maps-maker) (cons ['A :B "C"] tails))
                 first meta :header)))
      (is (= maps ((maps-maker first rest) (cons head tails))))
      (is (= [:a :b :c]
             (-> ((maps-maker first rest) (cons head tails))
                 first meta :header)))
      (is (= ['A :B "C"]
             (-> ((maps-maker first rest) (cons ['A :B "C"] tails))
                 first meta :header))))

    (testing "bespoke-header tests."
      (is (thrown? Exception (bespoke-header [:a] true {:b 2})))
      (is (= [:a] (bespoke-header [:a] true {})))
      (let [header (bespoke-header [:a :b] false {:a 1 :b 2 :c 3 :d 4})]
        (is (= header (bespoke-header [:a :b] {:a 1 :b 2 :c 3 :d 4})))
        (is (= header [:a :b :c :d])))
      (try (bespoke-header [:a] true {:b 2})
           (catch Exception ex
             (let [data (ex-data ex)]
               (is (= {:b 2} (:a-map data)))
               (is (= #{:a} (:missing data)))
               (is (= #{:a} (:header-set data)))
               (is (= #{:b} (:present-set data)))
               (is (= #{:b} (:remainder-set data)))))))

    (testing "vecs-maker strategies."
      (let [default-maker (vecs-maker)
            sparse-maker  (vecs-maker defaults2)
            daft-maker    (vecs-maker defaults2 identity identity)]
        (is (= [["A" "B" "C"] ["1" "1" "1"] ["2" "2" "2"] ["3" "4" "5"]]
               (default-maker [head maps])))
        (is (= [["A" "B" "X"] ["1" "1" "0"] ["2" "2" "0"] ["3" "4" "0"]]
               (sparse-maker [head2 (mapv #(dissoc % :c) maps)])))
        (is (= defaults2 (:defaults (meta daft-maker))))
        (is (= identity (:header-prep (meta daft-maker))))
        (is (= identity (:row-prep (meta daft-maker))))
        (is (string? (:doc (meta daft-maker))))))))

(defn m-o-m?!|multiverse<+-*%'&=:> [] nil)

(deftest Keywords-are-tits-test

  (testing "Friendlify?"
    (is (= "clojure.core/conj" (friendlify conj)))
    (is (= "szew.io.util-test/m-o-m?!|multiverse<+-*%'&=:>"
           (friendlify m-o-m?!|multiverse<+-*%'&=:>)))))

(deftest Getter-is-not-awesome-but-at-least-it-works-test
  (is (fn? (getter :a)))
  (is (= ((getter :a) {:a 1024}) 1024))
  (is (= ((getter :a 512) {:a 1024}) 1024))
  (is (= ((getter :b 512) {:a 1024}) 512))
  (is (nil? ((getter :b) {:a 1024})))
  (is (= (meta (getter :b)) {:key :b :default nil}))
  (is (= (meta (getter :b 512)) {:key :b :default 512})))

(deftest But-juxt-map-is-fn-and-games-testing
  (is (fn? (juxt-map :+ inc)))
  (is (= ((juxt-map :+ inc :- dec := identity) 2) {:+ 3 :- 1 := 2}))
  (is (thrown? AssertionError (juxt-map :+ inc dec))))

(deftest Deep-sorting-awww-yeah
  (is (= (sorted-map :a 1 :b 2 :c (sorted-map :no :yes :yes :no))
         (deep-sort {:c {:yes :no :no :yes} :b 2 :a 1})))
  (is (= (sorted-map :a 1 :b 2 :c (sorted-set :yes :no))
         (deep-sort {:c #{:yes :no} :b 2 :a 1})))
  (is (= #{:a 'b "c" 3} (deep-sort #{:a 'b "c" 3})))
  (let [with-metas (with-meta
                     {:a 1 :b 2 :c (with-meta {:zz :top} {:m false})}
                     {:m true})]
  (is (= {:m true} (meta (deep-sort with-metas))))
  (is (= {:m false} (meta (get (deep-sort with-metas) :c))))))

(deftest How-about-some-roll-ining-testing-and-also-roll-outing
  (let [seq-of-vecs [[:a :b 3] [:a :c 4] [:x :z 0] [:a :c 2]]
        set-of-vecs (disj (into (hash-set) seq-of-vecs) [:a :c 4])]
    (testing "Basic roll-in"
      (is (= {:a {:b 3 :c 2} :x {:z 0}}
             (roll-in seq-of-vecs)
             (roll-in (fn [_ b] b) seq-of-vecs)))
      (is (= {:a {:b 3 :c 6} :x {:z 0}}
             (roll-in (fnil + 0) seq-of-vecs))))
    (testing "Some basic roll-out"
      (is (= set-of-vecs
             (into (hash-set) (roll-out {:a {:b 3 :c 2} :x {:z 0}})))))
    (testing "Time for some there and back"
      (is (= set-of-vecs
             (->> seq-of-vecs
                  (roll-in)
                  (roll-out)
                  (into (hash-set)))))
      (is (= {:a {:b 4 :c 4} :x {:z 0}}
             (->> {:a {:b 4 :c 4} :x {:z 0}}
                  (roll-out)
                  (roll-in)
                  (roll-out)
                  (roll-in))))) ;; yeah, OK.
      (is (= [[:x :z 0] [:a {:b 3 :c 6}]]
             (->> seq-of-vecs
                  (roll-in (fnil + 0))
                  (roll-out #(contains? % :b)))))))

(deftest Parametrized-parallel-whaaa

  (let [f  (fn f [& args]
             args)
        p? (fn p? [arg]
             (odd? arg))
        x  (fn x [arg]
             (or (and (odd? arg) arg) nil))
        xx (fn xx [idx arg]
             (or (and (odd? arg) [idx arg]) nil))
        c1 (vec (range 10))
        c2 [:a :b :c :d :e :f :g :h :i :j]
        c3 (mapv name c2)]
    (testing "parametrized-parallel-map"
      (is (= (into [] (map f) c1)
             (into [] (pp-map 1 f) c1)
             (into [] (pp-map 4 f) c1)
             (into [] (pp-map 16 f) c1)))
      (is (= (sequence (map f) c1)
             (sequence (pp-map 1 f) c1)
             (sequence (pp-map 4 f) c1)
             (sequence (pp-map 16 f) c1)))
      (is (= (mapv f c1)
             (vec (pp-map 1 f c1))
             (vec (pp-map 4 f c1))
             (vec (pp-map 16 f c1))))
      (is (= (mapv f c1 (take 2 c2))
             (vec (pp-map 1 f c1 (take 2 c2)))))
      (is (= (mapv f c1 c2 c3)
             (vec (pp-map 1 f c1 c2 c3))
             (vec (pp-map 4 f c1 c2 c3))
             (vec (pp-map 16 f c1 c2 c3))))
      (is (= (mapv f [])
             (vec (pp-map 1 f []))
             (mapv f c1 [])
             (vec (pp-map 1 f c1 [])))))
    (testing "parametrized-parallel-filter"
      (is (= (into [] (filter p?) c1)
             (into [] (pp-filter 1 p?) c1)
             (into [] (pp-filter 4 p?) c1)
             (into [] (pp-filter 16 p?) c1)))
      (is (= (sequence (filter p?) c1)
             (sequence (pp-filter 1 p?) c1)
             (sequence (pp-filter 4 p?) c1)
             (sequence (pp-filter 16 p?) c1))
          (is (= (filterv p? c1)
                 (vec (pp-filter 1 p? c1))
                 (vec (pp-filter 4 p? c1))
                 (vec (pp-filter 16 p? c1)))))
      (is (empty? (pp-filter 1 p? []))))
    (testing "parametrized-parallel-mapcat"
      (is (= (into [] (mapcat (juxt dec identity inc)) c1)
             (into [] (pp-mapcat 4 (juxt dec identity inc)) c1)))
      (is (= [0 1 2 1 2 3 2 3 4 3 4 5]
             (vec (pp-mapcat 4 (juxt dec identity inc) [1 2 3 4]))
             (vec (mapcat (juxt dec identity inc) [1 2 3 4])) ))
      (is (= [] (pp-mapcat 4 identity []) (mapcat identity []))))
    (testing "parametrized-parallel-remove"
      (is (= (into [] (remove p?) c1)
             (into [] (pp-remove 4 p?) c1)))
      (is (= (vec (remove p? c1))
             (vec (pp-remove 4 p? c1))))
      (is (= (vec (remove p? []))
             (vec (pp-remove 4 p? [])))))
    (testing "parametrized-parallel-keep"
      (is (= (into [] (keep x) c1)
             (into [] (pp-keep 4 x) c1)))
      (is (= (vec (keep x c1))
             (vec (pp-keep 4 x c1))))
      (is (= (vec (keep x []))
             (vec (pp-keep 4 x [])))))
    (testing "parametrized-parallel-map-indexed"
      (is (= (into [] (map-indexed xx) c1)
             (into [] (pp-map-indexed 1 xx) c1)
             (into [] (pp-map-indexed 4 xx) c1)
             (into [] (pp-map-indexed 16 xx) c1)))
      (is (= (vec (map-indexed xx c1))
             (vec (pp-map-indexed 1 xx c1))
             (vec (pp-map-indexed 4 xx c1))
             (vec (pp-map-indexed 16 xx c1))))
      (is (= (vec (map-indexed xx []))
             (vec (pp-map-indexed 1 xx []))
             (vec (pp-map-indexed 4 xx []))
             (vec (pp-map-indexed 16 xx [])))))
    (testing "parametrized-parallel-keep-indexed"
      (is (= (sequence (keep-indexed xx) c1)
             (sequence (pp-keep-indexed 1 xx) c1)
             (sequence (pp-keep-indexed 4 xx) c1)
             (sequence (pp-keep-indexed 16 xx) c1)))
      (is (= (into [] (keep-indexed xx) c1)
             (into [] (pp-keep-indexed 1 xx) c1)
             (into [] (pp-keep-indexed 4 xx) c1)
             (into [] (pp-keep-indexed 16 xx) c1)))
      (is (= (vec (keep-indexed xx c1))
             (vec (pp-keep-indexed 1 xx c1))
             (vec (pp-keep-indexed 4 xx c1))
             (vec (pp-keep-indexed 16 xx c1))))
      (is (= (vec (keep-indexed xx []))
             (vec (pp-keep-indexed 1 xx []))
             (vec (pp-keep-indexed 4 xx []))
             (vec (pp-keep-indexed 16 xx [])))))))

(deftest Fluff-tests-heyooo

  (testing "all"
    (is (every? (all number? even?) [2 4 6]))
    (is (not (every? (all number? even?) [1 4 6])))
    (is (not (every? (all number? even?) [2 4 :odd!])))
    (is (not (every? (all number? even?) [2 4 7]))))

  (testing "any"
    (is (every? (any number? string?) [2 4 6]))
    (is (every? (any number? string?) [1 4 6]))
    (is (not (every? (any number? string?) [2 4 :odd!])))
    (is (every? (any number? keyword?) [2 4 :odd!]))))
