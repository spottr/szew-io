; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns szew.io.fu-test
  "Wrappers for java.nio.file Path and Files utilities test suite."
  (:require
    [clojure.java.io :as cio]
    [szew.io.fu
     :refer [copy-option link-option open-option visit-option
             select-options lookup-option
             link-opts copy-opts open-opts visit-opts
             string->perms perms->string perms->attr
             ;; Conversions
             to-path to-uri to-url to-file to-string
             ;; predicates
             absolute?  directory? executable? exists? not-exists? file?
             hidden?  readable? same-file? symbolic-link? writable?
             starts-with? ends-with?
             ;; Files ops
             create-directories! create-directory! create-file!
             create-link! create-symbolic-link!
             create-temp-directory! create-temp-directory-at!
             create-temp-file! create-temp-file-at!
             delete! delete-if-exists! delete-on-exit!
             copy! stream-in! stream-out! move!
             ;; Path ops
             path file-name basename parent-path root-path absolute-path
             real-path file-system path-name-count path-name-at subpath
             path-elements normalize-path relativize-path resolve-path
             resolve-sibling-path
             ;; Attributes
             attrs-struct attrs attr size file-store last-modified-time owner
             posix-perms probe-content-type
             ;; Locations
             cwd fs-roots
             ;; Path setters
             attr! last-modified-time! owner! posix-perms!
             ;; Misc helpers
             charset-please path->bytes path->lines path->string
             dir->stream dir->vec bytes->path lines->path string->path
             ;; IO
             buffered-reader buffered-writer
             input-stream output-stream
             ;; BONUS
             pruning-path-seq delete-tree! copy-tree!
             url-encode url-decode url-encode-path url-decode-path
             is-empty? all-subpaths sane-path mtime mtime!]]
    [clojure.test :refer [deftest testing is]]
    [orchestra.spec.test :as st])
  (:import [java.io File]
           [java.nio.file Path Paths]
           [clojure.lang ExceptionInfo]
           [java.time Instant]))

;; NOTE: Most of these do not test the logic of the underlying calls, rather
;; the wrapper call-site implementation. The objective is to uncover any wrong
;; types and bad call signatures that might've been caused by clojurification
;; efforts.

(st/instrument)

(deftest Options-helpers-testing-arena

  (testing "Option selection and lookup"
    (is (= 0 (count (select-options copy-option [:unkown]))))
    (is (= 1 (count (select-options copy-option [:atomic-move :unkown]))))
    (is (= 0 (count (select-options link-option [:unkown]))))
    (is (= 1 (count (select-options link-option [:nofollow-links :unkown]))))
    (is (= 0 (count (select-options open-option [:unkown]))))
    (is (= 1 (count (select-options open-option [:create :unkown]))))
    (is (= 0 (count (select-options visit-option [:unkown]))))
    (is (= 1 (count (select-options visit-option [:follow-links :unkown]))))
    (is (= [:atomic-move :nofollow-links :create :follow-links nil]
           (mapv lookup-option
                 [java.nio.file.StandardCopyOption/ATOMIC_MOVE
                  java.nio.file.LinkOption/NOFOLLOW_LINKS
                  java.nio.file.StandardOpenOption/CREATE
                  java.nio.file.FileVisitOption/FOLLOW_LINKS
                  :what-am-I-doing-here-guys]))))

  (testing "Copy options"
    (let [ordered (vec copy-option)
          kw-keys (mapv first ordered)
          en-vals (mapv second ordered)]
      (is (= en-vals (vec (copy-opts kw-keys))))
      (is (= en-vals (vec (copy-opts en-vals))))
      (is (not (seq (copy-opts [:junk-values :are-dropped]))))))

  (testing "Link options"
    (let [ordered (vec link-option)
          kw-keys (mapv first ordered)
          en-vals (mapv second ordered)]
      (is (= en-vals (vec (link-opts kw-keys))))
      (is (= en-vals (vec (link-opts en-vals))))
      (is (not (seq (link-opts [:junk-values :are-dropped]))))))

  (testing "Open options"
    (let [ordered (vec open-option)
          kw-keys (mapv first ordered)
          en-vals (mapv second ordered)]
      (is (= en-vals (vec (open-opts kw-keys))))
      (is (= en-vals (vec (open-opts en-vals))))
      (is (not (seq (open-opts [:junk-values :are-dropped]))))))

  (testing "Visit options"
    (let [ordered (vec visit-option)
          kw-keys (mapv first ordered)
          en-vals (mapv second ordered)]
      (is (= en-vals (vec (visit-opts kw-keys))))
      (is (= en-vals (vec (visit-opts en-vals))))
      (is (not (seq (visit-opts [:junk-values :are-dropped])))))))

(deftest POSIX-file-permissions
  (let [p "rwxr-xr-x"]
    (is (= p (perms->string (string->perms p)))))
  (let [p "---------"]
    (is (= p (perms->string (string->perms p)))))
  (is (not (nil? (perms->attr (string->perms "rwx------"))))))

(deftest Conversions-testing-extravaganza

  (testing "Protocol extensions: nil"
    (is (nil? (to-path nil)))
    (is (nil? (to-uri nil)))
    (is (nil? (to-url nil)))
    (is (nil? (to-file nil)))
    (is (= "" (to-string nil))))

  (let [s "."
        f (File. s)
        i (.toURI f)
        l (.toURL i)
        p (Paths/get s (into-array String []))]
    (testing "Protocol extensions: Path"
      (is (same-file? p (to-path p)))
      (is (same-file? i (to-uri p)))
      (is (same-file? l (to-url p)))
      (is (same-file? f (to-file p)))
      (is (same-file? s (to-string p))))
    (testing "Protocol extensions: URI"
      (is (same-file? p (to-path i)))
      (is (same-file? i (to-uri i)))
      (is (same-file? l (to-url i)))
      (is (same-file? f (to-file i)))
      (is (same-file? s (to-string i))))
    (testing "Protocol extensions: URL"
      (is (same-file? p (to-path l)))
      (is (same-file? i (to-uri l)))
      (is (same-file? l (to-url l)))
      (is (same-file? f (to-file l)))
      (is (same-file? s (to-string l))))
    (testing "Protocol extensions: File"
      (is (same-file? p (to-path f)))
      (is (same-file? i (to-uri f)))
      (is (same-file? l (to-url f)))
      (is (same-file? f (to-file f)))
      (is (same-file? s (to-string f))))
    (testing "Protocol extensions: String"
      (is (same-file? p (to-path s)))
      (is (same-file? i (to-uri s)))
      (is (same-file? l (to-url s)))
      (is (same-file? f (to-file s)))
      (is (same-file? s (to-string s))))
    (testing "Coercions via clojure.java.io"
      (is (same-file? p (cio/as-file p)))
      (is (same-file? p (cio/as-url p))))))

(deftest Predicates-and-modifiers

  (testing "Path creation wrapper with a twist."
    (let [p (path "datasets")]
      (is (same-file? p (path "datasets")))
      (is (same-file? p (path p)))
      (is (same-file? p (path (to-file p))))
      (is (same-file? p (path (to-string p))))
      (is (same-file? p (path (to-uri p))))
      (is (same-file? p (path (to-url p))))))

  (testing "Predicates: just stretching the wrapper, not testing logic."
    (let [p (path "datasets")]
      (is (not (absolute? p)))
      (is (absolute? (absolute-path p)))
      (is (directory? p))
      (is (or (not (executable? p)) (executable? p)))
      (is (exists? p))
      (is (not (exists? (resolve-path p "super-random-nx-file"))))
      (is (not (not-exists? p)))
      (is (not-exists? (resolve-path p "super-random-nx-file")))
      (is (not (file? p)))
      (is (not (hidden? p)))
      (is (readable? p))
      (is (same-file? p (path (to-string p) ".." (to-string p))))
      (is (not (same-file? p (path (to-string p) ".."))))
      (is (not (symbolic-link? p)))
      (is (or (not (writable? p)) (writable? p)))
      (is (starts-with? p p))
      (is (ends-with? p p))
      (is (not (starts-with? (absolute-path p) p)))
      (is (ends-with? (absolute-path p) p))))

  (testing "Some lifting here and there"
    (let [p (path "datasets")]
      (is (= (to-string p) (basename p)))
      (is (= (to-string p) (basename (absolute-path p)))))))

(deftest Attributes-etc

  (testing "Reading basic attributes."
    (let [from (path "datasets/example.csv")
          as   (attrs from "size,lastModifiedTime,isDirectory,isRegularFile")]
      (is (= (size from) (get as "size")))
      (is (= (size from) (attr from "size")))
      (is (= (last-modified-time from) (get as "lastModifiedTime")))
      (is (= (last-modified-time from) (attr from "lastModifiedTime")))
      (is (= (directory? from) (get as "isDirectory")))
      (is (= (directory? from) (attr from "isDirectory")))
      (is (= (file? from) (get as "isRegularFile")))
      (is (= (file? from) (attr from "isRegularFile")))
      (is (instance? java.nio.file.attribute.UserPrincipal
                     (owner from)))
      (is (instance? java.nio.file.attribute.UserPrincipal
                     (owner from [:nofollow-links])))
      (is (instance? java.nio.file.attribute.BasicFileAttributes
                     (attrs-struct from
                                   java.nio.file.attribute.BasicFileAttributes
                                   [])))
      (is (instance? java.nio.file.FileStore (file-store from)))
      (is (= "text/csv" (probe-content-type from)))
      (is (seq (posix-perms from))))) ;; let's see where this breaks... /wink

  (testing "Writing basic attributes maybe?"
    (let [from (path "datasets/example.csv")
          to   (create-temp-file! "attr-writing-test" ".csv")]
      (delete-on-exit! to)
      (copy! from to [:replace-existing :copy-attributes])
      (is (= (owner from) (owner to)))
      (is (= (posix-perms from) (posix-perms to)))
      (is (= (basename to) (basename (owner! to (owner from)))))
      (is (= (basename to)
             (basename (posix-perms! to (string->perms "rwxrwxrwx")))))
      (is (= "rwxrwxrwx" (perms->string (posix-perms to))))
      (is (= (basename to)
             (basename
               (attr! to "lastModifiedTime" (attr from "lastModifiedTime")))))
      (is (= (basename to)
             (basename (last-modified-time! to (last-modified-time from))))))))

(deftest Path-manipulation-operations

  (testing "Field orientation!"
    (let [p (path "datasets/example.csv")]
      (is (instance? java.nio.file.Path p))
      (is (= "example.csv" (to-string (file-name p))))
      (is (= "example.csv" (basename p)))
      (is (= "datasets" (basename (parent-path p))))
      (is (nil? (root-path (path "datasets"))))
      (is (= (path "/") (root-path (path "/datasets"))))
      (is (not (absolute? p)))
      (is (absolute? (absolute-path p)))
      (is (same-file? p (absolute-path p)))
      (is (same-file? p (real-path p)))
      (is (not (nil? (file-system p)))) ;; yeah
      (is (= 2 (path-name-count p)))
      (is (= "datasets" (to-string (path-name-at p 0))))
      (is (= "example.csv" (to-string (path-name-at p 1))))
      (is (= (path "datasets") (subpath p 0 1)))
      (is (= (path "example.csv") (subpath p 1 2)))
      (is (= [(path "datasets") (path "example.csv")] (path-elements p)))
      (is (= p (normalize-path "datasets/../datasets/./example.csv")))
      (is (same-file? p (normalize-path "datasets/../datasets/./example.csv")))
      (is (= (path "..") (relativize-path p (path "datasets"))))
      (is (= (path "example.csv") (relativize-path (path "datasets") p)))
      (is (= p (resolve-path (path "datasets") (path "example.csv"))))
      (is (same-file? p (resolve-path (path "datasets") (path "example.csv"))))
      (is (= (path "datasets/extra.file")
             (resolve-sibling-path p (path "extra.file")))))))

(deftest Files-based-io-operations

  (let [prefix "test-prefix"
        suffix "-test.file"
        tmp-d  (create-temp-directory! prefix)
        tmp-da (create-temp-directory-at! tmp-d prefix)
        tmp-f  (create-temp-file! prefix suffix)
        tmp-f2 (resolve-sibling-path tmp-f (str "sibling-" (basename tmp-f)))
        tmp-fa (create-temp-file-at! tmp-da prefix suffix)
        sub-d  (create-directory! (resolve-sibling-path tmp-fa "sub-d"))
        sub-ds (create-directories! (resolve-sibling-path tmp-fa "a/b/c/d"))
        sub-f  (create-file! (resolve-path sub-ds "deep.file"))
        ps     [tmp-d tmp-da tmp-fa sub-d sub-ds sub-f]
        ps+    (conj ps tmp-f)]

    (testing "clojure.java.io IOFactory glue"
      (is (try
            (with-open [_ (cio/reader (path sub-f))]
              true)
            (catch Exception _ false)))
      (is (try
            (with-open [_ (cio/writer (path sub-f))]
              true)
            (catch Exception _ false))))

    (testing "Basic presence sanity checks."
      (is (every? exists? ps+))
      (let [pseq    (vec (pruning-path-seq tmp-d))
            deleted (vec (delete-tree! tmp-d))]
        (is (= 9 (count deleted)))
        (is (= (set (map basename deleted))
               (set (map basename pseq)))))
      (is (every? not-exists? ps))
      (is (nil? (delete-on-exit! tmp-f)))
      (is (exists? tmp-f))
      (is (not-exists? tmp-f2)))

    (testing "Copy operation"
      (is (= (basename tmp-f2)
             (basename (copy! tmp-f tmp-f2 [:copy-attributes]))))
      (is (exists? tmp-f2))
      (is (delete-if-exists! tmp-f2))
      (is (not-exists? tmp-f2))
      (is (= (basename tmp-f2) (basename (copy! tmp-f tmp-f2))))
      (is (exists? tmp-f2))
      (is (nil? (delete! tmp-f2)))
      (is (not-exists? tmp-f2))
      (is (number?
            (with-open [is ^java.io.InputStream (input-stream tmp-f)]
              (stream-in! is tmp-f2 [:replace-existing]))))
      (is (exists? tmp-f2))
      (is (nil? (delete! tmp-f2)))
      (is (not-exists? tmp-f2))
      (is (number?
            (with-open [os ^java.io.OutputStream (output-stream tmp-f2)]
              (stream-out! tmp-f os))))
      (is (exists? tmp-f2))
      (is (nil? (delete! tmp-f2)))
      (is (not-exists? tmp-f2)))

    (testing "Move operation"
      (is (= (basename tmp-f2) (basename (move! tmp-f tmp-f2 [:atomic-move]))))
      (is (exists? tmp-f2))
      (is (not-exists? tmp-f))
      (is (= (basename tmp-f) (basename (move! tmp-f2 tmp-f))))
      (is (exists? tmp-f))
      (is (not-exists? tmp-f2)))
      (is (= 4 (with-open [ls ^java.util.stream.Stream
                           (path->lines "datasets/example.csv" "UTF-8")]
                 (count (iterator-seq (.iterator ls))))))
      (is (.startsWith (path->string "datasets/example.csv" "UTF-8")
                       "Name,"))
      (is (.startsWith (String. (path->bytes "datasets/example.csv") "UTF-8")
                       "Name,"))

    (testing "Wax in, wax out operations... or something."
      (is (= (charset-please "UTF-8")
             (charset-please (charset-please "UTF-8"))))
      (is (= (basename tmp-f2)
             (basename
               (bytes->path tmp-f2
                            (path->bytes "datasets/example.csv")
                            [:write :create]))))
      (is (.startsWith (String. (path->bytes tmp-f2) "UTF-8")
                       "Name,"))
      (is (nil? (delete! tmp-f2)))
      (is (= (basename tmp-f2)
             (basename
               (with-open [ls ^java.util.stream.Stream
                           (path->lines "datasets/example.csv")]
                 (lines->path tmp-f2
                              (iterator-seq (.iterator ls))
                              [:write :create])))))
      (is (.startsWith (path->string tmp-f2 "UTF-8")
                       "Name,"))
      (is (nil? (delete! tmp-f2)))
      (is (= (basename tmp-f2)
             (basename
               (string->path tmp-f2
                             (path->string "datasets/example.csv" "UTF-8")
                             [:write :create]))))
      (is (.startsWith (path->string tmp-f2 "UTF-8")
                       "Name,"))
      (is (nil? (delete! tmp-f2)))
      (with-open [br (buffered-reader "datasets/example.csv" "UTF-8")
                  bw (buffered-writer tmp-f2 "UTF-8" [:write :create])]
        (loop [l (.readLine br)]
          (when-not (nil? l)
            (.write bw l)
            (.newLine bw)
            (recur (.readLine br)))))
      (is (.startsWith (path->string tmp-f2 "UTF-8")
                       "Name,")))

    (testing "Directory listing"
      (let [fs (with-open [d (dir->stream (path "datasets/"))]
                 (vec (iterator-seq (.iterator d))))
            fv (dir->vec (path "datasets/"))]
        (is (contains? (set (map basename fs)) "example.csv"))
        (is (contains? (set (map basename fv)) "example.csv"))))

    (testing "Links soft and hard!"
      (let [p (path "datasets/example.csv")
            t (create-temp-file! "example-test-" ".csv")
            l (resolve-sibling-path t "symbolic-link-test")
            h (resolve-sibling-path t "hard-link-test")]
        (delete-on-exit! t)
        (is (= (basename t) (basename (copy! p t [:replace-existing]))))
        (is (exists? t))
        (is (= (path->string p) (path->string t)))
        (let [l (create-symbolic-link! l t)
              h (create-link! h t)]
          (is (exists? l))
          (is (exists? h))
          (is (same-file? t l))
          (is (same-file? t h))
          (is (symbolic-link? l))
          (is (not (symbolic-link? h)))
          (delete-if-exists! l)
          (delete-if-exists! h))))

      (delete-if-exists! tmp-f)
      (delete-if-exists! tmp-f2)))

(deftest Some-leftover-things
  (testing "Tiny bits of URL encode/decode"
    (let [s "zażółć-gęślą-jaźń?x=1&y=1"]
      (is (not= s (url-encode s)))
      (is (= s (url-decode (url-encode s))))))
  (testing "Bigger bits of URL encode/decode paths"
    (let [absie (path "/mary/had/a/little/ląmb")
          uglie (path "C:\\Windows\\System32\\badrobót")
          basie (path "żążółć/gęślą/jaźń")]
      (is (= absie (url-decode-path (url-encode-path absie))))
      (is (= uglie (url-decode-path (url-encode-path uglie))))
      (is (= basie (url-decode-path (url-encode-path basie))))))
  (testing "Small fries!"
    (is (same-file? (path ".") (cwd)))
    (is (every? #(instance? java.nio.file.Path %) (fs-roots))))
  (testing "Getting it: all-subpaths maybe?"
    (is (= [(path "/a/b/c") (path "/a/b") (path "/a") (path "/")]
           (all-subpaths "/a/b/c"))))
  (testing "Sanity"
    (is (= (path "/a/b/c/e") (sane-path "/a/b/c/d/.././e"))))
  (testing "Last modified time... but java.time.Instant"
    (let [now (Instant/now)
          tmp (create-temp-file! "mtime-test-" ".txt")
          mtm (mtime tmp)]
      (delete-on-exit! tmp)
      (mtime! tmp now)
      (is (= (mtime tmp) now))
      (mtime! tmp mtm)
      (is (= (mtime tmp) mtm)))))

(deftest Emptiness-testing-galore
  (testing "State of emptiness: directories, files, symbolic links."
    (let [prefix "test-prefix"
          suffix "-test.file"
          tmp-d  (create-temp-directory! prefix)
          tmp-d2 (create-temp-directory-at! tmp-d prefix)
          tmp-f  (create-temp-file-at! tmp-d prefix suffix)
          tmp-f2 (create-temp-file-at! tmp-d prefix suffix)
          tmp-l  (create-symbolic-link! (resolve-sibling-path tmp-f "l") tmp-f)
          tmp-l2 (create-symbolic-link!
                   (resolve-sibling-path tmp-f2 "l2") tmp-f2)]
      (is (and (exists? tmp-d) (not (is-empty? tmp-d))))
      (is (and (exists? tmp-d2) (is-empty? tmp-d2)))
      (is (and (exists? tmp-f) (is-empty? tmp-f)))
      (spit (to-file tmp-f2) "content!")
      (is (and (exists? tmp-f2) (not (is-empty? tmp-d))))
      (is (and (exists? tmp-l) (is-empty? tmp-l)))
      (is (and (exists? tmp-l2) (not (is-empty? tmp-l2))))
      (delete! tmp-f)
      (is (and (not-exists? tmp-f) (true? (is-empty? tmp-l))))
      (delete! tmp-f2)
      (is (and (not-exists? tmp-f2) (true? (is-empty? tmp-l2))))
      (is (= [] (vec (delete-tree! tmp-d2 true))))
      (is (= 3 (count (delete-tree! tmp-d true))))
      (is (and (not-exists? tmp-d2) (is-empty? tmp-d2)))
      (is (and (exists? tmp-d) (is-empty? tmp-d)))
      (dorun (delete-tree! tmp-d)))))

(deftest Kage-bunshin-no-jutsu-tree-stuffs
  (testing "Klone it: directories, files, symbolic links."
    (let [prefix "test-prefix"
          suffix "-test.file"
          tmp-d  (create-temp-directory! prefix)
          _      (create-temp-directory-at! tmp-d prefix)
          tmp-f  (create-temp-file-at! tmp-d prefix suffix)
          tmp-f2 (create-temp-file-at! tmp-d prefix suffix)
          _      (create-symbolic-link! (resolve-sibling-path tmp-f "l") tmp-f)
          _      (create-symbolic-link!
                  (resolve-sibling-path tmp-f2 "l2") tmp-f2)
          tmp-dc (create-temp-directory! "clone-test-prefix")
          pairs  (vec (copy-tree! tmp-d tmp-dc))]
      (is (thrown? ExceptionInfo
                   (dorun (copy-tree! (resolve-path tmp-d "sub") tmp-dc))))
      (is (thrown? ExceptionInfo
                   (dorun (copy-tree! tmp-d (resolve-path tmp-d "sub")))))
      (is (every? #(and (exists? (first %)) (exists? (second %)))
                  pairs))
      #_(is (= (attrs (first (first pairs)) "*")
             (attrs (second (first pairs)) "*")))
      (doseq [[s t] (drop 1 pairs)]
        (is (= (.toInstant (last-modified-time s))
               (.toInstant (last-modified-time t)))))
      ;; done
      (is (= (count (delete-tree! tmp-d))
             (count (delete-tree! tmp-dc)))))))
