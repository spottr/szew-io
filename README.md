# szew/io

File to data and back.

[![szew/io](https://clojars.org/szew/io/latest-version.svg)](https://clojars.org/szew/io)

[API documentation][latest] | [Changelog][changelog]

_Please note: API breakage will happen in minor versions (0.X.0) before 1.0.0
is released. Only patch versions (0.n.X) are safe until then._

** Table of Contents **

[TOC]

---

## Why

I work with data files daily, so I assembled this handy wrapper for some common
formats and tasks.

## TL;DR

REPL oriented library for reading and writing common file formats swiftly.

Two concepts:

- `(Input/in! spec source)` parse source and feed user provided `:processor`,
then return its result, think:

```clojure
(with-open [r (reader source :encoding encoding)]
  (processor (parse r)))))

```

- `(Output/sink spec target)` produces a callable that will consume a sequence,
writing it to target, then return nothing:

```clojure
(with-open [w (writer target :encoding encoding :append append)]
  (doseq [output (unparse a-seq)]
    (.write w output)))

```

Plugging in `sink` callable into `:processor` allows data round-tripping and 
file conversion.

### Formats

Each constructor carries documentation for its spec, currently these are:

- `Lines`, constructed with `lines`

    * Input: text file in, sequence of Strings out
    * Output Sequence of Strings in, target text file propagated

- `DSV` (D is for Delimiter), constructed with `csv` or `tsv`

    * Input: DSV in, sequence of vectors of Strings out
    * Output: sequence of vectors of Strings in, DSV target propagated

- `FixedWidth`, constructed with `fixed-width`

    * Input: fixed width lines in, sequence of vectors of Strings out
    * Output: sequence of vectors of Strings in, fixed width file propagated

- `XML`, constructed with `xml`

    * Input: XML in, `data.xml/parse` result out
    * Output: data in, `data.xml/emit` put in target

- `EDN`, constructed with `edn`

    * Input: EDN in, vector of read objects out.
    * Output: sequence of objects in, EDN written to file.

- `Files`, constructed with `files`

    * Input: file or directory in, sequence of files out
    * Output: N/A

- `Paths`, constructed with `paths`

    * Input: path or directory in, sequence of paths out
    * Output: N/A

- `Hasher`, constructed with `hasher`

    * Input: file in, hash out
    * Output: N/A

- `FastHasher`, constructed with `fast-hasher`

    * Input: file in, Adler32 checksum out
    * Output: N/A

### Input Processing

You prepare a processor that is a data eating function or composition of such
functions. You shove that into a spec, it is then fed data while your source
file open. Just remember to let go of the head if you're short on memory!

```clojure
(require '[szew.io :as io])

(let [proc (partial into [] (comp (drop 2) (take 2)))]
  (println (io/in! (io/tsv {:processor proc}) "input.csv")))

;; => displays vector of third and fourth rows of input.csv

;; Direct call to the spec delegates to in!
(let [proc (partial into [] (comp (drop 2) (take 2)))
      tsv! (io/tsv {:processor proc})]
  (println (tsv! "input.csv")))

;; => displays vector of third and fourth rows of input.csv

```

Or simply, input `datasets/example.csv` (ignore the final \newline):

```csv
Name,Surname,Awesome
Dio,Brando,no
Jonathan,Joestar,
Robert E. O.,Speedwagon,yes
```

With a bunch of requires:

```clojure
(require '[szew.io :refer [csv]])
(require '[szew.io.util :refer [vecs->maps maps-maker]])
```

Can be processed with:

```clojure
((csv) "datasets/example.csv")
```

Result:

```clojure
[["Name" "Surname" "Awesome"]
 ["Dio" "Brando" "no"]
 ["Jonathan" "Joestar" ""]
 ["Robert E. O." "Speedwagon" "yes"]]
```

Or:

```clojure
((csv {:processor (comp vec vecs->maps)}) "datasets/example.csv")
```

Which gives:

```clojure
[{"Awesome" "no" "Name" "Dio" "Surname" "Brando"}
 {"Awesome" "" "Name" "Jonathan" "Surname" "Joestar"}
 {"Awesome" "yes" "Name" "Robert E. O." "Surname" "Speedwagon"}]
```

Or even:

```clojure
((csv {:processor (comp vec (maps-maker))}) "datasets/example.csv")
```

To get:

```clojure
[{:awesome "no" :name "Dio" :surname "Brando"}
 {:awesome "" :name "Jonathan" :surname "Joestar"}
 {:awesome "yes" :name "Robert E. O." :surname "Speedwagon"}]
```

These are lazy by default, so `vec` is used to realize them.

### Output Processing

On the other hand you've got output sink creators. That will accept spec
and path, giving you a callable that will consume a sequence and dump into
target output file.

```clojure
(require '[szew.io :refer [sink csv tsv in!]])

(let [hole (sink (tsv) "out.tsv")]
  (in! (csv {:processor hole}) "datasets/example.csv"))

;; => returns nil, converts TSV into CSV

;; alternatively, shorthand:

((csv {:processor (sink (tsv) "out.tsv")}) "datasets/example.csv")

;; Or from 0.4.0, shorter shorthand:

(csv {:processor (sink (tsv) "out.tsv")} "datasets/example.csv")
```

## Usage

Simple composed partials:

```clojure
(require '[szew.io :as io])
(require '[szew.io.util :as io.util])

;; A seq of lines from input.txt, processed with composed
;; functions and written to out.txt

(def p (comp (io/sink (io/lines) "out.txt")
             (partial take 10)
             (partial filter true?)
             (partial map #(or % false))
             (partial drop 1)))

(io/in! (io/lines {:processor #'p}) "input.txt")

;; A seq of vectors from in.csv, processed with composed functions
;; and written to out.tsv
(let [adj (io.util/row-adjuster ["default #1" "default #2" "default #3"])
      out (io/sink (io/tsv) "out.tsv")
      pro (comp out
                (partial cons ["col #1" "col #2" "col #3"])
                (partial map adj)
                (partial take 10)
                (partial filter true?)
                (partial map #(or % false))
                (partial drop 1))]
  (io/in! (in/csv {:processor pro, :strict true}) "input.csv"))

```

### Predicates And Path Operations

Namespace `szew.io.fu` wraps some Java7+ functionality provided by path
abstractions of `java.nio.file.Path` and included in `java.nio.file.Files`.
Most of `Files` static methods and `Path` helper methods have been wrapped
and made more clojure friendly (keyword arguments for file and path access
option allowing for pass-through of non-keyword arguments, some eager Stream
realizations etc.). Reflection warnings have been eliminated with type hinting,
specs are minimal. See [API docs][latest] for details.

### Utilities

The `szew.io.util` namespace contains several convenience functions to ease
transitioning between vectors, maps and back. It also hosts several other
little helpers, like `juxt-map`, `getter`, `friendlify`, `roll-in`
and `roll-out`. There's also `deep-sort` that tries to make maps and
sets ordered recursively, while leaving other collections untouched.

#### Parametrized Multi-Threaded Sequence Functions

Most importantly `szew.io.util` contains some parametrized multi-threaded
versions of `clojure.core` sequence functions:

* `map` -> `pp-map`
* `filter` -> `pp-filter`
* `mapcat` -> `pp-mapcat`
* `remove` -> `pp-remove`
* `keep` -> `pp-keep`
* `map-indexed` -> `pp-map-indexed`
* `keep-indexed` -> `pp-keep-indexed`

Change in functions call signature is `n` parameter, which signifies how many
threads should be used for the call.

When called without collection these produce a stateful transducer, like their
core counterparts.

## Development

From version 0.3.0 `szew.io` depends on Clojure 1.9.0 and `spec.alpha`.

Non-core libraries wrapped:

* [clojure-csv][clojure-csv]
* [org.clojure/data.xml][data.xml]
* [org.clojure/data.zip][data.zip]
* [camel-snake-kebab][csk]

Testing is done with [eftest][eftest].

## Known Issues, Limitations And Breakage

### Illegal Reflection Warning In XML Processing.

Unfortunately `data.xml` causes JDK9+ to have a fit about some reflections.
Clojure FAQ [Why do I get an illegal access warning?][FAQia] entry uses
the very call as example. Adding `--illegal-access=deny` is recommended until
this is fixed in upstream. For Clojure 1.9.0 keep that option at `warn`.

### Breaking Changes

Some breaking changes were introduced in version 0.4.0, please review
[changelog][changelog].

## License

Copyright © Sławek Gwizdowski

MIT License, text can be found in the LICENSE file.

[latest]: http://spottr.bitbucket.io/szew-io/latest/
[changelog]: CHANGELOG.md
[clojure-csv]: https://github.com/davidsantiago/clojure-csv
[data.xml]: https://github.com/clojure/data.xml
[data.zip]:https://github.com/clojure/data.zip
[csk]: https://github.com/qerub/camel-snake-kebab
[eftest]: https://github.com/weavejester/eftest
[FAQia]: https://clojure.org/guides/faq#illegal_access
