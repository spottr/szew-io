# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/).
and this project adheres to [Semantic Versioning](https://semver.org/).

## [Unreleased]

...

## [0.5.8] - 2025-02-20

### Fixed

- `szew.io.in!` for `DSV` failed on empty input files, as line indexer was
  not expecting `nil` input in `vary-meta` update.

## [0.5.7] - 2024-07-03

### Fixed

- `szef.fu.copy-tree!` failed to copy last modified time with `:copy-attributes`
  operation. Workaround added to check the presence of that option and bring mtime
  over manually between source and target. Bug spotted by running test suite
  after fixing comments (OpenJDK 17, Debian 12.6), looks like precision loss.
  Known issue: still does not copy timestamp for root location, unsure why.
- Minor comment updates, mainly added whitespace for visibility.

## [0.5.6] - 2022-08-18

### Added

- `clojure.java.io/IOFactory` for `java.nio.file.Path`, making
  `szew.io` functions work on paths automatically.
- `szew.io.FastHasher` based on Adler32.

### Fixed

- Direct native calls in `clojure.java.io/Coercions` glue.
- Added missing specs for `szew.io.Files` and `szew.io.Paths`.
- Spec validation in `Hasher/in!` method.

## [0.5.5] - 2022-06-11

### Fixed

- Further regexp fixes in `szew.io.util/friendlify`
- Faults in `szew.io.util/pp-map-fixed` (missed from 0.5.4)

## [0.5.4] - 2022-04-23

### Fixed

- Bug in `szew.io.util/friendlify` replacement regexp range.
- Faults in `szew.io.util/pp-map` (borks against Clojure 1.11).
- Faults in `szew.io.util/pp-filter` (borks against Clojure 1.11).
- Faults in `szew.io.util/map-indexed` (borks against Clojure 1.11).

## [0.5.3] - 2021-01-01

### Added

- `clojure.java.io/Coercions` glue for `java.nio.file.Path` in `szew.io.fu`.

## [0.5.2] - 2020-12-27

### Added

- `szew.io.fu/copy-tree!` for recursive directory tree copy.
- `szew.io.fu/sane-path` to help a bit with common tranformation.
- `szew.io.fu/mtime` and `szew.io.fu/mtime!` for `java.time.Instant`.

## [0.5.1] - 2020-09-08

### Changed

- QOL: more lenient `szew.io.fu/paths`: arguments are `to-string`ed now.
- QOL: `szew.io.fu/pathable?` predicate added to help with that change.
- QOL: `szew.io.fu/delete-tree!` now has `skip-root?` argument, default `false`.

### Added

- `szew.io.fu/root-path` wrapper for `Path.getRoot` method.
- `szew.io.fu/is-empty?` for directories, files and symbolic links.
- `szew.io.fu/url-encode-path` and `szew.io.fu/url-decode-path`.
- `szew.io.fu/all-subpaths` for enumeration of all ancestors.

## [0.5.0] - 2020-08-29

### Added

- `szew.io.fu` wrapper for `java.nio.file.Files` and `Path` loving friends.
- `szew.io/Paths` and `szew.io/paths` based on `szew.io.fu/pruning-path-seq`.
- `szew.io.util/all` and `szew.io.util/any` higher level predicates.

## [0.4.0] - 2020-03-29

### Changed

- Linted with `clj-kondo` and adressed reported issues.
- Renamed `fields` to `slices` in `szew.io.util/fixed-width-split`.
- Optimized `szew.io.util/vec->map` with defaults: runtime decrease by 30%.
- Optimized `szew.io.util/map->vec` with defaults: runtime decrease by 50%.
- Deprecated `szew.io/prunning-file-seq` shim.
- Deprecated `szew.io.util/ppmap`, use `szew.io.util/pp-map` instead.
- Deprecated `szew.io.util/ppfilter, use `szew.io.util/pp-filter` instead.
- Deprecated `szew.io.util/ppmapcat`, use `szew.io.util/pp-mapcat` instead.
- BREAKING: removed deprecated `szew.io.util/keywordify`.
- BREAKING: removed deprecated `szew.io.util/bastardify`.
- BREAKING: removed deprecated `szew.io.util/recordify` shim.
- BREAKING: removed deprecated `szew.io.util/de-recordify` shim.
- Formatting fixups: saved some vertical space, aligned lets and maps.

### Added

- `clojure.alpha.specs` to functions in `szew.io.util` and tests.
- `szew.io.util/pp-remove` like `clojure.core/remove` but parallel.
- `szew.io.util/pp-keep` like `clojure.core/keep` but parallel.
- `szew.io.util/pp-map-indexed` like `clojure.core/map-indexed` but parallel.
- `szew.io.util/pp-keep-indexed` like `clojure.core/keep-indexed` but parallel.
- Transducer-arity tests for all parallel `szew.io.util` funtions.
- Third "ingest immediately" arity of `[spec source]` to `szew.io/lines`,
  `szew.io/csv`, szew.io/tsv`, `szew.io/fixed-width`, `szew.io/xml`,
  `szew.io/edn`, `szew.io/files` and `szew.io/hasher`.

## [0.3.6] - 2019-03-17

## Changed

- Buffer fill wait eliminated: `szew.io.util/ppmap` & `szew.io.util/ppfilter`.

## [0.3.5] - 2019-03-12

## Fixed

- `szew.io.util/deep-sort` copies meta of sorted maps, skips unsortable.

### Added

- `szew.io/EDN` as wrapper around `clojure.edn`.

## [0.3.4] - 2019-03-05

### Changed

- Improved specs and specs handling in constructors.

## [0.3.3] - 2019-02-21

### Added

- `szew.io.util/ppmapcat`, parametrized parallel mapcat.
- `szew.io.util/deep-sort` to recursively convert maps and sets into sorted.

## [0.3.2] - 2019-01-31

### Added

- `szew.io.util/ppmap`, parametrized parallel map.
- `szew.io.util/ppfilter`, parametrized parallel filter.

## [0.3.1] - 2018-02-24

### Changed

- Fix line numbering in `szew.io.FixedWidth`, now 1-based.

### Added

- `szew.io/string-reader` and `szew.io/pass-through-writer`.

## [0.3.0] -  2018-02-11

### Changed

- Dependency: Clojure 1.9.0 and spec required. Forward testing with 1.10.0 alphas.
- Dependency: awesome `camel-snake-kebab` added.
- BREAKING: `szew.io.util/recordify` now an alias for `rows->maps`.
- BREAKING: `szew.io.util/de-recordify` now an alias for `map->row`(!).
- BREAKING: default EOL is now set to `line.separator` System property.
- BREAKING: `szew.io/pruning-file-seq`: runs `follow?` on `entry` now.
- BREAKING: in Files: empty seq for non-existing root. Flag removed.
- BREAKING: DSV no longer accepts `:eol` of `nil`.
- BREAKING: removed 0-arity from `szew.io.util/fixed-width-split`.
- BREAKING: removed 0-arity from `szew.io.util/row-adjuster`.
- BREAKING: renamed `szew.io.util/rollup` to `roll-in`.
- BREAKING: renamed `szew.io.util/spread` to `roll-out`.
- Deprecation: `szew.io.util/keywordify` and `szew.io.util/bastardify`.
- `szew.io/prunning-file-seq` renamed to `pruning-file-seq`, added alias.

### Added

- `clojure.alpha.spec` contracts to `szew.io` constructors.
- `:fill-char` to `FixedWidth`, used as right-pad during writing.
- `szew.io.util/vec->map` and `szew.io.util/map->vec`, for `meta` justice.
- `szew.io.util/vecs->maps` and `szew.io.util/maps->vecs`.
- `szew.io.util/maps-maker`, `szew.io.util/bespoke-header`,
  `szew.io.util/vecs-maker`.

### Fixed

- `szew.io.util/fixed-width-split`: off by one error.
- removed state atom from tests.

## [0.2.5] - 2017-06-18

### Added

- Added `szew.io.util/rollup` and `szew.io.util/spread`.

## [0.2.4] - 2017-02-11

### Fixed

- Added `encoding` to `data.xml/emit` call in XML `sink`.

### Changed

- Extracted `realizer` from XML's `in!` into `xml-realizer`.

## [0.2.3] - 2017-01-12

### Fixed

- Ignored user `:processor` in XML processing.

## [0.2.2] - 2016-10-08

### Fixed

- `io!` over `szew.io/DSV` counts records now and throws `ex-info` with
  offending rows.

### Changed

- Remove VIM modelines from sources.

## [0.2.1] - 2016-10-01

### Added

- Added license headers to source files.

### Changed

- `szew.io/CSV` renamed to `szew.io/DSV`, API (constructors) unchanged

## [0.2.0] - 2016-09-28

### Added

- Added CHANGELOG.md.

### Changed

- Renamed `szew.io.core` to `szew.io`.

## [0.1.2] - 2016-09-26

### Added

- Added `:aot [szew.io.core]` to global profile, derp.

## [0.1.1] - 2016-09-26

### Added

- Added `:aot :all` to uberjar profile.

## [0.1.0] - 2016-09-25

### Initial

- This is the first extraction from private `szew` toolkit.
- `szew.io.core` namespace defines protocols and formats:
    - Text lines
    - CSV/TSV
    - Fixed width record files
    - XML
    - Files (including `prunning-file-seq`)
    - Single file hasher
    - GZip stream and reader/writer convenience functions
- `szew.io.util` namespace defines some utility functions usable with
  and used by `core`

[Unreleased]: https://bitbucket.org/spottr/szew-io/branches/compare/master%0D0.5.8#diff
[0.5.8]: https://bitbucket.org/spottr/szew-io/branches/compare/0.5.8%0D0.5.7#diff
[0.5.7]: https://bitbucket.org/spottr/szew-io/branches/compare/0.5.7%0D0.5.6#diff
[0.5.6]: https://bitbucket.org/spottr/szew-io/branches/compare/0.5.6%0D0.5.5#diff
[0.5.5]: https://bitbucket.org/spottr/szew-io/branches/compare/0.5.5%0D0.5.4#diff
[0.5.4]: https://bitbucket.org/spottr/szew-io/branches/compare/0.5.4%0D0.5.3#diff
[0.5.3]: https://bitbucket.org/spottr/szew-io/branches/compare/0.5.3%0D0.5.2#diff
[0.5.2]: https://bitbucket.org/spottr/szew-io/branches/compare/0.5.2%0D0.5.1#diff
[0.5.1]: https://bitbucket.org/spottr/szew-io/branches/compare/0.5.1%0D0.5.0#diff
[0.5.0]: https://bitbucket.org/spottr/szew-io/branches/compare/0.5.0%0D0.4.0#diff
[0.4.0]: https://bitbucket.org/spottr/szew-io/branches/compare/0.4.0%0D0.3.6#diff
[0.3.6]: https://bitbucket.org/spottr/szew-io/branches/compare/0.3.6%0D0.3.5#diff
[0.3.5]: https://bitbucket.org/spottr/szew-io/branches/compare/0.3.5%0D0.3.4#diff
[0.3.4]: https://bitbucket.org/spottr/szew-io/branches/compare/0.3.4%0D0.3.3#diff
[0.3.3]: https://bitbucket.org/spottr/szew-io/branches/compare/0.3.3%0D0.3.2#diff
[0.3.2]: https://bitbucket.org/spottr/szew-io/branches/compare/0.3.2%0D0.3.1#diff
[0.3.1]: https://bitbucket.org/spottr/szew-io/branches/compare/0.3.1%0D0.3.0#diff
[0.3.0]: https://bitbucket.org/spottr/szew-io/branches/compare/0.3.0%0D0.2.5#diff
[0.2.5]: https://bitbucket.org/spottr/szew-io/branches/compare/0.2.5%0D0.2.4#diff
[0.2.4]: https://bitbucket.org/spottr/szew-io/branches/compare/0.2.4%0D0.2.3#diff
[0.2.3]: https://bitbucket.org/spottr/szew-io/branches/compare/0.2.3%0D0.2.2#diff
[0.2.2]: https://bitbucket.org/spottr/szew-io/branches/compare/0.2.2%0D0.2.1#diff
[0.2.1]: https://bitbucket.org/spottr/szew-io/branches/compare/0.2.1%0D0.2.0#diff
[0.2.0]: https://bitbucket.org/spottr/szew-io/branches/compare/0.2.0%0D0.1.2#diff
[0.1.2]: https://bitbucket.org/spottr/szew-io/branches/compare/0.1.2%0D0.1.1#diff
[0.1.1]: https://bitbucket.org/spottr/szew-io/branches/compare/0.1.1%0D0.1.0#diff
[0.1.0]: https://bitbucket.org/spottr/szew-io/branches/compare/0.1.0%0D67c521d6609a9b84c259e8d1cd59da365cee2b6a#diff

