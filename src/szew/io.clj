; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns szew.io
  "Input and Output processors."
  (:require
   [clojure.java.io :as clj.io]
   [clojure.string :as string]
   [clojure.spec.alpha :as s]
   [clojure-csv.core :as csv]
   [clojure.data.zip :as data.zip]
   [clojure.data.xml :as data.xml]
   [clojure.zip :as clj.zip]
   [clojure.edn]
   [szew.io.util :refer [fixed-width-split row-adjuster]]
   [szew.io.fu :refer [to-path readable? exists? pruning-path-seq]])
  (:import
   [java.io File
    BufferedReader BufferedInputStream InputStream
    BufferedWriter PushbackReader]
   [java.nio.file Path]
   [java.security MessageDigest]
   [java.util.zip Adler32]
   [java.util.zip GZIPInputStream GZIPOutputStream]
   [clojure.lang IFn]))

;; ## Protocols

(defprotocol Input
  "Protocol for getting data from source."
  (in! [spec source] "Read source, feed data to processor, return result."))

(defprotocol Output
  "Protocol for putting data to target."
  (sink [spec target] "Returned callable will eat seq and write to target."))

;; ## Contracts!

(s/def ::encoding (s/and string? (complement empty?)))

(s/def ::eol
  (s/or :char   char?
        :string (s/and string? (complement empty?))))

(s/def ::append boolean?)

(s/def ::final-eol boolean?)

(s/def ::part (s/and int? pos?))

(s/def ::callable (s/or :ifn ifn? :fn fn?))

(s/def ::processor ::callable)

(s/def ::reader ::callable)

(s/def ::writer ::callable)

(s/def ::lines-args
  (s/or :none (s/nilable (s/and map? empty?))
        :some (s/keys :opt-un [::encoding ::eol ::append ::final-eol
                               ::part ::processor ::reader ::writer])))

(s/def ::lines
  (s/keys :req-un [::encoding ::eol ::append ::final-eol ::part
                   ::processor ::reader ::writer]))

;; ## Text files, line reader.

(defrecord Lines [encoding   ;; r+w
                  eol        ;; w
                  append     ;; w
                  final-eol  ;; w
                  part       ;; w
                  processor  ;; r
                  reader     ;; r
                  writer]    ;; w

  Input

  (in! [spec source]
    (when-not (s/valid? ::lines spec)
      (throw
        (ex-info "Lines spec validation failed!"
                 {:explanation (s/explain-data ::lines spec)
                  :source      source})))
    (io! "Reading from file!"
         (with-open [^BufferedReader r (reader source :encoding encoding)]
           (processor (line-seq r)))))

  IFn

  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (in! spec (first args)))

  (invoke [spec target]
    (in! spec target))

  Output

  (sink [spec target]
    (when-not (s/valid? ::lines spec)
      (throw
        (ex-info "Lines spec validation failed!"
                 {:explanation (s/explain-data ::lines spec)
                  :target      target})))
    (with-meta
      (fn lines-sink [a-seq]
        (io! "Writing to file!"
             (with-open [^BufferedWriter w (writer target
                                                   :encoding encoding
                                                   :append append)]
               (loop [parts (partition part part nil (map str a-seq))]
                 (when (seq parts)
                   (.write w ^String (string/join eol (first parts)))
                   (when (or (seq (rest parts)) final-eol)
                     (.write w ^String eol))
                   (recur (rest parts)))))))
      {:spec   spec
       :target target})))

(defn lines
  "Accepts map, creates Lines spec, good for reading and writing.

  :field     |R|W| Default            | Description
  -----------+-+-+--------------------+---------------------------------------
  :encoding  |X|X| UTF-8              | File encoding, String.
  :eol       | |X| Env line.separator | End Of Line, char or String.
  :append    | |X| false              | Append to file? Boolean.
  :final-eol | |X| false              | EOL at the end of output? Boolean.
  :part      | |X| 64                 | Writer partitioning. Positive int.
  :processor |X| | vec                | Your callable.
  :reader    |X| | c.j.io/reader      | Reader provider.
  :writer    | |X| c.j.io/writer      | Writer provider.

  Invoking (lines) produces default spec.
  "
  {:added "0.1.0"}
  ([]
   {:post [(or (s/valid? ::lines %) (s/explain ::lines %))]}
   (map->Lines {:encoding  "UTF-8"
                :eol       (System/getProperty "line.separator" "\n")
                :append    false
                :final-eol false
                :part      64
                :processor vec
                :reader    clj.io/reader
                :writer    clj.io/writer}))
  ([spec]
   {:pre [(or (s/valid? ::lines-args spec) (s/explain ::lines-args spec))]
    :post [(or (s/valid? ::lines %) (s/explain ::lines %))]}
   (into (lines) spec))
  ([spec source]
   (in! (lines spec) source)))

(s/fdef
  fixed-width
  :args (s/alt :0-args (s/cat)
               :1-arg  (s/cat :spec ::lines-args)
               :2-args (s/cat :spec ::lines-args :source any?))
  :ret (s/or :lines (s/and (partial instance? Lines) ::lines)
             :other any?))

;; ## Delimiter Separated Values & Friends

(s/def ::delimiter :szew.io.util/delimiter)

(s/def ::strict boolean?)

(s/def ::quote-char char?)

(s/def ::force-quote boolean?)

(s/def ::dsv-args
  (s/or :none (s/nilable (s/and map? empty?))
        :some (s/keys :opt-un [::encoding ::append ::delimiter ::strict ::eol
                               ::quote-char ::force-quote ::part
                               ::processor ::reader ::writer])))

(s/def ::dsv
  (s/keys :req-un [::encoding ::append ::delimiter ::strict ::eol ::quote-char
                   ::force-quote ::part ::processor ::reader ::writer]))

(defrecord DSV [encoding    ;; r+w
                append      ;; w
                delimiter   ;; r+w
                strict      ;; r
                eol         ;; r+w
                quote-char  ;; r+w
                force-quote ;; w
                part        ;; w
                processor   ;; r
                reader      ;; r
                writer]     ;; w

  Input

  (in! [spec source]
    (when-not (s/valid? ::dsv spec)
      (throw
        (ex-info "DSV spec validation failed!"
                 {:explanation (s/explain-data ::dsv spec)
                  :source      source})))
    (letfn [(indexer [idx rows]
             (when (seq rows)
               (let [metas {:line idx :source source :spec spec}]
                 (cons
                  ;; This double try is due to clojure-csv. Sorry.
                  ;; Goof on the get-go, we are here:
                  (try (vary-meta (first rows) merge metas)
                       (catch Exception ex
                         (throw
                          (ex-info
                           (format "Issue on line: %d (1-indexed)."
                                   idx)
                           metas
                           ex))))
                  ;; Goof on the next candidate, trust me:
                  (try (when (seq (rest rows))
                         (lazy-seq (indexer (inc idx) (rest rows))))
                       (catch Exception ex
                         (throw
                          (ex-info
                           (format "Issue on line: %d (1-indexed)."
                                   (inc idx))
                           (update metas :line inc)
                           ex))))))))]
      (io! "Reading from file!"
           (with-open [^BufferedReader r (reader source :encoding encoding)]
             (processor (indexer 1 (csv/parse-csv r
                                                  :delimiter delimiter
                                                  :end-of-line eol
                                                  :quote-char quote-char
                                                  :strict strict)))))))

  IFn

  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (in! spec (first args)))

  (invoke [spec target]
    (in! spec target))

  Output

  (sink [spec target]
    (when-not (s/valid? ::dsv spec)
      (throw
        (ex-info "DSV spec validation failed!"
                 {:explanation (s/explain-data ::dsv spec)
                  :target      target})))
    (fn csv-sink [a-seq]
      (io! "Writing to file!"
           (with-open [^BufferedWriter w (writer target
                                                 :encoding encoding
                                                 :append append)]
             (loop [parts (partition part part nil a-seq)]
               (when (seq parts)
                 (.write w ^String (csv/write-csv (first parts)
                                                  :delimiter   delimiter
                                                  :end-of-line eol
                                                  :quote-char  quote-char
                                                  :force-quote force-quote))
                 (recur (rest parts)))))))))

(defn csv
  "Accepts map, creates CSV spec, good for reading and writing.

  :field       |R|W| Default            | Description
  -------------+-+-+--------------------+--------------------------------------
  :encoding    |X|X| UTF-8              | File encoding, String.
  :append      | |X| false              | Append to file? Boolean.
  :delimiter   |X|X| \\,                 | Field delimiter, char or String.
  :strict      |X| | true               | Fail in malformed input? Boolean.
  :eol         | |X| Env line.separator | End Of Line, char or String.
  :quote-char  |X|X| \\\"                 | Quote character, char.
  :force-quote | |X| false              | Quote ALL fields. Boolean.
  :part        | |X| 64                 | Writer partitioning. Positive int.
  :processor   |X| | vec                | Your callable.
  :reader      |X| | c.j.io/reader      | Reader provider.
  :writer      | |X| c.j.io/writer      | Writer provider.

  Invoking (csv) produces default spec.
  "
  {:added "0.1.0"}
  ([]
   {:post [(or (s/valid? ::dsv %) (s/explain ::dsv %))]}
   (map->DSV {:encoding    "UTF-8"
              :append      false
              :delimiter   \,
              :strict      true
              :eol         (System/getProperty "line.separator" "\n")
              :quote-char  \"
              :force-quote false
              :part        64
              :processor   vec
              :reader      clj.io/reader
              :writer      clj.io/writer}))
  ([spec]
   {:pre [(or (s/valid? ::dsv-args spec) (s/explain ::dsv-args spec))]
    :post [(or (s/valid? ::dsv %) (s/explain ::dsv %))]}
   (into (csv) spec))
  ([spec source]
   (in! (csv spec) source)))

(s/fdef
  csv
  :args (s/alt :0-args (s/cat)
               :1-arg  (s/cat :spec ::dsv-args)
               :2-args (s/cat :spec ::dsv-args :source any?))
  :ret (s/or :dsv   (s/and (partial instance? DSV) ::dsv)
             :other any?))

(defn tsv
  "Accepts map, creates TSV spec, good for reading and writing.

  This constructor delegates to csv, changes only :delimiter.

  Invoking (tsv) produces default spec.
  "
  {:added "0.1.0"}
  ([]
   (csv {:delimiter \tab}))
  ([spec]
   {:pre [(or (s/valid? ::dsv-args spec) (s/explain ::dsv-args spec))]
    :post [(or (s/valid? ::dsv %) (s/explain ::dsv %))]}
   (into (tsv) spec))
  ([spec source]
   (in! (tsv spec) source)))

(s/fdef
  tsv
  :args (s/alt :0-args (s/cat)
               :1-arg  (s/cat :spec ::dsv-args)
               :2-args (s/cat :spec ::dsv-args :source any?))
  :ret  (s/or :dsv     (s/and (partial instance? DSV) ::dsv)
              :other   any?))

;; ## Fixed Width Processing

(s/def ::widths (s/+ (s/and int? pos?)))

(s/def ::fill-char char?)

(s/def
  ::fixed-width-args
  (s/or :none (s/nilable (s/and map? empty?))
        :some (s/keys :opt-un [::encoding ::append ::eol ::strict ::widths
                               ::final-eol ::fill-char ::part
                               ::processor ::reader ::writer])))

(s/def
  ::fixed-width
  (s/keys :req-un [::encoding ::append ::eol ::strict ::widths ::fill-char
                   ::final-eol ::part ::processor ::reader ::writer]))

(defrecord FixedWidth [encoding   ;; r+w
                       append     ;; w
                       eol        ;; w
                       strict     ;; r
                       widths     ;; r+w
                       final-eol  ;; w
                       fill-char  ;; w
                       part       ;; w
                       processor  ;; r
                       reader     ;; r
                       writer]    ;; w

  Input

  (in! [spec source]
    (when-not (s/valid? ::fixed-width spec)
      (throw
        (ex-info "FixedWidth spec validation failed!"
                 {:explanation (s/explain-data ::fixed-width spec)
                  :source      source})))
    (let [width-sum (reduce + widths)
          ;; pass too-short record to splitter and trip it or remove quietly?
          prefilter (if strict
                      (constantly true)
                      (comp (partial <= width-sum) count second))
          splitter  (fixed-width-split widths)
          packer    (fn packer [[idx row]]
                      (vary-meta (splitter row)
                                 assoc :line-no (inc idx) :source source))]
      (io! "Reading from file!"
           (with-open [^BufferedReader r (reader source :encoding encoding)]
             (->> (line-seq r)
                  (map vector (range))
                  (filter prefilter)
                  (map packer)
                  processor)))))

  IFn

  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (in! spec (first args)))

  (invoke [spec target]
    (in! spec target))

  Output

  (sink [spec target]
    (when-not (s/valid? ::fixed-width spec)
      (throw
        (ex-info "FixedWidth spec validation failed!"
                 {:explanation (s/explain-data ::fixed-width spec)
                  :target      target})))
    (let [proto-row (mapv #(string/join (repeat % (str fill-char))) widths)
          adjuster  (row-adjuster proto-row)
          widener   (fn [width fill content]
                      (subs (str content fill) 0 width))
          prepper   (comp string/join
                          (partial mapv widener widths proto-row)
                          adjuster
                          (partial mapv str))]
      (with-meta
        (fn fixed-width-sink [a-seq]
          (io! "Writing to file!"
               (with-open [^BufferedWriter w (writer target
                                                     :encoding encoding
                                                     :append append)]
                 (loop [parts (partition part part nil (map prepper a-seq))]
                   (when (seq parts)
                     (.write w ^String (string/join eol (first parts)))
                     (when (or (seq (rest parts)) final-eol)
                       (.write w ^String (str eol)))
                     (recur (rest parts)))))))
        {:spec   spec
         :target target}))))

(defn fixed-width
  "Accepts map, creates FixedWidth spec, good for reading and writing.

  :field       |R|W| Default            | Description
  -------------+-+-+--------------------+--------------------------------------
  :encoding    |X|X| UTF-8              | File encoding, String.
  :append      | |X| false              | Append to file? Boolean.
  :eol         | |X| Env line.separator | End Of Line, char or String.
  :strict      |X| | true               | Fail in malformed input? Boolean.
  :widths      [X|X] [1]                | Slice sizes, vec of positive ints.
  :final-eol   |X|X| false              | EOL at the end of output? Boolean.
  :fill-char   | |X| \\space             | Right fill character, char.
  :part        | |X| 64                 | Writer partitioning. Positive int.
  :processor   |X| | vec                | Your callable.
  :reader      |X| | c.j.io/reader      | Reader provider.
  :writer      | |X| c.j.io/writer      | Writer provider.

  Invoking (fixed-width) produces default spec (with: {:widths [1]}).
  "
  {:added "0.1.0"}
  ([]
   {:post [(or (s/valid? ::fixed-width %) (s/explain ::fixed-width %))]}
   (map->FixedWidth {:encoding  "UTF-8"
                     :append    false
                     :eol       (System/getProperty "line.separator" "\n")
                     :strict    false
                     :widths    [1]
                     :final-eol false
                     :fill-char \space
                     :part      64
                     :processor vec
                     :reader    clj.io/reader
                     :writer    clj.io/writer}))
  ([spec]
   {:pre [(or (s/valid? (s/nilable map?) spec) (s/explain ::fixed-width spec))]
    :post [(or (s/valid? ::fixed-width %) (s/explain ::fixed-width %))]}
   (into (fixed-width) spec))
  ([spec source]
   (in! (fixed-width spec) source)))

(s/fdef
  fixed-width
  :args (s/alt :0-args (s/cat)
               :1-arg  (s/cat :spec ::fixed-width-args)
               :2-args (s/cat :spec ::fixed-width-args :source any?))
  :ret (s/or :fixed-width (s/and (partial instance? FixedWidth) ::fixed-width)
             :other       any?))

;; ## Basic XML processing

(s/def
  ::xml-args
  (s/or :none (s/nilable (s/and map? empty?))
        :some (s/keys :opt-un [::encoding ::append ::processor])))

(s/def ::xml
  (s/keys :req-un [::encoding ::append]
          :opt-un [::processor ::reader ::writer]))

(defrecord XML [encoding  ;; r+w
                append    ;; w
                processor ;; r
                reader    ;; r
                writer]   ;; w

  Input

  (in! [spec source]
    (when-not (s/valid? ::xml spec)
      (throw
        (ex-info "XML spec validation failed!"
                 {:explanation (s/explain-data ::xml spec)
                  :source      source})))
    (io! "Reading from file!"
         (with-open [^BufferedReader r (reader source :encoding encoding)]
           (processor (data.xml/parse r)))))

  IFn

  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (in! spec (first args)))

  (invoke [spec target]
    (in! spec target))

  Output

  (sink [spec target]
    (when-not (s/valid? ::xml spec)
      (throw
        (ex-info "XML spec validation failed!"
                 {:explanation (s/explain-data ::xml spec)
                  :target      target})))
    (with-meta
      (fn xml-sink [a-seq]
        (io! "Writing to file!"
             (with-open [^BufferedWriter w (writer target
                                                   :encoding encoding
                                                   :append append)]
               (data.xml/emit a-seq w :encoding encoding))))
      {:spec   spec
       :target target})))

(defn xml-realizer
  "Get parsed XML, touch every node, return head."
  {:added "0.2.4"}
  [parsed-xml]
  (-> parsed-xml clj.zip/xml-zip data.zip/descendants dorun)
  parsed-xml)

(defn xml
  "Accepts map, creates XML spec, good for reading and writing.

  :field       |R|W| Default              | Description
  -------------+-+-+----------------------+------------------------------------
  :encoding    |X|X| UTF-8                | File encoding, String.
  :append      | |X| false                | Append to file? Boolean.
  :processor   |X| | szew.io/xml-realizer | Your callable.
  :reader      |X| | c.j.io/reader        | Reader provider.
  :writer      | |X| c.j.io/writer        | Writer provider.

  Invoking (xml) produces default spec.

  Tips:

  1. Uses clojure.data.xml/parse and clojure.data.xml/emit
  2. Feed to clojure.zip/xml-zip first
  3. clojure.data.zip.xml next: xml->, attr=, attr, :tag, tag=, text & text=
  4. clojure.zip/node within! xml-> will extract whole loc from zip
  5. xml-> returns sequence of matches.
  "
  {:added "0.1.0"}
  ([]
   {:post [(or (s/valid? ::xml %) (s/explain ::xml %))]}
   (map->XML {:encoding  "UTF-8"
              :append    false
              :processor xml-realizer
              :reader    clj.io/reader
              :writer    clj.io/writer}))
  ([spec]
   {:pre [(or (s/valid? ::xml-args spec) (s/explain ::xml-args spec))]
    :post [(or (s/valid? ::xml %) (s/explain ::xml %))]}
   (into (xml) spec))
  ([spec source]
   (in! (xml spec) source)))

(s/fdef
  xml
  :args (s/alt :0-args (s/cat)
               :1-arg  (s/cat :spec ::xml-args)
               :2-args (s/cat :spec ::xml-args :source any?))
  :ret (s/or :xml   (s/and (partial instance? XML) ::xml)
             :other any?))

;; ## Basic EDN processing

(s/def ::readers (s/nilable map?))

(s/def ::default (s/nilable (s/or :fn fn? :ifn ifn?)))

(s/def
  ::edn-args
  (s/or :none (s/nilable (s/and map? empty?))
        :some (s/keys :opt-un [::encoding ::append ::processor
                               ::readers ::default ::reader ::writer])))

(s/def ::edn
  (s/keys :req-un [::encoding ::append ::processor ::readers ::default
                   ::reader ::writer]))

(defrecord EDN [encoding  ;; r+w
                append    ;; w
                processor ;; w
                readers   ;; r
                default   ;; r
                reader    ;; r
                writer]   ;; w

  Input

  (in! [spec source]
    (when-not (s/valid? ::edn spec)
      (throw
        (ex-info "EDN spec validation failed!"
                 {:explanation (s/explain-data ::edn spec)
                  :source      source})))
    (io! "Reading from file!"
         (with-open [^BufferedReader r (reader source :encoding encoding)
                     ^PushbackReader p (PushbackReader. r)]
           (let [eof      (Object.)
                 opts     {:eof eof :readers readers :default default}
                 not-done (partial not= eof)
                 more!    (fn [] (clojure.edn/read opts p))]
             (processor (take-while not-done (repeatedly more!)))))))

  IFn

  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (in! spec (first args)))

  (invoke [spec target]
    (in! spec target))

  Output

  (sink [spec target]
    (when-not (s/valid? ::edn spec)
      (throw
        (ex-info "EDN spec validation failed!"
                 {:explanation (s/explain-data ::edn spec)
                  :target      target})))
    (with-meta
      (fn edn-sink [a-seq]
        (io! "Writing to file!"
             (with-open [^BufferedWriter w (writer target
                                                   :encoding encoding
                                                   :append append)]
               (loop [items a-seq]
                 (when-let [item (first items)]
                   (.write w ^String (prn-str item))
                   (recur (rest items)))))))
      {:spec   spec
       :target target})))

(defn edn
  "Accepts map, creates EDN spec, good for reading and writing.

  :field       |R|W| Default              | Description
  -------------+-+-+----------------------+------------------------------------
  :encoding    |X|X| UTF-8                | File encoding, String.
  :append      | |X| false                | Append to file? Boolean.
  :processor   |X| | vec                  | Your callable.
  :readers     |X| | nil                  | Custom readers for clojure.edn.
  :default     |X| | vector               | Default callable for tag+value.
  :reader      |X| | c.j.io/reader        | Reader provider.
  :writer      | |X| c.j.io/writer        | Writer provider.

  Invoking (edn) produces default spec.
  "
  {:added "0.3.5"}
  ([]
   {:post [(or (s/valid? ::edn %) (s/explain ::edn %))]}
   (map->EDN {:encoding  "UTF-8"
              :append    false
              :processor vec
              :readers   {}
              :default   vector
              :reader    clj.io/reader
              :writer    clj.io/writer}))
  ([spec]
   {:pre [(or (s/valid? ::edn-args spec) (s/explain ::edn-args spec))]
    :post [(or (s/valid? ::edn %) (s/explain ::edn %))]}
   (into (edn) spec))
  ([spec source]
   (in! (edn spec) source)))

(s/fdef
  edn
  :args (s/alt :0-args (s/cat)
               :1-arg  (s/cat :spec ::edn-args)
               :2-args (s/cat :spec ::edn-args :source any?))
  :ret (s/or :edn   (s/and (partial instance? EDN) ::edn)
             :other any?))

;; ## File tree processor

(defn pruning-file-seq
  "Like file-seq, but can prune files and directories based on predicate.

  If `follow?` returns false - file/directory is not processed, nor entered.

  Predicate will be first executed on given entry.

  If predicate not given it just calls file-seq.

  Why?

  Ever went into a massive .svn directory? Now you can have tree-seq that
  prunes the entire path of enquiry and moves on. Pretty nice!
  "
  {:added "0.3.0"}
  ([entry follow?]
   (let [root (clj.io/as-file entry)]
     (when (follow? root)
       (tree-seq (fn branch?
                   [^java.io.File f]
                   (.isDirectory f))
                 (fn children
                   [^java.io.File d]
                   (filter follow? (.listFiles d)))
                 root))))
  ([entry]
   (file-seq entry)))

(def ^{:doc        "Those who can't spell - shim."
       :added      "0.1.0"
       :deprecated "0.4.0"}
  prunning-file-seq pruning-file-seq)

(s/def ::follow? ::callable)

(s/def
  ::files-args
  (s/or :none (s/nilable (s/and map? empty?))
        :some (s/keys :opt-un [::follow? ::processor])))

(s/def ::files
  (s/keys :req-un [::follow? ::processor]))

(defrecord Files [follow? processor]

  Input

  (in! [spec source]
    (let [root ^File (clj.io/as-file source)]
      (if-not (and (.exists root) (.canRead root))
        (processor '())
        (io! "Walking over files!"
             (processor (pruning-file-seq root follow?))))))

  IFn

  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (in! spec (first args)))

  (invoke [spec target]
    (in! spec target)))

(defn files
  "Accepts map, creates Files spec, good for reading.

  :field       |R|W| Default            | Description
  -------------+-+-+--------------------+--------------------------------------
  :follow?     | |X| (constantly true)  | Keep? Predicate: File -> Boolean.
  :processor   |X| | vec                | Your callable.

  Invoking (files) produces default spec.
  "
  {:added "0.1.0"}
  ([]
   {:post [(or (s/valid? ::files %) (s/explain ::files %))]}
   (Files. (constantly true) vec))
  ([spec]
   {:pre [(or (s/valid? ::files-args spec) (s/explain ::files-args spec))]
    :post [(or (s/valid? ::files %) (s/explain ::files %))]}
   (into (files) spec))
  ([spec source]
   (in! (files spec) source)))

(s/fdef
  files
  :args (s/alt :0-args (s/cat)
               :1-arg  (s/cat :spec ::files-args)
               :2-args (s/cat :spec ::files-args :source any?))
  :ret (s/or :files    (s/and (partial instance? Files) ::files)
             :other    any?))

(s/def
  ::paths-args
  (s/or :none (s/nilable (s/and map? empty?))
        :some (s/keys :opt-un [::follow? ::processor])))

(s/def ::paths
  (s/keys :req-un [::follow? ::processor]))

(defrecord Paths [follow? processor]

  Input

  (in! [spec source]
    (let [root ^Path (to-path source)]
      (if-not (and (exists? root) (readable? root))
        (processor '())
        (io! "Walking over paths!"
             (processor (pruning-path-seq root follow?))))))

  IFn

  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (in! spec (first args)))

  (invoke [spec target]
    (in! spec target)))

(defn paths
  "Accepts map, creates Paths spec, good for reading.

  :field       |R|W| Default            | Description
  -------------+-+-+--------------------+--------------------------------------
  :follow?     | |X| (constantly true)  | Keep? Predicate: Path -> Boolean.
  :processor   |X| | vec                | Your callable.

  Invoking (paths) produces default spec.
  "
  {:added "0.5.0"}
  ([]
   {:post [(or (s/valid? ::paths %) (s/explain ::paths %))]}
   (map->Paths {:follow?   (constantly true)
                :processor vec}))
  ([spec]
   {:pre [(or (s/valid? ::paths-args spec) (s/explain ::paths-args spec))]
    :post [(or (s/valid? ::paths %) (s/explain ::paths %))]}
   (into (paths) spec))
  ([spec source]
   (in! (paths spec) source)))

(s/fdef
  paths
  :args (s/alt :0-args (s/cat)
               :1-arg  (s/cat :spec ::paths-args)
               :2-args (s/cat :spec ::paths-args :source any?))
  :ret (s/or :paths    (s/and (partial instance? Paths) ::paths)
             :other    any?))

;; ## Single file -> hash

(s/def ::hash-name string?)

(s/def ::skip-bytes (s/and int? (complement neg?)))

(s/def ::sample-size
  (s/or :keyword #{:full}
        :number  (s/and int? pos?)))

(s/def ::hasher-args
  (s/or :none (s/nilable (s/and map? empty?))
        :some (s/keys :opt-un [::hash-name ::skip-bytes ::sample-size])))

(s/def ::hasher
  (s/keys :req-un [::hash-name ::skip-bytes ::sample-size]))

(def default-sample-size (* 128 1099511627776)) ;; 128TiB

;; This code is written like this, because somebody was stuck with old Java.
;; Left like that for historical reasons.
(defrecord Hasher [hash-name skip-bytes sample-size input-stream]

  Input

  (in! [spec source]
    (when-not (s/valid? ::hasher spec)
      (throw
        (ex-info "Hasher spec validation failed!"
                 {:explanation (s/explain-data ::hasher spec)
                  :source      source})))
    (let [a-file      ^File (clj.io/as-file source)
          sample-size (long (if (= :full sample-size)
                              default-sample-size sample-size))
          buff-size   (long (* 256 1024))
          buffer      (byte-array buff-size (byte 0))
          mess        (MessageDigest/getInstance hash-name)]
      (io! "Reading from file!"
           (with-open [^BufferedInputStream r
                       (doto ^InputStream (input-stream a-file)
                         (.skip skip-bytes))]
             (loop [read-bytes (.read r buffer 0 (min buff-size sample-size))
                    to-read    sample-size]
               (let [left-to-read (if (neg? read-bytes)
                                    0 (- to-read read-bytes))]
                 (when-not (neg? read-bytes)
                   (.update mess buffer 0 read-bytes))
                 (if (zero? left-to-read)
                   (.toString (BigInteger. 1 (.digest mess)) 16)
                   (recur (.read r buffer 0 (min buff-size left-to-read))
                          left-to-read))))))))

    IFn

    (applyTo [spec args]
             (when-not (= (count args) 1)
               (throw (ex-info "Wrong number of arguments! Expected 1."
                               {:spec spec
                                :args args})))
             (in! spec (first args)))

    (invoke [spec target]
            (in! spec target)))

(defn hasher
  "Accepts map, creates Hasher spec, good for reading.

  :field        |R|W| Default             | Description
  --------------+-+-+---------------------+------------------------------------
  :hash-name    |X| | SHA-256             | Hash algo name.
  :skip-bytes   |X| | 0                   | Skip bytes, non-negative int.
  :sample-size  |X| | :full               | How many bytes, :full or pos. int.
  :input-stream |X| | c.j.io/input-stream | InputStream provider.
  "
  {:added "0.1.0"}
  ([]
   {:post [(or (s/valid? ::hasher %) (s/explain ::hasher %))]}
   (Hasher. "SHA-256" 0 :full clj.io/input-stream))
  ([spec]
   {:pre [(or (s/valid? ::hasher-args spec) (s/explain ::hasher-args spec))]
    :post [(or (s/valid? ::hasher %) (s/explain ::hasher %))]}
   (into (hasher) spec))
  ([spec source]
   (in! (hasher spec) source)))

(s/fdef
  hasher
  :args (s/or :0-args (s/cat)
              :1-arg  (s/cat :spec ::hasher-args)
              :2-args (s/cat :spec ::hasher-args :source any?))
  :ret (s/or :hasher (s/and (partial instance? Hasher) ::hasher)
             :other  any?))

;; Adler32 hasher, a bit faster. ;-)

(s/def ::fast-hasher-args
  (s/or :none (s/nilable (s/and map? empty?))
        :some (s/keys :opt-un [::skip-bytes ::sample-size ::input-stream])))

(s/def ::fast-hasher
  (s/keys :req-un [::skip-bytes ::sample-size ::input-stream]))

(defrecord FastHasher [skip-bytes sample-size input-stream]

  Input

  (in! [spec source]
    (when-not (s/valid? ::fast-hasher spec)
      (throw
       (ex-info "Adler32 spec validation failed!"
                {:explanation (s/explain-data ::fast-hasher spec)
                 :source      source})))
    (let [a-file      ^File (clj.io/as-file source)
          sample-size (long (if (= :full sample-size)
                              default-sample-size sample-size))
          buff-size   (long (* 32 1024))
          buffer      (byte-array buff-size (byte 0))
          hash-obj    (Adler32.)]
      (io! "Reading from file!"
           (with-open [^BufferedInputStream r
                       (doto ^InputStream (input-stream a-file)
                         (.skip skip-bytes))]
             (loop [read-bytes (.read r buffer 0 (min buff-size sample-size))
                    to-read    sample-size]
               (let [left-to-read (if (neg? read-bytes)
                                    0 (- to-read read-bytes))]
                 (when-not (neg? read-bytes)
                   (.update hash-obj buffer 0 read-bytes))
                 (if (zero? left-to-read)
                   (.getValue hash-obj)
                   (recur (.read r buffer 0 (min buff-size left-to-read))
                          left-to-read))))))))

  IFn

  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (in! spec (first args)))

  (invoke [spec target]
    (in! spec target)))

(defn fast-hasher
  "Accepts map, creates FastHasher spec, good for reading.

  :field        |R|W| Default             | Description
  --------------+-+-+---------------------+------------------------------------
  :skip-bytes   |X| | 0                   | Skip bytes, non-negative int.
  :sample-size  |X| | :full               | How many bytes, :full or pos. int.
  :input-stream |X| | c.j.io/input-stream | InputStream provider.
  "
  {:added "0.5.6"}
  ([]
   {:post [(or (s/valid? ::fast-hasher %) (s/explain ::fast-hasher %))]}
   (FastHasher. 0 :full clj.io/input-stream))
  ([spec]
   {:pre [(or (s/valid? ::fast-hasher-args spec)
              (s/explain ::fast-hasher-args spec))]
    :post [(or (s/valid? ::fast-hasher %) (s/explain ::fast-hasher %))]}
   (into (fast-hasher) spec))
  ([spec source]
   (in! (fast-hasher spec) source)))

(s/fdef
  fast-hasher
  :args (s/or :0-args (s/cat)
              :1-arg  (s/cat :spec ::fast-hasher-args)
              :2-args (s/cat :spec ::fast-hasher-args :source any?))
  :ret (s/or :hasher (s/and (partial instance? FastHasher) ::fast-hasher)
             :other  any?))

;; ## Helpers

(defn gzip-input-stream
  "Return output-stream with GZIPInputStream within.
  "
  {:added "0.1.0"}
  [file-like & opts]
  (->> (cons file-like opts)
       (apply clj.io/input-stream)
       (GZIPInputStream.)
       (clj.io/input-stream)))

(defn gzip-output-stream
  "Return output-stream with GZIPOutputStream within.
  "
  {:added "0.1.0"}
  [file-like & opts]
  (->> (cons file-like opts)
       (apply clj.io/output-stream)
       (GZIPOutputStream.)
       (clj.io/output-stream)))

(defn gzip-reader
  "Return reader with GZIPInputStream within.
  "
  {:added "0.1.0"}
  [file-like & opts]
  (->> opts
       (cons (apply gzip-input-stream (cons file-like opts)))
       (apply clj.io/reader)))

(defn gzip-writer
  "Return writer with GZIPOutputStream within.
  "
  {:added "0.1.0"}
  [file-like & opts]
  (->> opts
       (cons (apply gzip-output-stream (cons file-like opts)))
       (apply clj.io/writer)))

(defn string-reader
  "Return a function providing StringReader over a given String.
  "
  {:added "0.3.1"}
  [^String a-string & opts]
  (apply clj.io/reader (cons (java.io.StringReader. a-string) opts)))

(defn pass-through-writer
  "Return a function passing over provided writer. Like a StringWriter.
  "
  {:added "0.3.1"}
  [^java.io.Writer a-writer & opts]
  (apply clj.io/writer (cons a-writer opts)))
