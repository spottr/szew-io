; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns szew.io.util
  "Useful data mangling functions."
  (:require
   [clojure.set :as sets]
   [clojure.walk :as walk]
   [clojure-csv.core :as csv]
   [camel-snake-kebab.core :as csk]
   [clojure.string :as string]
   [clojure.spec.alpha :as s]))

;; ## Row fixers

(defn row-adjuster
  "Creates a function that will return vectors of same length as default-row.
  Missing columns will be filled by defaults. Extra columns dropped.

  Why?

  Because *SV does not have to be well formed, numbers of column may vary.

  How?

  ((row-adjuster [1 2 3]) [:x])
  => [:x 2 3]

  ((row-adjuster [1 2 3]) [:1 :2 :3 :4])
  => [:1 :2 :3]
  "
  {:added "0.1.0"}
  ([default-row]
   {:pre [(vector? default-row)]}
   (if (empty? default-row)
     identity
     (let [default        (vec default-row)
           default-length (count default)]
       (with-meta
         (fn adjuster
           ([a-row]
            {:pre [(vector? a-row)]}
            (let [row-length (count a-row)]
              (with-meta
                (if (= row-length default-length)
                  a-row
                  (if (> row-length default-length)
                    (subvec a-row 0 default-length)
                    (into a-row (subvec default row-length))))
                {:original-row a-row
                 :default-row  default-row}))))
         {:default-row    default
          :default-length default-length
          :doc "Adjust row based on default-row and default-length."})))))

(s/fdef row-adjuster
  :args (s/cat :default-row (s/coll-of any? :kind vector? :into []))
  :ret  ifn?)

;; ## Line Splitters

(defn fixed-width-split
  "Takes vector of slice sizes, returns function that will cut String.

  Why?

  Because somebody thought fixed width data records are a good thing.

  How?

  ((fixed-width-split [4 3 4]) \"Ala ma Kota.\")
  => [\"Ala \" \"ma \" \"Kota\"]
  "
  {:added "0.1.0"}
  ([slices]
   {:pre [(seq slices) (every? number? slices) (every? pos? slices)]}
   (let [steps   (partition 2 1 (reductions + 0 slices))
         idx-end (last (last steps))
         slicer  (->> steps
                      (mapv (fn [[start stop]] #(subs % start stop)))
                      (apply juxt))]
     (with-meta
       (fn fixed-width-splitter
         ([^String row]
          {:pre [(string? row)]}
          (let [len (count row)]
            (when (> idx-end len)
              (throw (ex-info
                       (format "Given row is too short: is %d, want %d."
                               len idx-end)
                       {:row     row
                        :idx-end idx-end
                        :length  len})))
            (with-meta
              (slicer row)
              {:original-row row
               :steps        steps
               :slices       slices
               :fields       slices}))))
       {:steps  steps
        :slices slices
        :fields slices
        :doc    "Accepts String, returns [String] slices."}))))

(s/def ::slice (s/and number? pos?))

(s/def ::slices (s/coll-of ::slice
                           :kind      vector?
                           :into      []
                           :min-count 1))

(s/fdef fixed-width-split
  :args (s/cat :slices ::slices)
  :ret  ifn?)

;; ## Record curators

(defn field-unpacker
  "See if field contains a str/char delimiter, if so -- tries to parse as CSV.

  Why?

  Sometimes your fields have fields. (So many fields, fields for days.)

  How?

  (field-unpacker \\, \"ala,ma,kota\")
  => [\"ala\", \"ma\", \"kota\"]
  "
  {:added "0.1.0"}
  [delimiter ^String field]
  (if-not (and (string? field) (.contains field (str delimiter)))
    field
    (first (csv/parse-csv field :delimiter delimiter))))

(s/def ::delimiter
  (s/or :char   char?
        :string (s/and string? (complement empty?))))

(s/def ::field string?)

(s/fdef field-unpacker
  :args (s/cat :delimiter ::delimiter :field ::field)
  :ret  (s/or :field ::field :unpacked vector?))

(defn row-field-unpacker
  "Takes a delimiter and a row, runs field unpacker over the row.

  Why?

  Partial it away and apply to nested CSV/TSV stuff.

  How?

  (row-field-unpacker \\, [\"xnay\" \"ala,ma,kota\" \"unpackey\")
  => [\"xnay\" [\"ala\" \"ma\" \"kota\"] \"unpackey\"]
  "
  {:added "0.1.0"}
  [delimiter a-row]
  (mapv (partial field-unpacker delimiter) a-row))

(s/def ::a-row (s/coll-of ::field
                          :kind vector?
                          :into []))

(s/fdef row-field-unpacker
  :args (s/cat :delimiter ::delimiter :a-row ::a-row)
  :ret  vector?)

;; ## Record helpers

(defn vec->map
  "Takes header and values vector, zipmaps, adds metadata. Allows defaults.

  Why?

  Singular operation for vecs->maps, with inputs attached as meta.

  How?

  (vec->map [:k1 :k2] [1 2])
  => {:k1 1, :k2 2}

  (vec->map {:kx 100} [:k1 :k2] [1 2])
  => {:kx 100, :k1 1, :k2 2}

  (meta (vec->map {:kx 100} [:k1 :k2] [1 2]))
  => {:defaults {:kx 100}, :header [:k1 :k2], :values [1 2]}
  "
  {:added "0.3.0"}
  ([defaults header values]
   (with-meta
     (reduce-kv (fn assoc-with-default [m k v]
                  (assoc m k (if (nil? v) (get defaults k) v)))
                  defaults
                  (zipmap header values))
     {:defaults defaults
      :header   header
      :values   values}))
  ([header values]
   (with-meta
     (zipmap header values)
     {:defaults nil
      :header   header
      :values   values})))

(s/fdef vec->map
  :args
  (s/alt :3-args (s/cat :defaults map? :header vector? :values vector?)
         :2-args (s/cat :header vector? :values vector?))
  :ret map?)

(defn vecs->maps
  "Takes header vector and tail, returns sequence of maps.

  Zipmaps head with tail. Merges with defaults, doing nil substitution.

  Why?

  Maps are friendlier than vectors.

  How?

  (vecs->maps [[:k1 :k2] [1 2] [3 4]])
  => ({:k1 1, :k2 2}, {:k1 3 :k2 4})

  (vecs->maps [:k1 :k2] [[1 2] [3 4]])
  => ({:k1 1, :k2 2}, {:k1 3 :k2 4})

  (let [s [[:a :b] [1 2] [3 4]]]
    (vecs->maps (first s) (rest s)))
  => ({:a 1, :b 2}, {:a 3, :b 4})

  (let [d {:x 100, :b 9}
        s [[:a :b] [1 nil] [3 4]]]
    (vecs->maps d (first s) (rest s)))
  => ({:a 1 :b 9 :x 100} {:a 3 :b 4 :x 100})
  "
  {:added "0.3.0"}
  ([defaults head tail]
   (map (partial vec->map defaults head) tail))
  ([head tail]
   (map (partial vec->map head) tail))
  ([a-seq]
   (vecs->maps (first a-seq) (rest a-seq))))

(s/fdef vecs->maps
  :args
  (s/alt :3-args (s/cat :defaults map?
                        :head     (s/coll-of any? :kind vector? :into [])
                        :tail     (s/coll-of vector?))
         :2-args (s/cat :head     (s/coll-of any? :kind vector? :into [])
                        :tail     (s/coll-of vector?))
         :1-arg  (s/cat :a-seq    (s/coll-of vector?)))
  :ret (s/coll-of map?))

(defn map->vec
  "Takes header vector and map mapped. Returns vector with meta.

  The defaults map is used if key is missing from mapped.

  Why?

  Some formats serialize naturally from a vector. Uniform vectors are nice.

  How?

  (map->vec [:k1 :k2] {:k1 1, :k2 2})
  => [1 2]

  (map->vec {:kx 100} [:k1 :k2 :kx] {:k1 1, :k2 2})
  => [1 2 100]

  (meta (map->vec {:kx 100} [:k1 :k2 :kx] {:k1 1 :k2 2}))
  => {:defaults {:kx 100}, :header [:k1 :k2 :kx], :mapped {:k1 1, :k2 2}}
  "
  {:added "0.3.0"}
  ([defaults header mapped]
   (with-meta
     (mapv (fn skip-getter [k] (or (get mapped k) (get defaults k))) header)
     {:defaults defaults
      :header   header
      :mapped   mapped}))
  ([header mapped]
   (with-meta
     (mapv mapped header)
     {:defaults nil
      :header   header
      :mapped   mapped})))

(s/fdef map->vec
  :args
  (s/alt :3-args (s/cat :defaults (s/nilable map?)
                        :header   (s/coll-of any?)
                        :mapped   map?)
         :2-args (s/cat :header   (s/coll-of any?)
                        :mapped   map?))
  :ret (s/coll-of any? :kind vector? :into []))

(defn maps->vecs
  "Takes header vector and maps collection tail, returns sequence of vectors.

  The defaults map is used if key is missing from tail map.

  Why?

  Sometimes you just need your data as uniform vectors instead of maps.

  How?

  (maps->vecs [[:k1 :k2] {:k1 1, :k2 2}])
  => ([1 2])

  (maps->vecs [:k1 :k2] [{:k1 1, :k2 2}])
  => ([1 2])

  (maps->vecs {:kx 100} [:k1 :k2 :kx] [{:k1 1, :k2 2}])
  => ([1 2 100])

  (maps->vecs [:k1 :k2] [{:k1 1, :k2 2} {:k1 1, :k2 0}])
  => ([1 2] [1 0])
  "
  {:added "0.3.0"}
  ([defaults head tail]
   (map (partial map->vec defaults head) tail))
  ([head tail]
   (map (partial map->vec head) tail))
  ([a-seq]
   (maps->vecs (first a-seq) (rest a-seq))))

(s/fdef maps->vecs
  :args
  (s/alt :3-args (s/cat :defaults (s/nilable map?)
                        :head     (s/coll-of any? :into [])
                        :tail     (s/coll-of map?))
         :2-args (s/cat :head     (s/coll-of any? :into [])
                        :tail     (s/coll-of map?))
         :1-arg  (s/cat :a-seq    (s/and (s/coll-of any? :min-count 2)
                                         (comp vector? first)
                                         (comp (partial every? map?) rest))))
  :ret  (s/coll-of vector?))

(defn maps-maker
  "Create function of collection of vectors using two callables: head and tail.

  By default head is first row as kebab-case-keyword vector and tail is rest.

  Why?

  Ingest spreadsheet like grid data, get a sequence of sane-ish maps.

  How?

  ((maps-maker) [[\"X\" \"Y\"] [0 0] [1 1] [10 10]])
  => ({:x 0 :y 0} {:x 1 :y 1} {:x 10 :y 10})
  "
  {:added "0.3.0"}
  ([head tail]
   (with-meta
     (fn make-maps
       ([defaults a-sequence]
        (let [header      (head a-sequence)
              header-meta #(vary-meta %2
                                      assoc
                                      :number   %1
                                      :header   header
                                      :defaults defaults)
              datas       (tail a-sequence)]
          (if (empty? defaults)
            (map header-meta (range) (vecs->maps header datas))
            (map header-meta (range) (vecs->maps defaults header datas)))))
       ([a-sequence]
        (make-maps {} a-sequence)))
     {:head head :tail tail :tails tail}))
  ([]
   (maps-maker (comp (partial mapv csk/->kebab-case-keyword) first) rest)))

(defn bespoke-header
  "Accepts vector and a map, returns header vector derived from both.

  Header consists of base-header and remaining keys from the map, sorted.

  If strict: base-header must be a subset of keys in the map, otherwise
  ExceptionInfo. If not strict then just 'do your best'.

  If only given the map - returns vector of its sorted keys.

  Why?

  You want some key columns first, while keeping all the data.

  How?

  (bespoke-header [:x :y :z] {:state :resting :x 0 :y 0 :z 0})
  => [:x :y :z :state]

  (bespoke-header [:x :y :z :cycle] {:state :resting :x 0 :y 0 :z 0})
  => [:x :y :z :cycle :state]

  (bespoke-header [:x :y :z :cycle] true {:state :resting :x 0 :y 0 :z 0})
  => clojure.lang.ExceptionInfo: Missing keys in header?
  "
  {:added "0.3.0"}
  ([base-header strict? a-map]
   (let [header-set    (set base-header)
         present-set   (set (keys a-map))
         remainder-set (sets/difference present-set header-set)]
     (if (empty? a-map)
       (vec base-header)
       (if (and strict? (not (sets/subset? header-set present-set)))
         (throw
           (ex-info "Missing keys in header?"
                    {:a-map         a-map
                     :missing       (sets/difference header-set present-set)
                     :header-set    header-set
                     :present-set   present-set
                     :remainder-set remainder-set}))
         (into (vec base-header) (sort remainder-set))))))
  ([base-header a-map]
   (bespoke-header base-header false a-map))
  ([a-map]
   (vec (sort (keys a-map)))))

(s/fdef bespoke-header
  :args
  (s/alt :3-args (s/cat :base-header (s/coll-of any? :kind vector? :into [])
                        :strict?     boolean?
                        :a-map       map?)
         :2-args (s/cat :base-header (s/coll-of any? :kind vector? :into [])
                        :a-map       map?)
         :1-arg  (s/cat :a-map       map?))
  :ret vector?)

(defn vecs-maker
  "Accepts defaults and two output producing functions of header and row.

  By default header-prep is vector of SCREAMING_SNAKE_CASE_STRING and row-prep
  is a vector of String with nil replaced by \"NULL\".

  Bespoke header based on first record if sequence, so they better be uniform!

  Why?

  Quick and dirty dumps for spreadsheet consumption!

  How?

  ((vecs-maker) [[:x :y :z] [{:x 0 :y 0} {:x 10 :y 10}]])
  => ([\"X\" \"Y\" \"Z\"] [\"0\" \"0\" \"NULL\"] [\"10\" \"10\" \"NULL\"])

  ((vecs-maker {:z 0}) [[:x :y :z] [{:x 0 :y 0} {:x 10 :y 10}]])
  => ([\"X\" \"Y\" \"Z\"] [\"0\" \"0\" \"0\"] [\"10\" \"10\" \"0\"])

  ((vecs-maker {:z 0} identity identity)
   [[:x :y :z] [{:x 0 :y 0} {:x 10 :y 10}]])
  => ([:x :y :z] [0 0 0] [10 10 0])

  ((vecs-maker nil identity identity)
   [[:x :y :z] [{:x 0 :y 0} {:x 10 :y 10}]])
  => ([:x :y :z] [0 0 nil] [10 10 nil])
  "
  {:added "0.3.0"}
  ([defaults header-prep row-prep]
   (with-meta
     (fn make-vecs
       ([[header records]]
        (let [extended (bespoke-header header (first records))]
          (cons (header-prep extended)
                (map row-prep
                     (maps->vecs defaults extended records))))))
     {:defaults    defaults
      :header-prep header-prep
      :row-prep    row-prep
      :doc "Wants [header records], returns sequence of vectors."}))
  ([defaults]
   (vecs-maker defaults
              (partial mapv csk/->SCREAMING_SNAKE_CASE_STRING)
              (partial mapv (fnil str "NULL"))))
  ([]
   (vecs-maker nil
               (partial mapv csk/->SCREAMING_SNAKE_CASE_STRING)
               (partial mapv (fnil str "NULL")))))

(defn friendlify
  "Take Clojure object and try to make a pretty String from its Class.

  Why?

  Display function name at runtime, but similar to how it looks in code.

  How?

  (friendlify friendlify)
  => \"szew.io.util/friendlify\"
  "
  {:added "0.1.0"}
  [a-something]
  (-> a-something
      (class)
      (str)
      (string/replace-first #"(^class )" "")
      (string/replace-first #"((?:__\d+|)@[a-fA-F0-9]+$)" "")
      (string/replace-first #"(__\d+$)" "")
      (string/replace \$ \/)
      (string/replace "_BANG_" "!")
      (string/replace "_BAR_" "|")
      (string/replace "_SHARP_" "#")
      (string/replace "_PERCENT_" "%")
      (string/replace "_AMPERSAND_" "&")
      (string/replace "_QMARK_" "?")
      (string/replace "_PLUS_" "+")
      (string/replace "_STAR_" "*")
      (string/replace "_LT_" "<")
      (string/replace "_GT_" ">")
      (string/replace "_EQ_" "=")
      (string/replace "_DOT_" ".")
      (string/replace "_COLON_" ":")
      (string/replace "_SINGLEQUOTE_" "'")
      (string/replace \_ \-)))

(s/fdef friendlify
  :args (s/cat :something any?)
  :ret  string?)

(defn getter
  "Takes key and default value, returns a function that gets it.

  Why?

  Builtin get takes collection as first argument, this is the other way around.

  How?

  (mapv (getter :yolo :no) [{:yolo :yes} {:wat? :wat}])
  => [:yes :no]

  (meta (getter :yolo :no))
  => {:key :yolo, :default :no}

  (mapv (getter 0 :no) [[:yes] []])
  => [:yes :no]

  (meta (getter 1 :no))
  => {:key 1, :default :no}
  "
  {:added "0.1.0"}
  ([a-key default]
   (with-meta
     (fn sub-getter [gettable]
       (get gettable a-key default))
     {:key a-key :default default}))
  ([a-key]
   (getter a-key nil)))

(s/fdef getter
  :args
  (s/alt :2-args (s/cat :a-key any? :default any?)
         :1-arg  (s/cat :a-key any?))
  :ret ifn?)

(defn juxt-map
  "Takes keys and values, get juxt map of keys-values. Hint: zipmap over juxt.

  Why?

  Maps are bees knees and juxt is just cool.

  How?

  ((juxt-map :+ inc :- dec := identity) 2)
  => {:+ 3, :- 1, := 2}
  "
  {:added "0.1.0"}
  [& keys-fns]
  {:pre [(-> keys-fns count even?)]}
  (let [parts (partition 2 2 nil keys-fns)
        keysv (mapv first parts)
        juxt* (apply juxt (map second parts))]
    (with-meta
      (fn juxt-mapper [& args]
          (zipmap keysv (apply juxt* args)))
      {:keys-fns keys-fns})))

(defn deep-sort
  "Takes nested collection, returns it with sorted maps and sets.

  Keeps meta.

  Note: Sorting requires comparable values. Non-compliance means no order.
  Other containers are passed through.

  Why?

  Sometimes you want to be able to just look at the map and know if a key
  is there. Ordering keys helps most of the time.

  How?

  (deep-sort {100 :a, 90 {10 :maybe, 9 {:yes :no}}, 80 :b})
  => {80 :b, 90 {9 {:yes :no}, 10 :maybe}, 100 :a}
  "
  {:added "0.3.3"}
  [coll-of-colls]
  (letfn [(sorted-w-meta [obj]
            (try
              (cond
                (sorted? obj)
                obj
                (map? obj)
                (into (with-meta (sorted-map) (meta obj)) obj)
                (set? obj)
                (into (with-meta (sorted-set) (meta obj)) obj)
                :else
                obj)
              ;; Probably mixed types within objects... naughty.
              (catch ClassCastException _ obj)))]
    (walk/postwalk sorted-w-meta coll-of-colls)))

(s/fdef deep-sort
  :args (s/cat :coll-of-colls (s/coll-of any?))
  :ret  (s/coll-of any?)
  :fn   (fn [d] (= (:ret d) (:coll-of-colls (:args d)))))

(defn roll-in
  "Takes sequence of sequences, returns map of maps: last item is value,
  butlast items are the key.

  If agg callable is given -- it's used with update-in, otherwise entries
  action is assoc-in.

  Why?

  I just like maps.

  How?

  (roll-in [[:a :b 3] [:a :c 4] [:x :z 0]])
  => {:a {:b 3 :c 4} :x {:z 0}}

  (roll-in (fnil + 0) [[:a :b 3] [:a :c 4] [:x :z 0] [:a :c 2]])
  => {:a {:b 3 :c 6} :x {:z 0}}
  "
  {:added "0.3.0"}
  ([seq-of-seqs]
   (reduce (partial apply assoc-in)
           {}
           (map (juxt butlast last) seq-of-seqs)))
  ([agg seq-of-seqs]
   (reduce (partial apply update-in)
           {}
           (map (juxt butlast (constantly agg) last) seq-of-seqs))))

(s/fdef roll-in
  :args
  (s/alt :1-arg  (s/cat :seq-of-seqs (s/coll-of (s/coll-of any? :into [])))
         :2-args (s/cat :agg         ifn?
                        :seq-of-seqs (s/coll-of (s/coll-of any? :into []))))
  :ret   map?)

(defn roll-out
  "Takes a map of maps, returns sequence of vectors.

  The stop-at? predicate halts expansion on keys it returns true for.

  Why?

  I like maps, but vectors are also pretty neat.

  How?

  (set (roll-out {:a {:b 3 :c 4} :x {:z 0}}))  ;; because ordering.
  => #{[:a :b 3] [:a :c 4] [:x :z 0]}

  (set (roll-out #(contains? % :b) {:a {:b 3 :c 4} :x {:z 0}}))
  => #{[:a {:b 3 :c 4}] [:x :z 0]}
  "
  {:added "0.3.0"}
  ([map-of-maps]
   (roll-out (constantly false) map-of-maps))
  ([stop-at? map-of-maps]
   (letfn[(step [path stack]
            (when (seq stack)
              (let [path* (if (zero? (count path)) [] (pop path))]
                (if (empty? (peek stack)) ;; nothing to do on current level
                  (recur path* (pop stack))  ;; POP-POP!
                  [(conj path* (peek (peek stack)))  ;; switch in path
                   (conj (pop stack) (pop (peek stack)))])))) ;; up in stack
          (snek [path stack]
            (let [x (get-in map-of-maps path)]
              (if-not (or (not (map? x)) (stop-at? x))
                (let [descent (vec (sort (keys x)))]
                  (recur (conj path (peek descent))
                         (conj stack (pop descent))))
                (let [[path* stack*] (step path stack)]
                  (cons (conj path x)
                        (lazy-seq
                          (when (seq path*)
                            (snek path* stack*))))))))]
     (if (stop-at? map-of-maps)
       [map-of-maps]
       (let [[path stack] (step [] [(vec (sort (keys map-of-maps)))])]
         (snek path stack))))))

(s/fdef roll-out
  :args
  (s/alt :2-args (s/cat :stop-at? ifn? :map-of-maps map?)
         :1-arg  (s/cat :map-of-maps map?))
  :ret   (s/nilable sequential?))

;; Parallel transducers

(defn default-parallel-n
  "Return advised parallel workload. Logical processor count + 2.

  Why?

  Sometimes you just want the answer.

  How?

  (default-parallel-n)
  => 10
  "
  {:added "0.3.2"}
  []
  (+ 2 (.availableProcessors (Runtime/getRuntime))))

(s/def ::num-threads (s/and number? pos?))

(s/fdef default-parallel-n
  :args (s/cat)
  :ret  ::num-threads)

(defn pp-map
  "Parametrized parallel map, with max of n threads at once.

  Accepts number of threads n, function f and collections. If no collections
  are provided it produces a stateful transducer. Uses futures.

  Why?

  Better pmap for multicore world.

  How?

  (vec (map inc [1 2 3]))
  => [2 3 4]

  (vec (pp-map 1 inc [1 2 3]))
  => [2 3 4]
  "
  {:added "0.4.0"}
  ([n f]
    (fn pp-map-xf [rf]
      (let [buffer (volatile! [])]
        (fn
          ([]
            (rf))
          ([result]
           (let [buff @buffer]
             (vreset! buffer [])
             (if (= 0 (count buff))
               (rf result)
               (transduce (map deref) rf result buff))))
          ([result input]
           (let [buff (vswap! buffer conj (future (f input)))
                 done (if (= n (count buff))
                        buff (vec (take-while realized? buff)))]
             (if (= 0 (count done))
               result
               (do
                 (vswap! buffer subvec (count done))
                 (loop [nidx 1 acc (rf result (deref (get done 0)))]
                   (if (reduced? acc)
                     acc
                     (if (< nidx (count done))
                       (recur (inc nidx) (rf acc (deref (get done nidx))))
                       acc)))))))
          ([result input & inputs]
           (let [buff (vswap! buffer conj (future (apply f input inputs)))
                 done (if (= n (count buff))
                        buff (vec (take-while realized? buff)))]
             (if (= 0 (count done))
               result
               (do
                 (vswap! buffer subvec (count done))
                 (loop [nidx 1 acc (rf result (deref (get done 0)))]
                   (if (reduced? acc)
                     acc
                     (if (< nidx (count done))
                       (recur (inc nidx) (rf acc (deref (get done nidx))))
                       acc)))))))))))
  ([n f coll]
   (sequence (pp-map n f) coll))
  ([n f coll & colls]
   (apply sequence (pp-map n f) coll colls)))

(s/def ::callable (s/or :fn fn? :ifn ifn?))

(s/def ::any-coll (s/coll-of any?))

(s/fdef pp-map
  :args
  (s/alt :2-args (s/cat :n     ::num-threads
                        :f     ::callable)
         :3-args (s/cat :n     ::num-threads
                        :f     ::callable
                        :coll  ::any-coll)
         :n-args (s/cat :n     ::num-threads
                        :f     ::callable
                        :coll  ::any-coll
                        :colls (s/* ::any-coll)))
  :ret  (s/or :sequential (s/nilable sequential?) :xf fn?))

(def ^{:deprecated "0.4.0"
       :added      "0.3.2"
       :doc        "Shim for pp-map!"}
  ppmap pp-map)

(defn pp-filter
  "Parametrized parallel filter, with max of n threads at once.

  Accepts number of threads n, predicate p and a collection. If no collection
  is provided it produces a stateful transducer. Predicates run in futures.

  Why?

  The real question is: why no pfilter!?

  How?

  (vec (filter odd? [1 2 3]))
  => [1 3]

  (vec (pp-filter 1 odd? [1 2 3]))
  => [1 3]
  "
  {:added "0.4.0"}
  ([n p]
    (fn pp-filter-xf [rf]
      (let [buffer (volatile! [])
            subxf  (comp (filter (comp deref first)) (map second))]
        (fn
          ([]
           (rf))
          ([result]
           (let [buff @buffer]
             (vreset! buffer [])
             (if (= 0 (count buff))
               (rf result)
               (transduce subxf rf result buff))))
          ([result input]
           (let [buff (vswap! buffer conj [(future (p input)) input])
                 done (if (= n (count buff))
                        buff (vec (take-while (comp realized? first) buff)))
                 pass (into [] subxf done)]
             (vswap! buffer subvec (count done))
             (if (= 0 (count pass))
               result
               (loop [nidx 1 acc (rf result (get pass 0))]
                 (if (reduced? acc)
                   acc
                   (if (< nidx (count pass))
                     (recur (inc nidx) (rf acc (get pass nidx)))
                     acc))))))))))
  ([n p coll]
   (sequence (pp-filter n p) coll)))

(s/fdef pp-filter
  :args
  (s/alt :2-args (s/cat :n     ::num-threads
                        :p     ::callable)
         :3-args (s/cat :n     ::num-threads
                        :p     ::callable
                        :coll  ::any-coll)
         :n-args (s/cat :n     ::num-threads
                        :p     ::callable
                        :coll  ::any-coll
                        :colls (s/* ::any-coll)))
  :ret  (s/or :sequential (s/nilable sequential?) :xf fn?))

(def ^{:deprecated "0.4.0"
       :added      "0.3.2"
       :doc        "Shim for pp-filter!"}
  ppfilter pp-filter)

(defn pp-mapcat
  "Parametrized parallel mapcat, with max of n threads at once for mapping.

  Accepts number of threads n, function f and a collection. If no collection
  is provided it produces a stateful transducer. Uses futures. 

  Why?

  Free once pp-map is defined.

  How?

  (mapcat (juxt dec identity inc) [1 2 3 4])
  => (0 1 2 1 2 3 2 3 4 3 4 5)

  (pp-mapcat 4 (juxt dec identity inc) [1 2 3 4])
  => (0 1 2 1 2 3 2 3 4 3 4 5)
  "
  {:added "0.4.0"}
  ([n f]
   (comp (pp-map n f) cat))
  ([n f & colls]
   (apply sequence (pp-mapcat n f) colls)))

(s/fdef pp-mapcat
  :args
  (s/alt :2-args (s/cat :n     ::num-threads
                        :f     ::callable)
         :3-args (s/cat :n     ::num-threads
                        :f     ::callable
                        :coll  ::any-coll)
         :n-args (s/cat :n     ::num-threads
                        :f     ::callable
                        :coll  ::any-coll
                        :colls (s/* ::any-coll)))
  :ret  (s/or :sequential (s/nilable sequential?) :xf fn?))

(def ^{:deprecated "0.4.0"
       :added      "0.3.2"
       :doc        "Shim for pp-mapcat!"}
  ppmapcat pp-mapcat)

(defn pp-remove
  "Parametrized parallel remove, with max of n threads at once.

  Accepts number of threads n, predicate p and a collection. If no collection
  is provided it produces a stateful transducer. Uses futures.

  Why?

  Free once pp-filter is defined.

  How?

  (vec (remove odd? [1 2 3]))
  => [2]

  (vec (pp-remove 1 odd? [1 2 3]))
  => [2]
  "
  {:added "0.4.0"}
  ([n p]
   (pp-filter n (complement p)))
  ([n p coll]
   (pp-filter n (complement p) coll)))

(s/fdef pp-remove
  :args
  (s/alt :2-args (s/cat :n     ::num-threads
                        :f     ::callable)
         :3-args (s/cat :n     ::num-threads
                        :f     ::callable
                        :coll  ::any-coll))
  :ret  (s/or :sequential (s/nilable sequential?) :xf fn?))

(defn pp-keep
  "Parametrized parallel keep, with max of n threads at once.

  Accepts number of threads n, function f and a collection. If no collection
  is provided it produces a stateful transducer. Uses futures.

  Why?

  Free once pp-map is defined.

  How?

  (vec (keep {:x 1} [:x :y :z]))
  => [1]

  (vec (pp-keep 1 {:x 1} [:x :y :z]))
  => [1]
  "
  {:added "0.4.0"}
  ([n f]
   (comp (pp-map n f) (filter (complement nil?))))
  ([n f coll]
   (sequence (pp-keep n f) coll)))

(s/fdef pp-keep
  :args
  (s/alt :2-args (s/cat :n     ::num-threads
                        :f     ::callable)
         :3-args (s/cat :n     ::num-threads
                        :f     ::callable
                        :coll  ::any-coll))
  :ret  (s/or :sequential (s/nilable sequential?) :xf fn?))

(defn pp-map-indexed
  "Parametrized parallel map-indexed, with max of n threads at once.

  Accepts number of threads n, function f and collections. If no collections
  are provided it produces a stateful transducer. Uses futures.

  Why?

  You sometimes need to compose it, that's why.

  How?

  (vec (map-indexed (fn [i k] [i (get {:x 1} k)]) [:x :y :z]))
  => [[0 1] [1 nil] [2 nil]]

  (vec (pp-map-indexed 1 (fn [i k] [i (get {:x 1} k)]) [:x :y :z]))
  => [[0 1] [1 nil] [2 nil]]
  "
  {:added "0.4.0"}
  ([n f]
   (fn pp-map-indexed-xf [rf]
     (let [buffer (volatile! [])
           index  (volatile! -1)
           subxf  (map deref)]
       (fn
         ([]
          (rf))
         ([result]
          (let [buff @buffer]
            (if (seq buff)
              (transduce subxf rf result buff)
              (rf result))))
         ([result input]
          (let [idx  (vswap! index inc)
                buff (vswap! buffer conj (future (f idx input)))
                done (if (= n (count buff))
                       buff
                       (vec (take-while realized? buff)))]
            (if (= 0 (count done))
              result
              (do
                (vswap! buffer subvec (count done))
                (loop [nidx 1 acc (rf result (deref (get done 0)))]
                  (if (reduced? acc)
                    acc
                    (if (< nidx (count done))
                      (recur (inc nidx) (rf acc (deref (get done nidx))))
                      acc)))))))))))
  ([n f coll]
   (sequence (pp-map-indexed n f) coll)))

(s/fdef pp-map-indexed
  :args
  (s/alt :2-args (s/cat :n     ::num-threads
                        :f     ::callable)
         :3-args (s/cat :n     ::num-threads
                        :f     ::callable
                        :coll  ::any-coll))
  :ret  (s/or :sequential (s/nilable sequential?) :xf fn?))

(defn pp-keep-indexed
  "Parametrized parallel keep-indexed, with max of n threads at once.

  Accepts number of threads n, function f and a collection. If no collection
  is provided it produces a stateful transducer. Uses futures.

  Why?

  Free once pp-map-indexed is defined.

  How?

  (vec (keep-indexed (fn [_ k] (get {:x 1} k)) [:x :y :z]))
  => [1]

  (vec (pp-keep-indexed 1 (fn [_ k] (get {:x 1} k)) [:x :y :z]))
  => [1]
  "
  {:added "0.4.0"}
  ([n f]
   (comp (pp-map-indexed n f) (filter (complement nil?))))
  ([n f coll]
   (sequence (pp-keep-indexed n f) coll)))

(s/fdef pp-keep-indexed
  :args
  (s/alt :2-args (s/cat :n     ::num-threads
                        :f     ::callable)
         :3-args (s/cat :n     ::num-threads
                        :f     ::callable
                        :coll  ::any-coll))
  :ret  (s/or :sequential (s/nilable sequential?) :xf fn?))

(defn all
  "Returns a function that succeeds if all predicate succeed, fails otherwise.
  Short circuits based on clojure.core/every? built-in and true for empty
  list of predicates.

  Why?

  Higher order functions are fun.

  How?

  (filterv (all number? odd?) [1 2 3 4])
  => [1 3]

  (filterv (all number? (complement odd?)) [1 2 3 4])
  => [2 4]
  "
  {:added "0.5.0"}
  [& preds]
  (with-meta
    (fn all-tester [v]
      (every? #(%1 v) preds))
    {:predicates preds}))

(s/fdef all
  :args (s/cat :preds (s/* ::callable))
  :ret  ::callable)

(defn any
  "Returns a function that succeeds if any predicate succeeds, fails if none.
  Short circuits based on clojure.core/some built-in and false for empty list
  of predicates.

  Why?

  Because we can.

  How?

  (filterv (any odd? pos?) [-1 0 1 2 3])
  => [-1 1 2 3]

  (filterv (any even? zero?) [-1 0 1 2 3])
  => [0 2]
  "
  {:added "0.5.0"}
  [& preds]
  (with-meta
    (fn any-tester [v]
      (boolean (some #(%1 v) preds)))
    {:predicates preds}))

(s/fdef any
  :args (s/cat :preds (s/* ::callable))
  :ret  ::callable)
