; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns szew.io.fu
  "Wrappers for java.nio.file Path and Files utilities."
  (:require
   [clojure.java.io :as cio]
   [clojure.spec.alpha :as s])
  (:import [java.io
            File InputStream OutputStream
            BufferedReader BufferedWriter]
           [java.net
            URI URL URLEncoder URLDecoder]
           [java.nio.file
            Files Path Paths FileStore
            CopyOption StandardCopyOption
            LinkOption FileVisitOption
            OpenOption StandardOpenOption]
           [java.nio.file.attribute
            BasicFileAttributes FileTime UserPrincipal
            PosixFilePermissions FileAttribute]
           [java.nio.charset Charset]
           [java.util.stream Stream]
           [java.time Instant]))

;; Constants

(def copy-option
  "Map of keywords to StandardCopyOption enums.
  "
  {:atomic-move      StandardCopyOption/ATOMIC_MOVE
   :copy-attributes  StandardCopyOption/COPY_ATTRIBUTES
   :replace-existing StandardCopyOption/REPLACE_EXISTING})

(def link-option
  "Map of keywords to LinkOption enums.
  "
  {:nofollow-links LinkOption/NOFOLLOW_LINKS})

(def open-option
  "Map of keywords to StandardOpenOption enums.
  "
  {:append            StandardOpenOption/APPEND
   :create            StandardOpenOption/CREATE
   :cretea-new        StandardOpenOption/CREATE_NEW
   :delete-on-close   StandardOpenOption/DELETE_ON_CLOSE
   :dsync             StandardOpenOption/DSYNC
   :read              StandardOpenOption/READ
   :sparse            StandardOpenOption/SPARSE
   :sync              StandardOpenOption/SYNC
   :truncate-existing StandardOpenOption/TRUNCATE_EXISTING
   :write             StandardOpenOption/WRITE})

(def visit-option
  "Map of keywords to FileVisitOption enums.
  "
  {:follow-links FileVisitOption/FOLLOW_LINKS})

(defn select-options
  "Lookup keyword options in given pool, translate if found, drop otherwise.
  Pass non-keywords 'as is'.
  "
  {:added "0.5.0"}
  ([pool opts]
   (keep (fn [opt]
           (if (keyword? opt)
             (when (contains? pool opt) (get pool opt))
             opt))
         opts)))

(defn lookup-option
  "Reverse lookup of option object to respective keyword. Not found: nil.
  "
  {:added "0.5.0"}
  ([option-object]
   (ffirst (filter (comp (partial = option-object) second)
                   (concat copy-option link-option open-option visit-option)))))

(defn copy-opts
  "Accepts opts, keywords are subbed with copy-option, other values passed on.
  "
  {:added "0.5.0"}
  (^"[Ljava.nio.file.CopyOption;" [options]
   (into-array CopyOption (select-options copy-option (distinct options))))
  (^"[Ljava.nio.file.CopyOption;" []
   (into-array CopyOption [])))

(defn link-opts
  "Accepts opts, keywords are subbed with link-option, other values passed on.
  "
  {:added "0.5.0"}
  (^"[Ljava.nio.file.LinkOption;" [options]
   (into-array LinkOption (select-options link-option (distinct options))))
  (^"[Ljava.nio.file.LinkOption;" []
   (into-array LinkOption [])))

(defn open-opts
  "Accepts opts, keywords are subbed with open-option, other values passed on.
  "
  {:added "0.5.0"}
  (^"[Ljava.nio.file.OpenOption;" [options]
   (into-array OpenOption (select-options open-option (distinct options))))
  (^"[Ljava.nio.file.OpenOption;" []
   (into-array OpenOption [])))

(defn visit-opts
  "Accepts opts, keywords are subbed with file-visit-option, others passed on.
  "
  {:added "0.5.0"}
  (^"[Ljava.nio.file.FileVisitOption;" [options]
   (into-array FileVisitOption (select-options visit-option (distinct options))))
  (^"[Ljava.nio.file.FileVisitOption;" []
   (into-array FileVisitOption [])))

(defn file-attrs
  "Accepts attributes, packs them into FileAttribute typed array.
  "
  {:added "0.5.0"}
  (^"[Ljava.nio.file.attribute.FileAttribute;" [attributes]
   (into-array FileAttribute attributes))
  (^"[Ljava.nio.file.attribute.FileAttribute;" []
   (into-array FileAttribute [])))

(defn string->perms
  "Translate 'rwxrwxrwx' String into PosixFilePermission enums set via call
  to PosixFilePermissions/fromString.

  Counterpart of perms->string function.
  "
  {:added "0.5.0"}
  ([^String rwx]
   (PosixFilePermissions/fromString rwx)))

(defn perms->string
  "Translate collection of PosixFilePermission objects into a String via call
  to PosixFilePermissions/toString.

  Counterpart of string->perms function.
  "
  {:added "0.5.0"}
  (^String [permissions]
   (PosixFilePermissions/toString
    (doto (java.util.HashSet.) (.addAll permissions)))))

(defn perms->attr
  "Produce a set of FileAttributes based on PosixFilePermission, via call
  to PosixFilePermissions/asFileAttribute.
  "
  {:added "0.5.0"}
  ([permissions]
   (PosixFilePermissions/asFileAttribute
    (doto (java.util.HashSet.) (.addAll permissions)))))

;; Important bits!

(defprotocol Conversions
  "Best effort Conversion protocol."
  (->path [this] "Convert to Path instance.")
  (->uri [this] "Convert to URI instance.")
  (->url [this] "Convert to URL instance.")
  (->file [this] "Convert to File instance.")
  (->string [this] "Convert to String instance."))

(extend-protocol Conversions
  nil
  (->path [_] nil)
  (->uri [_] nil)
  (->url [_] nil)
  (->file [_] nil)
  (->string [_] "")
  Path
  (->path [this] this)
  (->uri [this] (.toUri this))
  (->url [this] (.toURL ^URI (.toUri this)))
  (->file [this] (.toFile this))
  (->string [this] (.toString this))
  URI
  (->path [this] (Paths/get this))
  (->uri [this] this)
  (->url [this] (.toURL this))
  (->file [this] (cio/as-file (.toURL this)))
  (->string [this] (.toString (cio/as-file this)))
  URL
  (->path [this] (.toPath ^File (cio/as-file this)))
  (->uri [this] (.toURI this))
  (->url [this] this)
  (->file [this] (cio/as-file this))
  (->string [this] (.toString (cio/as-file this)))
  File
  (->path [this] (.toPath this))
  (->uri [this] (.toURI this))
  (->url [this] (.toURL ^URI (.toURI this)))
  (->file [this] this)
  (->string [this] (.toString this))
  String
  (->path [this] (Paths/get this (into-array String [])))
  (->uri [this] (.toURI (cio/as-file this)))
  (->url [this] (.toURL ^URI (.toURI (cio/as-file this))))
  (->file [this] (cio/as-file this))
  (->string [this] this))

(defn to-path
  "Tries to convert object to Path.
  "
  {:added "0.5.0"}
  (^Path [object]
   (->path object)))

(defn to-uri
  "Tries to convert object to URI.
  "
  {:added "0.5.0"}
  (^URI [object]
   (->uri object)))

(defn to-url
  "Tries to convert object to URL.
  "
  {:added "0.5.0"}
  (^URL [object]
   (->url object)))

(defn to-file
  "Tries to convert object to File.
  "
  {:added "0.5.0"}
  (^File [object]
   (->file object)))

(defn to-string
  "Tries to convert object to String.
  "
  {:added "0.5.0"}
  (^String [object]
   (->string object)))

;; Making clojure.java.io's as-file and file work with Paths.

(extend-protocol cio/Coercions
  Path
  (as-file [this] (.toFile this))
  (as-url [this] (.toURL ^URI (.toUri this))))

;; Making clojure.java.io's readers and writers work with Paths.
;; :make-reader and :make-writer rely on stream methods to be set.

(extend Path
  cio/IOFactory
  (assoc cio/default-streams-impl
         :make-input-stream
         (fn make-input-stream [^Path p opts]
           (cio/make-input-stream (.toFile p) opts))
         :make-output-stream
         (fn make-output-stream [^Path p opts]
           (cio/make-output-stream (.toFile p) opts))))

;; Path predicates, finally.

(defn absolute?
  "Checks if resulting Path is absolute via Path.isAbsolute call.
  "
  {:added "0.5.0"}
  ([a-path]
   (.isAbsolute (to-path a-path))))

(defn directory?
  "Checks if given file is a directory via Files/isDirectory call.

  Accepts link options.
  "
  {:added "0.5.0"}
  ([a-path options]
   (Files/isDirectory (to-path a-path) (link-opts options)))
  ([a-path]
   (Files/isDirectory (to-path a-path) (link-opts))))

(defn executable?
  "Checks if resulting Path is executable via Files/isExecutable call.
  "
  {:added "0.5.0"}
  ([a-path]
   (Files/isExecutable (to-path a-path))))

(defn exists?
  "Checks if resulting Path exists via Files/exists call.

  Accepts link options.
  "
  {:added "0.5.0"}
  ([a-path options]
   (Files/exists (to-path a-path) (link-opts options)))
  ([a-path]
   (Files/exists (to-path a-path) (link-opts))))

(defn not-exists?
  "Checks if resulting Path does not exist via Files/notExists call.

  Accepts link options.
  "
  {:added "0.5.0"}
  ([a-path options]
   (Files/notExists (to-path a-path) (link-opts options)))
  ([a-path]
   (Files/notExists (to-path a-path) (link-opts))))

(defn file?
  "Checks if resulting Path is a regular file via Path.isRegularFile call.

  Accepts link options.
  "
  {:added "0.5.0"}
  ([a-path options]
   (Files/isRegularFile (to-path a-path) (link-opts options)))
  ([a-path]
   (Files/isRegularFile (to-path a-path) (link-opts))))

(defn hidden?
  "Checks if resulting Path is a hidden file via Files/isHidden call.
  "
  {:added "0.5.0"}
  ([a-path]
   (Files/isHidden (to-path a-path))))

(defn readable?
  "Checks if resulting Path is readable via Files/isReadable call.
  "
  {:added "0.5.0"}
  ([a-path]
   (Files/isReadable (to-path a-path))))

(defn same-file?
  "Checks if file-1 and file-2 the same file via Files/isSameFile call.
  "
  {:added "0.5.0"}
  ([file-1 file-2]
   (Files/isSameFile (to-path file-1) (to-path file-2))))

(defn symbolic-link?
  "Checks if resulting Path is a symbolic link via Files/isSymbolicLink call.
  "
  {:added "0.5.0"}
  ([a-path]
   (Files/isSymbolicLink (to-path a-path))))

(defn writable?
  "Czech of resulting Path is writable via Files/isWritable call.
  "
  {:added "0.5.0"}
  ([a-path]
   (Files/isWritable (to-path a-path))))

(defn starts-with?
  "Check if Path path-1 starts with path-2 via Path.startsWith call.
  "
  {:added "0.5.0"}
  ([path-1 path-2]
   (.startsWith (to-path path-1) (to-path path-2))))

(defn ends-with?
  "Check if Path path-1 ends with path-2 via Path.endsWith call.
  "
  {:added "0.5.0"}
  ([path-1 path-2]
   (.endsWith (to-path path-1) (to-path path-2))))

;; Files ops

(defn create-directories!
  "Create all directories in Path via Files/createDirectories call.

  Accepts optional FileAttribute container.
  "
  {:added "0.5.0"}
  (^Path [a-path]
   (Files/createDirectories (to-path a-path)
                            (into-array FileAttribute [])))
  (^Path [a-path attrs]
   (Files/createDirectories (to-path a-path)
                            (into-array FileAttribute attrs))))

(defn create-directory!
  "Create directory in Path via Files/createDirectory call.

  Accepts optional FileAttribute container.
  "
  {:added "0.5.0"}
  (^Path [a-path]
   (Files/createDirectory (to-path a-path) (file-attrs)))
  (^Path [a-path attrs]
   (Files/createDirectory (to-path a-path) (file-attrs attrs))))

(defn create-file!
  "Create file in Path via Files/createFile call.
  "
  {:added "0.5.0"}
  (^Path [a-path]
   (Files/createFile (to-path a-path) (file-attrs)))
  (^Path [a-path attrs]
   (Files/createFile (to-path a-path) (file-attrs attrs))))

(defn create-link!
  "Create hard link to existing-path in link-path via Files/createLink call.
  "
  {:added "0.5.0"}
  (^Path [link-path existing-path]
   (Files/createLink (to-path link-path) (to-path existing-path))))

(defn create-symbolic-link!
  "Create link-path to existing-path symbolic link via Files/createSymbolicLink
  call.
  "
  {:added "0.5.0"}
  (^Path [link-path existing-path]
   (Files/createSymbolicLink (to-path link-path)
                             (to-path existing-path)
                             (file-attrs)))
  (^Path [link-path existing-path attrs]
   (Files/createSymbolicLink (to-path link-path)
                             (to-path existing-path)
                             (file-attrs attrs))))

(defn create-temp-directory!
  "Create temporary directory at default TEMP location with given prefix via
  Files/createTempDirectory call.
  "
  {:added "0.5.0"}
  (^Path [^String prefix]
   (Files/createTempDirectory prefix (file-attrs)))
  (^Path [^String prefix attrs]
   (Files/createTempDirectory prefix (file-attrs attrs))))

(defn create-temp-directory-at!
  "Create a temporary directory at Path location with given prefix via
  Files/createTempDirectory call.
  "
  {:added "0.5.0"}
  (^Path [a-path ^String prefix]
   (Files/createTempDirectory (to-path a-path) prefix (file-attrs)))
  (^Path [a-path ^String prefix attrs]
   (Files/createTempDirectory (to-path a-path) prefix (file-attrs attrs))))

(defn create-temp-file!
  "Create temporary file at default TEMP location with given prefix and suffix
  via Files/createTempFile call.
  "
  {:added "0.5.0"}
  (^Path [^String prefix ^String suffix]
   (Files/createTempFile prefix suffix (file-attrs)))
  (^Path [^String prefix ^String suffix attrs]
   (Files/createTempFile prefix suffix (file-attrs attrs))))

(defn create-temp-file-at!
  "Create temporary file at Path location with given prefix and suffix via
  Files/createTempFile call.
  "
  {:added "0.5.0"}
  (^Path [a-path ^String prefix ^String suffix]
   (Files/createTempFile (to-path a-path) prefix suffix (file-attrs)))
  (^Path [a-path ^String prefix ^String suffix attrs]
   (Files/createTempFile (to-path a-path) prefix suffix (file-attrs attrs))))

(defn delete!
  "Delete a file via Files/delete call.
  "
  {:added "0.5.0"}
  ([a-path]
   (Files/delete (to-path a-path))))

(defn delete-if-exists!
  "Delete a fil via Files/deleteIfExists call.
  "
  {:added "0.5.0"}
  ([a-path]
   (Files/deleteIfExists (to-path a-path))))

(defn delete-on-exit!
  "Mark given file for deletion on JVM exit via File.deleteOnExit call.
  "
  {:added "0.5.0"}
  ([a-file]
   (.deleteOnExit (to-file a-file))))

(defn copy!
  "Copy file-1 to file-2 via Files/copy call.

  Accepts copy options

  See `stream-in!` and `stream-out!` for copies from and to streams.
  "
  {:added "0.5.0"}
  (^Path [file-1 file-2 options]
   (Files/copy (to-path file-1) (to-path file-2) (copy-opts options)))
  (^Path [file-1 file-2]
   (Files/copy (to-path file-1) (to-path file-2) (copy-opts))))

(defn stream-in!
  "Copy InputStream stream-1 to path-2 via Files/copy call.

  Accepts copy options

  See `copy!` for file to file and `stream-out!` for to stream copy.
  "
  {:added "0.5.0"}
  (^long [stream-1 path-2 options]
   (Files/copy ^InputStream stream-1 (to-path path-2) (copy-opts options)))
  (^long [stream-1 path-2]
   (Files/copy ^InputStream stream-1 (to-path path-2) (copy-opts))))

(defn stream-out!
  "Copy path-1 to OutputStream via Files/copy call.

  See `stream-in!` for stream to file and `copy!` for file to file.
  "
  {:added "0.5.0"}
  (^long [path-1 stream-2]
   (Files/copy (to-path path-1) ^OutputStream stream-2)))

(defn move!
  "Move path-1 to path-2 via Files/move call.

  Accepts copy options.
  "
  {:added "0.5.0"}
  (^Path [path-1 path-2 options]
   (Files/move (to-path path-1) (to-path path-2) (copy-opts options)))
  (^Path [path-1 path-2]
   (Files/move (to-path path-1) (to-path path-2) (copy-opts))))

;; Path ops

(defn pathable?
  "Checks if obj has ->string defined.
  "
  [obj]
  (try
    (->string obj)
    true
    (catch Exception _ false)))

(defn path
  "Builds a Path from given String(s) via Path/get static method.

  Runs all arguments through to-string to avoid trouble, so just keep in mind
  that Conversions protocol and the fact that nil returns valid, empty string,
  which does not produce a path segment, but when resolved to absolute path
  will be equal to current working directory.

  This allows to work with strings, paths, files and nils with no hassle
  and with URLs and URIs with some attention, as their string representations
  are absolute paths.
  "
  {:added "0.5.0"}
  (^Path [path & paths]
   (Paths/get (to-string path) (into-array String (mapv to-string paths)))))

(s/fdef path
  :args (s/alt :path pathable? :paths (s/* pathable?)))

(defn file-name
  "Convert to Path, get file name Path via Path.getFileName method.
  "
  {:added "0.5.0"}
  (^Path [a-path]
   (.getFileName (to-path a-path))))

(defn basename
  "Convert to path, make absolute, normalize, get file-name, stringify that.
  "
  {:added "0.5.0"}
  (^String [a-path]
   (.toString (.getFileName (.normalize (.toAbsolutePath (to-path a-path)))))))

(defn parent-path
  "Convert to Path, get parent Path via Path.getParent method.
  "
  {:added "0.5.0"}
  (^Path [a-path]
   (.getParent (to-path a-path))))

(defn root-path
  "Convert to Path, get root Path via Path.getRoot, nil if none present.
  "
  {:added "0.5.1"}
  (^Path [a-path]
   (.getRoot (to-path a-path))))

(defn absolute-path
  "Convert to Path, get absolute path via Path.toAbsolutePath method.
  "
  {:added "0.5.0"}
  (^Path [a-path]
   (.toAbsolutePath (to-path a-path))))

(defn real-path
  "Convert to Path, get real path via Path.toRealPath method.

  Accepts link options.
  "
  {:added "0.5.0"}
  (^Path [a-path options]
   (.toRealPath (to-path a-path) (link-opts options)))
  (^Path [a-path]
   (.toRealPath (to-path a-path) (link-opts))))

(defn file-system
  "Convert to Path, get file system via Path.getFileSystem call.
  "
  {:added "0.5.0"}
  ([a-path]
   (.getFileSystem (to-path a-path))))

(defn path-name-count
  "Convert to Path, get segment count via Path.getNameCount call.
  "
  {:added "0.5.0"}
  ([a-path]
   (.getNameCount (to-path a-path))))

(defn path-name-at
  "Convert to Path, get segment Path at index idx via Path.getName call.
  "
  {:added "0.5.0"}
  (^Path [a-path idx]
   (.getName (to-path a-path) idx)))

(defn subpath
  "Call Path.subpath to produce segments spanning from begin to end indices.
  "
  {:added "0.5.0"}
  (^Path [a-path begin end]
   (.subpath (to-path a-path) begin end)))

(defn path-elements
  "Convert to Path, return vector of Path.iterator produced segments.
  "
  {:added "0.5.0"}
  ([a-path]
   (vec (iterator-seq (.iterator (to-path a-path))))))

(defn normalize-path
  "Convert to Path, normalize it via Path.normalize call"
  {:added "0.5.0"}
  (^Path [a-path]
   (.normalize (to-path a-path))))

(defn relativize-path
  "Create relative Path between path-1 and path-2 via Path.relativize call.
  "
  {:added "0.5.0"}
  (^Path [path-1 path-2]
   (.relativize (to-path path-1) (to-path path-2))))

(defn resolve-path
  "Create resolved Path between path-2 and path-1 via Path.resolve call.
  "
  {:added "0.5.0"}
  (^Path [path-1 path-2]
   (.resolve (to-path path-1) (to-path path-2))))

(defn resolve-sibling-path
  "Create sibling Path between path-1 and path-2 via Path.resolveSibling call.
  "
  {:added "0.5.0"}
  (^Path [path-1 path-2]
   (.resolveSibling (to-path path-1) (to-path path-2))))

;; Attributes

(defn attrs-struct
  "Read Path attributes into given class instance via Files/readAttributes call.

  Accepts attribute class and link options. Defaults to BasicFileAttributes
  class and no options.
  "
  {:added "0.5.0"}
  ([path]
   (Files/readAttributes (to-path path) BasicFileAttributes (link-opts)))
  ([path options]
   (Files/readAttributes (to-path path)
                         BasicFileAttributes
                         (link-opts options)))
  ([path ^java.lang.Class attr-class options]
   (Files/readAttributes (to-path path) attr-class (link-opts options))))

(defn attrs
  "Produces a map of selected attributes via Files/readAttributes call.

  Accepts link options.

  Attributes can be:

  \"*\" for all basic attributes,
  \"dos:*\" for all DOS expected attributes,
  \"posix:*\" for all POSIX attributes,
  \"size,lastModifiedTime,lastAccessTime\" to get selected basic values
  \"posix:permissions,size\" to pick more specifig values.
  "
  {:added "0.5.0"}
  ([a-path ^String attributes]
   (into (hash-map)
         (Files/readAttributes (to-path a-path) (str attributes) (link-opts))))
  ([a-path ^String attributes options]
   (into (hash-map)
         (Files/readAttributes (to-path a-path)
                               (str attributes)
                               (link-opts options)))))

(defn attr
  "Get attribute from a Path via Files/getAttribute call.

  Accepts link options.
  "
  {:added "0.5.0"}
  ([a-path ^String attribute]
   (Files/getAttribute (to-path a-path) attribute (link-opts)))
  ([a-path ^String attribute options]
   (Files/getAttribute (to-path a-path) attribute (link-opts options))))

(defn size
  "Read size attribute of a Path via Files/size call.
  "
  {:added "0.5.0"}
  (^long [a-path]
   (Files/size (to-path a-path))))

(defn file-store
  "Get file store of Path via Files/getFileStore call.
  "
  {:added "0.5.0"}
  (^FileStore [a-path]
   (Files/getFileStore (to-path a-path))))

(defn last-modified-time
  "Get last modified time FileTime via Files/getLastModifiedTime call.

  Accepts link options.
  "
  {:added "0.5.0"}
  (^FileTime [a-path]
   (Files/getLastModifiedTime (to-path a-path) (link-opts)))
  (^FileTime [a-path options]
   (Files/getLastModifiedTime (to-path a-path) (link-opts options))))

(defn owner
  "Get UserPrincipal via Files/getOwner call.

  Accepts link options.
  "
  {:added "0.5.0"}
  (^UserPrincipal [a-path]
   (Files/getOwner (to-path a-path) (link-opts)))
  (^UserPrincipal [a-path options]
   (Files/getOwner (to-path a-path) (link-opts options))))

(defn posix-perms
  "Get POSIX file permissions via Files/getPosixFilePermissions call.

  Accepts link options.
  "
  {:added "0.5.0"}
  ([a-path]
   (into #{} (Files/getPosixFilePermissions (to-path a-path) (link-opts))))
  ([a-path options]
   (into #{} (Files/getPosixFilePermissions (to-path a-path)
                                            (link-opts options)))))

(defn probe-content-type
  "Try to probe content type via Files/probeContentType call.
  "
  {:added "0.5.0"}
  (^String [a-path]
   (Files/probeContentType (to-path a-path))))

;; Locations

(defn cwd
  "Current working directory of JVM instance as absolute path.
  "
  {:added "0.5.0"}
  (^Path []
   (absolute-path (to-path "."))))

(defn fs-roots
  "Get available filesystem roots as paths from File/listRoots call.
  "
  {:added "0.5.0"}
  ([]
   (mapv to-path (File/listRoots))))

;; Path setters

(defn attr!
  "Set attribute value on a Path via Files/setAttribute call.

  Accepts link options.
  "
  {:added "0.5.0"}
  (^Path [a-path ^String attribute ^Object value options]
   (Files/setAttribute (to-path a-path)
                       attribute value
                       (link-opts options)))
  (^Path [a-path ^String attribute ^Object value]
   (Files/setAttribute (to-path a-path) attribute value (link-opts))))

(defn last-modified-time!
  "Set last modified time value from FileTime via Files/setLastModifiedtime
  call.
  "
  {:added "0.5.0"}
  (^Path [a-path ^FileTime lmt]
   (Files/setLastModifiedTime (to-path a-path) lmt)))

(defn owner!
  "Set UserPrincipal owner on a Path via Files/setOwner call.
  "
  {:added "0.5.0"}
  (^Path [a-path ^UserPrincipal new-owner]
   (Files/setOwner (to-path a-path) new-owner)))

(defn posix-perms!
  "Sets POSIX file permissions on Path via Files/setPosixFilePermissions call.
  "
  {:added "0.5.0"}
  (^Path [a-path file-permissions]
   (let [perms (doto (java.util.HashSet.) (.addAll file-permissions))]
     (Files/setPosixFilePermissions (to-path a-path) perms))))

;; Misc helpers

(defn charset-please
  "Get Charset or String, give Charset back via Charset/forName call.
  "
  {:added "0.5.0"}
  (^Charset [enc]
   (if (string? enc)
     (Charset/forName enc)
     enc)))

(defn path->bytes
  "Returns bytes content via Files/readAllBytes call.
  "
  {:added "0.5.0"}
  (^bytes [a-path]
   (Files/readAllBytes (to-path a-path))))

(defn path->lines
  "Returns a lazy Stream<String> from Path and encoding via Files/lines call.

  Remember to close Stream after use.
  "
  {:added "0.5.0"}
  (^Stream [a-path charset]
   (Files/lines (to-path a-path) (charset-please charset)))
  (^Stream [a-path]
   (Files/lines (to-path a-path))))

(defn path->string
  "Returns string content via Files/readString call.
  "
  {:added "0.5.0"}
  (^String [a-path charset]
   (Files/readString (to-path a-path) (charset-please charset)))
  (^String [a-path]
   (Files/readString (to-path a-path))))

(defn dir->stream
  "Returns a Stream<Path> of directory contents.

  Remember to close the Stream after use.
  "
  {:added "0.5.0"}
  (^Stream [a-path]
   (Files/list (to-path a-path))))

(defn dir->vec
  "Returns a [Path] of directory contents.
  "
  {:added "0.5.0"}
  ([a-path]
   (with-open [d (Files/list (to-path a-path))]
     (vec (iterator-seq (.iterator d))))))

(defn bytes->path
  "Writes some-bytes to a-path via Files/write call.

  Accepts open options.
  "
  {:added "0.5.0"}
  (^Path [a-path ^bytes some-bytes options]
   (Files/write (to-path a-path) some-bytes (open-opts options))))

(defn lines->path
  "Writes some-lines Iterable to a-path via Files/write call.

  Accepts open options."
  {:added "0.5.0"}
  (^Path [a-path ^Iterable some-lines options]
   (Files/write (to-path a-path)
                some-lines
                (open-opts options)))
  (^Path [a-path ^Iterable some-lines charset options]
   (Files/write (to-path a-path)
                some-lines
                (charset-please charset)
                (open-opts options))))

(defn string->path
  "Writes a-string to a-path via Files/writeString call.

  Accepts open options.
  "
  {:added "0.5.0"}
  (^Path [a-path ^String a-string options]
   (Files/writeString (to-path a-path) a-string (open-opts options)))
  (^Path [a-path ^String a-string charset options]
   (Files/writeString (to-path a-path)
                      a-string
                      (charset-please charset)
                      (open-opts options))))

;; Streams and Buffered stuff

(defn buffered-reader
  "Produces BufferedReader with Files/newBufferedReader call.
  "
  {:added "0.5.0"}
  (^BufferedReader [a-path]
   (Files/newBufferedReader (to-path a-path)))
  (^BufferedReader [a-path charset]
   (Files/newBufferedReader (to-path a-path) (charset-please charset))))

(defn buffered-writer
  "Produces BufferedWriter with Files/newBufferedWriter call.
  "
  {:added "0.5.0"}
  (^BufferedWriter [a-path options]
   (Files/newBufferedWriter (to-path a-path)
                            (charset-please "UTF-8")
                            (open-opts options)))
  (^BufferedWriter [a-path charset options]
   (Files/newBufferedWriter (to-path a-path)
                            (charset-please charset)
                            (open-opts options))))

(defn input-stream
  "Produces InputStream with Files/newInputStream call.
  "
  {:added "0.5.0"}
  (^InputStream [a-path]
   (Files/newInputStream (to-path a-path) (open-opts)))
  (^InputStream [a-path options]
   (Files/newInputStream (to-path a-path) (open-opts options))))

(defn output-stream
  "Produces OutputStream with Files/newOutputStream call.
  "
  {:added "0.5.0"}
  (^OutputStream [a-path]
   (Files/newOutputStream (to-path a-path) (open-opts)))
  (^OutputStream [a-path options]
   (Files/newOutputStream (to-path a-path) (open-opts options))))

;; Some extras

(defn pruning-path-seq
  "Like file-seq, but works on Path and can prune branches based on predicate.

  If `follow?` returns false - Path is not processed, directory not entered.
  Predicate will be first executed on given entry.

  Why?

  Ever went into a massive .svn directory? Now you can have tree-seq that
  prunes the entire Path of enquiry and moves on. Pretty nice!
  "
  {:added "0.5.0"}
  ([entry follow?]
   (let [root (to-path entry)]
     (when (follow? root)
       (tree-seq (fn branch? [^Path p]
                   (directory? p))
                 (fn children [^Path p]
                   (filterv follow? (dir->vec p)))
                 root))))
  ([entry]
   (pruning-path-seq entry (constantly true))))

(defn delete-tree!
  "Recursively delete all files, then directories. Returns sequence of paths.

  Walks the tree depth-first, files get deleted in FIFO order and directories
  go in LIFO order. Lazy, so please make sure to either use dorun or realize
  it into a collection.
  "
  {:added "0.5.0"}
  ([a-path skip-root?]
   (letfn [(delete-r [todo parked]
             (if (seq todo)
               (let [f (first todo)]
                 (if (directory? f)
                   (recur (next todo) (conj parked f))
                   (do (delete! f)
                       (cons f (lazy-seq (delete-r (next todo) parked))))))
               (when (seq parked)
                 (let [l (peek parked)]
                   (delete! l)
                   (cons l (lazy-seq (delete-r nil (pop parked))))))))]
     (if skip-root?
       (delete-r (drop 1 (pruning-path-seq (to-path a-path))) [])
       (delete-r (pruning-path-seq (to-path a-path)) []))))
  ([a-path]
   (delete-tree! a-path false)))

(defn copy-tree!
  "Recursively copy a file tree with copy options in iteration order.

  Returns a lazy sequence of [from to] path pairs. Skips entry point directory
  unless it's explicitly part of to path. If from is a file copies that file
  under to directory. If skipped then default copy options are :copy-attributes
  and :replace-existing. If :copy-attributes specified then tries to at least
  maintain last-modified-time across files and directories.

  It is lazy, so either dorun or realize into a collection.
  "
  {:added "0.5.2"}
  ([from to options]
   (let [from-path (normalize-path (absolute-path (to-path from)))
         to-path   (normalize-path (absolute-path (to-path to)))
         mtime?    (some (partial = :copy-attributes) options)]
     (when (not-exists? from-path)
       (throw (ex-info "Source does not exist!"
                       {:from from :to to})))
     (when (starts-with? to-path from-path)
       (throw (ex-info "Target directory is within source!"
                       {:from from :to to})))
     (letfn [(cpy [from-subpath]
               (let [to-subpath (->> from-subpath
                                     (relativize-path from-path)
                                     (resolve-path to-path))]
                 (copy! from-subpath to-subpath options)
                 (when mtime?
                   (->> (last-modified-time from-subpath)
                        (last-modified-time! to-subpath)))
                 [from-subpath to-subpath]))]
       (map cpy (pruning-path-seq from-path)))))
  ([from to]
   (copy-tree! from to [:copy-attributes :replace-existing])))

(defn url-encode
  "Calls URLEncoder.encode on a-string with encoding, UTF-8 by default.
  "
  {:added "0.5.0"}
  (^String [^String a-string]
   (URLEncoder/encode a-string "UTF-8"))
  (^String [^String a-string ^String enc]
   (URLEncoder/encode a-string enc)))

(defn url-decode
  "Calls URLDecoder.decode on a-string with encoding, UTF-8 by default.
  "
  {:added "0.5.0"}
  (^String [^String a-string]
   (URLDecoder/decode a-string "UTF-8"))
  (^String [^String a-string ^String enc]
   (URLDecoder/decode a-string enc)))

(defn url-encode-path
  "URL-encode given path as UTF-8, element by element.
  "
  {:added "0.5.1"}
  (^Path [a-path]
   (let [a-path  (to-path a-path)
         root    ^Path (.getRoot a-path)
         encoded (reduce resolve-path
                         (path "")
                         (mapv (comp url-encode to-string)
                               (path-elements a-path)))]
     (if (and (absolute? a-path) (not (starts-with? encoded root)))
       (resolve-path root encoded)
       encoded))))

(defn url-decode-path
  "URL-decode given path as UTF-8, element by element.
  "
  {:added "0.5.1"}
  (^Path [a-path]
   (let [a-path  (to-path a-path)
         root    ^Path (.getRoot a-path)
         decoded (reduce resolve-path
                         (path "")
                         (mapv (comp url-decode to-string)
                               (path-elements a-path)))]
     (if (and (absolute? a-path) (not (starts-with? decoded root)))
       (resolve-path root decoded)
       decoded))))

(defn is-empty?
  "True for childless directories, 0 byte files and non-existing Paths.

  Checks for existence first, then tries to resolve with real-path.

  Anything else: throws ExceptionInfo.
  "
  {:added "0.5.1"}
  (^Boolean [^Path a-path]
   (if (not-exists? (to-path a-path)) ;; resolves symlinks by default
     true
     (let [check-path (real-path (to-path a-path))]
       (cond
         (directory? check-path)
         (= 0 (count (dir->vec check-path)))
         (file? check-path)
         (= 0 (size check-path))
         :else
         (throw (ex-info "Cannot determine emptiness!"
                         {:path a-path})))))))

(defn all-subpaths
  "Returns all Path ancestors from given point, going up.

  Lazy!
  "
  {:added "0.5.1"}
  [a-path]
  (let [tail (to-path a-path)]
    (cons tail
          (lazy-seq
           (when-let [parent (parent-path tail)]
             (all-subpaths parent))))))


(defn sane-path
  "Make path absolute first and normalized later. Keep your sanity.
  "
  {:added "0.5.2"}
  (^Path [^Path a-path]
   (normalize-path (absolute-path (to-path a-path)))))

(defn mtime
  "Get last modified time as Instant from a path.
  "
  {:added "0.5.2"}
  (^Instant [a-path]
   (.toInstant (last-modified-time (to-path a-path)))))

(defn mtime!
  "Change modified time of file based on given Instant or to current time.
  "
  {:added "0.5.2"}
  (^Path [^Path a-path ^Instant an-instant]
   (last-modified-time! (to-path a-path) (FileTime/from an-instant)))
  (^Path [^Path a-path]
   (mtime! (to-path a-path) (Instant/now))))

